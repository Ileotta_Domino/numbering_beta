﻿using Numbering3.Helper.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Numbering3.View
{
    /// <summary>
    /// Interaction logic for CounterSettings.xaml
    /// </summary>
    public partial class CounterSettings : Window
    {
        public CounterSettings()
        {
            InitializeComponent();
            this.MouseLeftButtonDown += delegate { this.DragMove(); };
            DataContextChanged += new DependencyPropertyChangedEventHandler(CounterSettings_DataContextChanged);
        }

        void CounterSettings_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }


        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AlphabetValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,a-z,A-Z]+");
            e.Handled = regex.IsMatch(e.Text);
        }


        private void SequenceValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^A,B,C,N]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SequenceRequested_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void BSequence_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void CSequence_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
