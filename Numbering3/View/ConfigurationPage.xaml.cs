﻿using Microsoft.Win32;
using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Numbering3.View
{
    /// <summary>
    /// Interaction logic for ConfigurationPage.xaml
    /// </summary>
    public partial class ConfigurationPage : Window
    {
        IConfigurationListHelper _configurationListHelper = new ConfigurationListHelper();
        public ConfigurationPage()
        {
            InitializeComponent();
            this.MouseLeftButtonDown += delegate { this.DragMove(); };
            DataContextChanged += new DependencyPropertyChangedEventHandler(ConfigurationPage_DataContextChanged);
        }

        void ConfigurationPage_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }
        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            //SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            //saveFileDialog.FileName = "Document"; // Default file name
            //saveFileDialog.DefaultExt = ".text"; // Default file extension
            //saveFileDialog.Filter = "Text documents (.txt)|*.txt"; // Filter files by extensionSaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();


            //Nullable<bool> result = saveFileDialog.ShowDialog();
            //if (result == true)
            //{
            //    File. WriteAllText(saveFileDialog.FileName,"");
            //    string filename = saveFileDialog.FileName;
            //    _configurationListHelper.WriteActualConfiguration();
            //    _configurationListHelper.WriteListTofile(filename);
            //}
            //this.Close();
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            //    // Create OpenFileDialog 
            //    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            //    dlg.DefaultExt = ".txt";

            //    // Display OpenFileDialog by calling ShowDialog method 
            //    Nullable<bool> result = dlg.ShowDialog();


            //    // Get the selected file name and display in a TextBox 
            //    if (result == true)
            //    {
            //        // Open document 
            //        string filename = dlg.FileName;
            //        FileName.Text = filename;
            //        _configurationListHelper.ReadConfigurationFile(filename);

            //    }
            //    this.Close();
            //}


        }
    }
}
