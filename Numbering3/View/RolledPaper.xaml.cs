﻿using Numbering3.Helper.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Numbering3.View
{
    /// <summary>
    /// Interaction logic for RolledPaper.xaml
    /// </summary>
    public partial class RolledPaper : Window
    {
        public RolledPaper()
        {
            InitializeComponent();
            this.MouseLeftButtonDown += delegate { this.DragMove(); };
            DataContextChanged += new DependencyPropertyChangedEventHandler(Rolledpaper_DataContextChanged);

        }

        void Rolledpaper_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }
        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //Application.Current.MainWindow.Close();
            System.Windows.Application.Current.Shutdown();
            Close();
        }
    }
}
