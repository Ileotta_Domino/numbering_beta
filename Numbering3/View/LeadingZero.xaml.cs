﻿using Numbering3.Helper.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Numbering3.View
{
    /// <summary>
    /// Interaction logic for LeadingZero.xaml
    /// </summary>
    public partial class LeadingZero : Window
    {
        //private EventHandler CloseWindowEvent;

        public LeadingZero()
        {
            InitializeComponent();
            this.MouseLeftButtonDown += delegate { this.DragMove(); };
            DataContextChanged += new DependencyPropertyChangedEventHandler(LeadingZero_DataContextChanged);

        }

        void LeadingZero_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }


        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LeadIn_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void LeadInNumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void LeadOutNumber_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void LeadOutNumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void LeadInNumber_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BindingExpression binding = LeadInText.GetBindingExpression(TextBox.TextProperty);
            binding.UpdateSource();
        }

        private void StepBoxNumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^1-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void StepBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void RecordsOfPacks_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void RecordsOfPacksValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^1-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void EveryXSheetsNumberToShow_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void EveryXSheetsNumberToShowValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^1-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void RecordsForTextBetweenSheets_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void RecordsForTextBetweenSheetsValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^1-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
