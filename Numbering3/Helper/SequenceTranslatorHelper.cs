﻿using Numbering3.Helper.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper
{
    public class SequenceTranslatorHelper: ISequenceTranslatorHelper
    {
        public char prevchar;
        private readonly IListOfTypeProcessor _listOfTypeProcessor=new ListOfTypeProcessor();

        public void CalculateScalesInArray(char[] sequenceArray)
        {
            int counter=0; 
            char actualchar;

            if (sequenceArray.Length > 0)
            {

               _listOfTypeProcessor.CreateListOfType();
                counter = 1;
                prevchar = sequenceArray[0];
                _listOfTypeProcessor.AddToList(new TypeElement(1, sequenceArray[0]));
            }
            if (sequenceArray.Length > 1)
            {
                for (int i = 1; i < sequenceArray.Length; i++)
                {
                    actualchar = sequenceArray[i];
                    prevchar = sequenceArray[i - 1];
                    if (actualchar == prevchar)
                    {
                        _listOfTypeProcessor.RemoveLastInList();
                        counter++;
                        _listOfTypeProcessor.AddToList(new TypeElement(counter, actualchar));
                    }
                    else
                    {
                        counter = 1;
                        //     _listOfTypeProcessor.AddToStackOfType(new TypeElement(counter, actualchar));
                        _listOfTypeProcessor.AddToList(new TypeElement(counter, actualchar));
                    }
                }

            }
            _listOfTypeProcessor.ReverseTypeList();
        }



    }
}
