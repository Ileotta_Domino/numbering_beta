﻿using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Numbering3.Helper
{
    public class MatrixToDataViewConverter: IMultiValueConverter
    {

        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();


        // public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
          //   int[,,] values = _matrixProcessor.GetMatrix();
             var myDataTable = new DataTable();
           // DataTable dt = new DataTable();
            //  var colums = values[0] as string[];
            //var rows = values[1] as string[];
            //var vals = values[2] as string[][];
            int[,,] Tempmatrix = _matrixProcessor.GetMatrix();
            int X = _matrixProcessor.GetRow();
            int Y = _matrixProcessor.GetColumn();
            int Z = _matrixProcessor.GetSheet();


            List<List<string>> listOfOutput = new List<List<string>>();



         //   if (values[0] is null)
       //     {
                myDataTable.Columns.Add("---");    //The blanc corner column
                for (int n = 0; n <= Tempmatrix.GetUpperBound(2); n++)
                {
                    for (int i = 0; i <= Tempmatrix.GetUpperBound(0); i++)
                    {
                        myDataTable.Columns.Add(i.ToString(), typeof(double));
                        
                        for (int j = 0; j <= Tempmatrix.GetUpperBound(1); j++)
                        {
                            DataRow dr = myDataTable.NewRow();
                            for (int col = 0; col < Tempmatrix.GetUpperBound(1); col++)
                            {
                                dr[col] = col;
                            }
                            myDataTable.Rows.Add(dr);
                        }
                       
                            /********************************************************/
                            /*
                            for (int n = 0; n <= Matrix.GetUpperBound(2); n++)
                            {
                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                {
                                    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                    {


                              */          /********************************************************************/


                            //foreach (var value in colums)
                            //{
                            //    myDataTable.Columns.Add(value);
                            //}
                            //int index = 0;

                            //foreach (string row in rows)
                            //{
                            //    var tmp = new string[1 + vals[index].Count()];
                            //    vals[index].CopyTo(tmp, 1);
                            //    tmp[0] = row;
                            //    myDataTable.Rows.Add(tmp);
                            //    index++;
                            //}
                        }
                    }
             //   }
            return myDataTable.DefaultView;
        }

            
             //   return myDataTable.DefaultView;
            
                    

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
