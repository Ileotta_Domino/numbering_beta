﻿using Numbering3.Helper.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper
{
    public class AncillaryFunctions: IAncillaryFunctions
    {
        IWeightingProcessor _weightingProcessor = new WeightingProcessor();
        public  string IntToString(int value, char[] baseChars)
        {
            string result = string.Empty;
            int targetBase = baseChars.Length;

            do
            {
                result = baseChars[value % targetBase] + result;
                value = value / targetBase;
            }
            while (value > 0);

            return result;
        }

        public string SpacesToadd(int generalArraylength, string tosubtract)
        {
            string spacesline = "";
            for (int h = 0; h < generalArraylength - tosubtract.Length; h++)
            {
                spacesline = spacesline + "  ";
            }
            return spacesline;
        }

        public string fromCharArrayToString(char[] input)
        {
            string result = string.Join("", input);
            return result;
        }

        public string fromStringArrayToString(string[] input)
        {
            string result = string.Join("", input);
            return result;
        }
        public string fromArrayToString(int[] input)
        {
            string result = string.Join("", input);
            return result;
        }

        public int CalculateWightedResult(int input)
        {
            int stringtoint = 0;
            int[] WeightsArray = new int[12];
            WeightsArray = _weightingProcessor.GetWeightingArray();

            int[] flexdigits = new int[input.ToString().Length];
            for (int d = flexdigits.Length - 1; d >= 0; d--)
            {
                flexdigits[d] = input % 10;
                input /= 10;
            }

            for (int i = 0; i < flexdigits.Length; i++)
            {
                flexdigits[i] = flexdigits[i] * WeightsArray[i];
            }
            Int32.TryParse(fromArrayToString(flexdigits), out stringtoint);

            return stringtoint;
        }

        public int CheckDigitMod101and3Calculator(int input )
        {
            int output = 0;
            int[] flexdigits = new int[input.ToString().Length];
            int[] temparray= new int[input.ToString().Length+1];
            int i = 0;
            for (int d = flexdigits.Length - 1; d >= 0; d--)
            {
                flexdigits[d] = input % 10;
                input /= 10;
            }
            while (i < flexdigits.Length)
            {
                if (i % 2 == 1)
                    flexdigits[i] = flexdigits[i] * 3;
                i++;
                    }
            output= CalculateMod10(flexdigits);
            return output;
        }


        public int CheckDigitMod103and1Calculator(int input)
        {
            int output = 0;
            int[] flexdigits = new int[input.ToString().Length];
            int[] temparray = new int[input.ToString().Length + 1];
            int i = 0;
            for (int d = flexdigits.Length - 1; d >= 0; d--)
            {
                flexdigits[d] = input % 10;
                input /= 10;
            }
            while (i < flexdigits.Length)
            {
                if (i % 2 == 0)
                    flexdigits[i] = flexdigits[i] * 3;
                i++;
            }
            output = CalculateMod10(flexdigits);
            return output;
        }

        public int CalculateMod10(int[] input)
        {
            int result = 0;
            result = input.Sum()%10;

            if (result != 0)
            {
                result = 10-result;
            }
            return result;
        }
    }
}
