﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper.Interface
{
    public interface ISequenceTranslatorHelper
    {
        void CalculateScalesInArray(char[] sequenceArray);
    }
}
