﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper.Interface
{
    public interface IAncillaryFunctions
    {
        string IntToString(int value, char[] baseChars);
        string SpacesToadd(int generalArraylength, string tosubtract);
        string fromCharArrayToString(char[] input);
        string fromStringArrayToString(string[] input);
        string fromArrayToString(int[] input);
        int CalculateWightedResult(int input);
        int CheckDigitMod103and1Calculator(int input);
        int CheckDigitMod101and3Calculator(int input);

    }
}
