﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper.Interface
{
    public interface ILineIndex
    {
        string AssignIndex(int counter, int[] lineIndex, int Arraylength, TextWriter tw);
    }
}
