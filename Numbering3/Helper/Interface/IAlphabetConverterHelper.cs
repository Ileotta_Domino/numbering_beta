﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper.Interface
{
    public interface IAlphabetConverterHelper
    {
     //   string formatValue(int i, int outputlength, char type);
        int CalculateScale(int inputint, char inputchar);
        string FormattingIterating(int inputnumber, int iteration, string stringtoadd);
        string FormatValue(int input, TypeElement inputtype);
        int CalculateMaxValueToBeWritten();
        char[] makeBaseArray();
        int UnformatValue(string input);
    }
}
