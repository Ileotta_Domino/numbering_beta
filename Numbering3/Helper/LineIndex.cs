﻿using Numbering3.Helper.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper
{
    public class LineIndex: ILineIndex
    {

        public string AssignIndex(int counter, int[] lineIndex, int Arraylength,TextWriter tw)
        {
            // write Line Index
            Array.Clear(lineIndex, 0, lineIndex.Length);
            //int indexval = counter;
            //int indexCount = 0;
            string tempRow;

            int[] zeroarr=new int[Arraylength];
            int indexCount = 0;
            int indexval = counter;
            for (; indexval != 0; indexval /= 10)
            { 

                lineIndex[indexCount] = indexval % 10;
                indexCount++;
            }
            Array.Reverse(lineIndex);
            tempRow = fromArrayToString(lineIndex) + "   ";
            for (int q = 0; q < lineIndex.Length; q++)
                tw.Write("{0}", lineIndex[q]);
            tw.Write("{0}", "   ");

         //   tempRow = tempRow + fromArrayToString(lineIndex) + "   ";
           
            for (int q = 0; q < lineIndex.Length; q++)
                tw.Write("{0}", zeroarr[q]);
            tw.Write("{0}", "   ");
            tempRow = tempRow + fromArrayToString(zeroarr) + "   ";
            return tempRow;

            //end line index
        }

        public string fromArrayToString(int[] input)
        {
            string result = string.Join("", input);
            return result;
        }
    }
}
