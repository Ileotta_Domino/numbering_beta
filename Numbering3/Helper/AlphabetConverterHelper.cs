﻿using Numbering3.Helper.Interface;
using Numbering3.Model;
using Numbering3.Model.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper
{

    public class AlphabetConverterHelper : IAlphabetConverterHelper
    {
        private readonly ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        private readonly IListOfTypeProcessor _listOfTypeProcessor = new ListOfTypeProcessor();
        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();

        //  private readonly IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        //       private List<TypeElement> ListOfType = new List<TypeElement>();
        private Stack<TypeElement> StackOfType = new Stack<TypeElement>();

        string stringtoadd="";
        int outputlength = 0;
        int stringtoint = 0;
        int k = 0;
        string finalstring = "";
        char[] tempcharArray;



        public char[] makeBaseArray()
        {
            
            int CountArrayLength = 0;
            List<TypeElement> tempList = _listOfTypeProcessor.ListOfTypeValue;
            foreach (var elem in tempList)
            {
                CountArrayLength+= elem.LengtOfSameElement;
            }
            char[] ResultArray = new char[CountArrayLength];
            int index = CountArrayLength - 1;

            foreach (var elem in tempList)
            {
                for (int i = 0; i < elem.LengtOfSameElement; i++)
                {
                    string alphabet = GetAlphabetSequence(elem.Type);
                    ResultArray[index--] = alphabet[0];
                }  
            }
            return ResultArray;
        }


        public string FormatValue(int input, TypeElement inputtype)
        {
            const int BitsInLong = 64;
            string Alphabet = GetAlphabetSequence(inputtype.Type);
            char[] baseChars = Alphabet.ToCharArray();
            int targetBase = baseChars.Length;
            int radix = Alphabet.Length;
            outputlength = inputtype.LengtOfSameElement;
           // string result;
            int quoz = 0;
            int i = 32;
            char[] result;

            char[] buffer = new char[i] ;
            for (int j = 0; j < 32; j++)
            {
                buffer[j] = Alphabet[0];
            }

            do
            {
                buffer[--i] = baseChars[input % targetBase];
                input = input / targetBase;
            }
            while (input > 0);
            if (_matrixProcessor.GetIsChecked()==true) {

                result = new char[outputlength];
                Array.Copy(buffer, 32 - outputlength, result, 0, outputlength);
            }
            else {
                result = new char[32 - i];
                Array.Copy(buffer, i, result, 0, 32 - i);
            }
                //  Array.Copy(buffer, i, result, 0, 32 - i);

            return new string(result);
        }

        public int CalculateMaxValueToBeWritten()
        {
            List<TypeElement> ListOfType = new List<TypeElement>();
            int CalculateMaxValueToBeWritten = 0;

            ListOfType = _listOfTypeProcessor.ListOfTypeValue;
            foreach (var elem in ListOfType)
            {
                string Alphabet = GetAlphabetSequence(elem.Type);
                int lengthAlphabet = Alphabet.Length;
                int actualMax =(int) Math.Pow(lengthAlphabet, elem.LengtOfSameElement)-1;

                CalculateMaxValueToBeWritten += actualMax;
            }


            return CalculateMaxValueToBeWritten;
        }

        
        public string GetAlphabetSequence(char inputchar)
        {
            string Alphabet = null;
            string AlphabetC = null;
            switch (inputchar)
            {
                case 'B':
                    Alphabet = _sequenceProcessor.GetAlphabetToKeepValue();
                    break;
                case 'C':
                    Alphabet = _sequenceProcessor.GetAlphabetCToKeepValue();
                    break;
                case 'N':
                    Alphabet = "0123456789";
                    break;
                case 'A':
                    Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    break;
            }
            return Alphabet;
        }
        public int CalculateScale(int inputint, char inputchar)
        {
            int maxvalue = 0;
            switch (inputchar)
            {

                //            if (inputchar == 'N')
                case 'B':
                    maxvalue = (int) Math.Pow (_sequenceProcessor.GetSequenceLength(), inputint )-1;
                    //    maxvalue = _sequenceProcessor.GetSequenceLength()^inputint;
                    break;
                case 'N':
                    maxvalue =(int)Math.Pow(10 , inputint) - 1;
                    break;
                case 'A':
                    maxvalue = (int)Math.Pow(26 , inputint)-1;
                    break;
            }
            return maxvalue;
        }

        public string fromArrayToString(int[] input)
        {
            string result = string.Join("", input);
            return result;
        }


        public string FormattingIterating(int inputnumber, int iteration, string stringtoadd)
        {
            // string result="";
            List<TypeElement> ListOfType = new List<TypeElement>();
            ListOfType = _listOfTypeProcessor.ListOfTypeValue;

            k = iteration;
            int countToDisplay = 0;
            int DominantRemainde = 0;
            finalstring = "";
            TypeElement TypeElelemntToPush = new TypeElement(0, 'N');
            TypeElement tempTypeElement = new TypeElement(0, 'N');
            TypeElement NextTypeElement = new TypeElement(0, 'N');

            //     if (k < ListOfType.Count())
            if (k < _listOfTypeProcessor.CountTypeList())
            {

                int maxValue = 0;
                tempTypeElement = ListOfType[k];

                maxValue = CalculateScale(tempTypeElement.LengtOfSameElement, tempTypeElement.Type);

                if (maxValue < 0)
                {
                    int tocheck = maxValue;
                }

                if (inputnumber < 0)
                {
                    int inputtocheck = inputnumber;
                }


                if (inputnumber > maxValue)
                {
                    tempTypeElement = ListOfType[k];
                    countToDisplay = (inputnumber % (maxValue + 1));
                    int alphabetlength = GetAlphabetSequence(tempTypeElement.Type).Length;
                    //   DominantRemainde =(int)Math.Floor((inputnumber - (double) countToDisplay) / maxValue);
                    DominantRemainde = (int)Math.Floor(((double)inputnumber / (maxValue + 1)));
                    if (countToDisplay == 0)
                    {
                        stringtoadd = FormatValue(inputnumber % (alphabetlength-1), tempTypeElement);
                        //countToDisplay = countToDisplay % 8;
                        //  stringtoadd = FormatValue(inputnumber % 9, tempTypeElement);
                        //      stringtoadd = FormatValue(maxValue-(inputnumber/maxValue)+1, tempTypeElement);
                    }
                    else
                    {
                        stringtoadd = FormatValue(countToDisplay, tempTypeElement);
                    }
                    k++;
                    finalstring = FormattingIterating(DominantRemainde, k, stringtoadd);
                }
                else
                {
                    finalstring = FormatValue(inputnumber, tempTypeElement) + stringtoadd;
                }

            }
            return finalstring;
        }



        public int UnformatValue(string input)
        {
            int k = 0;
            double result = 0;
            char[] inputArray = input.ToCharArray();
            char[] sequencearray = _sequenceProcessor.GetSequence().ToCharArray() ;
            TypeElement tempTypeElement = new TypeElement(0, 'N');
            List<TypeElement> ListOfType = new List<TypeElement>();
            ListOfType = _listOfTypeProcessor.ListOfTypeValue;
            string Alphabet = "";
            int[] tempvalArray = new int[input.Length];
            int[] valuesArray = new int[input.Length];

            if (input == "0")
            {
                result = 0;
                //for (int n = 0; n < sequencearray.Length; n++)
                //{ valuesArray[n] = "0"; }
            }
            else
            {

                for (int q = 0; q < input.Length; q++)
                {
                    char typecurr = sequencearray[sequencearray.Length - 1 - q];
                    string tempAlphabet = "";
                    tempAlphabet = GetAlphabetSequence(typecurr);
                    valuesArray[valuesArray.Length - 1 - q] = tempAlphabet.Length;
                }
                Array.Copy(valuesArray, tempvalArray, valuesArray.Length);

                for (int m = tempvalArray.Length - 1; m > 0; m--)
                {
                    tempvalArray[m - 1] = tempvalArray[m] * tempvalArray[m - 1];
                }

                for (int i = inputArray.Length - 1; i >= 0; i--)
                {

                    char type = sequencearray[sequencearray.Length - 1 - k];
                    Alphabet = GetAlphabetSequence(type);

                    char elem = inputArray[i];
                    int valelem = Alphabet.IndexOf(elem);

                    int j = valuesArray.Length - 1;

                    if (k == 0)
                    {
                        result = Alphabet.IndexOf(elem);
                    }
                    else if (k == 1)
                    {
                        //    Alphabet = GetAlphabetSequence(elem);
                        result = result + Alphabet.IndexOf(elem) * Alphabet.Length;
                    }
                    else
                    {

                        result = result + Alphabet.IndexOf(elem) * tempvalArray[tempvalArray.Length - k];
                        //  result = result + Alphabet.IndexOf(elem) * valuesArray[valuesArray.Length  - k];
                        //   k++;
                    }
                    k++;
                }
            }
            return (int) result;
        }

        



    }
}
