﻿using Numbering3.Helper.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper
{
    public class HorizontalCalculator
    {
        
        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        private readonly ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        private readonly IListRowsProcessor _listRowsProcessor = new ListRowsProcessor();
        private readonly IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        private readonly IAncillaryFunctions _ancillaryFunctions = new AncillaryFunctions();
        private string ValueStringtoAdd = "";


        public void NewCalculatorHorizontal()
        {
            int[,,] Matrix;

            Matrix = _matrixProcessor.GetMatrix();
            int sh = Matrix.GetUpperBound(2);
            int col = Matrix.GetUpperBound(1);
            int ro = Matrix.GetUpperBound(0);
            int pa = _matrixProcessor.GetPack();
            int sta = _matrixProcessor.GetStack();
            int StartPoint = 0;
            int Gridcounter ;
            int ResultLength = _sequenceProcessor.GetSequenceLength();
            int step;
            int val;
            int correction=0;
            int[] flexdigits;
            int[] ArrayBase = new int[ResultLength];
            bool BasechararrayOption = _matrixProcessor.GetAddZeroeValue();
            string finalstring="";
            string index;
            string unpaddedelement;
            string elementstring;
            string newLine;
            string pathBis = @".\NumberingTestBis.txt";

            StartPoint = (_alphabetConverterHelper.UnformatValue(_matrixProcessor.GetStartPoint().ToString()));
            step = 1;
            Gridcounter = 0;

        using (StreamWriter streamwriter = File.AppendText(pathBis))
                for (int p = 0; p < _matrixProcessor.GetPack(); p++)
            {

                for (int v = 0; v < _matrixProcessor.GetStack(); v++)
                {
                    for (int n = 0; n <= sh; n++)
                    {
                        for (int j = 0; j <= col; j++)
                        {
                            finalstring = "";
                            for (int i = 0; i <= ro; i++)
                            {

                                val = StartPoint + i + col * j + (col * ro * n)+correction;

                                flexdigits = new int[val.ToString().Length];
                                for (int d = flexdigits.Length - 1; d >= 0; d--)
                                {
                                    flexdigits[d] = val % 10;
                                    val /= 10;
                                }

                                unpaddedelement = fromArrayToString(flexdigits);

                                //if (BasechararrayOption == true)
                                //{
                                //    Array.Clear(ArrayBase, 0, ArrayBase.Length);
                                //    int startarr=ArrayBase.Length - flexdigits.Length;
                                //    Array.Copy(flexdigits, 0, ArrayBase, startarr, flexdigits.Length);

                                //}


                                elementstring = unpaddedelement.PadLeft(ResultLength, '0');
                                finalstring = finalstring + "    " + elementstring;



                            }
                            index = Gridcounter.ToString();
                            newLine = index.PadLeft(8, '0');
                            streamwriter.WriteLine(newLine+"000000     "+"01"+finalstring);
                            Gridcounter++;
                        }
                    }


                }
            }
        }

        public string fromArrayToString(int[] input)
        {
            string result = string.Join("", input);
            return result;
        }

    }
}
