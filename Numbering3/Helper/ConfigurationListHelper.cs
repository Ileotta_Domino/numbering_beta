﻿
using Numbering3.Helper.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper
{
    public class ConfigurationListHelper:IConfigurationListHelper
    {
        IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        ISaveProcessor _saveProcessor = new SaveProcessor();
        ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        public void WriteActualConfiguration()
        {
            string Rows = "0";
            string Columns = "0";
            string Sheets = "0";
            string Repetition = "0";
            string ChoosenOrder = "";
            string LeadIn = "None";
            string LeadInNumber = "0";
            string LeadOut = "None";
            string LeadOutNumber = "0";
            string Alphabet = "abc";
            string Sequence = "NNNNNN";
            string ModulusTypeSelectedName = "+";
            string ModulusTypePositionValue = "Beginning";
            int RecordsOfPacksValue = 1;
            string TextBetweenPacks = "";
            int EveryXSheets = 0;
            int RecordsOfSheets = 0;
            string TextBetweenSheetsToShow = "";
            int MultiplyByColumn = 1;
            int MultiplyBySheet = 1;
            string TextBetweenPacksToKeep = "None";
            int RecordsOfPacksToKeep = 0;
            int EveryXSheetsToKeep = 1;
            string TextBetweenSheetsToKeep = "None";
            int RecordsOfSheetToKeep = 0;

            string Modulus = "Modulus";
            _saveProcessor.CreateList();

            if (_matrixProcessor.GetRow()!=1)
            { Rows = _matrixProcessor.GetRow().ToString(); }
            _saveProcessor.AddToConfigurationList("Rows," + Rows);

            if (_matrixProcessor.GetColumn() != null)
            { Columns = _matrixProcessor.GetColumn().ToString(); }
            _saveProcessor.AddToConfigurationList("Columns," + Columns);

            if (_matrixProcessor.GetSheet() != null)
            { Sheets = _matrixProcessor.GetSheet().ToString(); }
            _saveProcessor.AddToConfigurationList("Sheets," + Sheets);

            if (_matrixProcessor.GetRepetitionNumber() != null)
            { Repetition = _matrixProcessor.GetRepetitionNumber().ToString(); }
            _saveProcessor.AddToConfigurationList("Repetition," + Repetition);

            if (_matrixProcessor.GetOrder() != null)
            { ChoosenOrder = _matrixProcessor.GetOrder(); }
            _saveProcessor.AddToConfigurationList("ChoosenOrder," + ChoosenOrder);

            if (_matrixProcessor.GetLeadInArray() != null)
            { LeadIn = fromCharArrayToString(_matrixProcessor.GetLeadInArray()); }
            _saveProcessor.AddToConfigurationList("LeadIn," + LeadIn);

            if (_matrixProcessor.GetNumberOfLeadIn() != 0)
            { LeadInNumber = _matrixProcessor.GetNumberOfLeadIn().ToString(); }
            _saveProcessor.AddToConfigurationList("LeadInNumber," + LeadInNumber);

            if (_matrixProcessor.GetLeadOutArray() != null)
            { LeadOut = fromCharArrayToString(_matrixProcessor.GetLeadOutArray()); }
            _saveProcessor.AddToConfigurationList("LeadOut," + LeadOut);

            if (_matrixProcessor.GetNumberOfLeadOut() != 0)
            { LeadOutNumber = _matrixProcessor.GetNumberOfLeadOut().ToString(); }     
            _saveProcessor.AddToConfigurationList("LeadOutNumber," + LeadOutNumber);

            if (_sequenceProcessor.GetAlphabet() != null)
            { Alphabet = _sequenceProcessor.GetAlphabet(); }
            _saveProcessor.AddToConfigurationList("Alphabet," + Alphabet);

            if (_sequenceProcessor.GetAlphabet() != null)
            { Alphabet = _sequenceProcessor.GetAlphabet(); }
            _saveProcessor.AddToConfigurationList("AlphabetC," + Alphabet);


            if (_sequenceProcessor.GetSequence() != null)
            { Sequence = _sequenceProcessor.GetSequence(); }
            _saveProcessor.AddToConfigurationList("Sequence," + Sequence);

            if (_matrixProcessor.GetBitCheck() != null)
            { Modulus = _matrixProcessor.GetBitCheck(); }
            _saveProcessor.AddToConfigurationList("Modulus," + Modulus);

            if (_matrixProcessor.GetModulusType() != null)
            { ModulusTypeSelectedName = _matrixProcessor.GetBitCheck(); }
            _saveProcessor.AddToConfigurationList("ModulusTypeSelectedName," + ModulusTypeSelectedName);

            if (_matrixProcessor.GetModulusPosition() != null)
            { ModulusTypePositionValue = _matrixProcessor.GetModulusPosition(); }
            _saveProcessor.AddToConfigurationList("ModulusTypePositionValue," + ModulusTypePositionValue);

            if (_matrixProcessor.GetRecordsOfPacksNumber() != 0)
            { RecordsOfPacksValue = _matrixProcessor.GetRecordsOfPacksNumber(); }
            _saveProcessor.AddToConfigurationList("RecordsOfPacksValue," + RecordsOfPacksValue.ToString());

            if (_matrixProcessor.GetTextBetweenPacks() != null)
            { TextBetweenPacks = _matrixProcessor.GetTextBetweenPacks(); }
            _saveProcessor.AddToConfigurationList("TextBetweenPacks," + TextBetweenPacks);

            if (_matrixProcessor.GetTextBetweenPacks() != null)
            { TextBetweenPacks = _matrixProcessor.GetTextBetweenPacks(); }
            _saveProcessor.AddToConfigurationList("TextBetweenPacks," + TextBetweenPacks);

            if (_matrixProcessor.GetEveryXSheetsValue() != 0)
            { EveryXSheets = _matrixProcessor.GetEveryXSheetsValue(); }
            _saveProcessor.AddToConfigurationList("EveryXSheets," + EveryXSheets.ToString());

            if (_matrixProcessor.GetRecordsOfSheetsNumber() != 0)
            { RecordsOfSheets = _matrixProcessor.GetEveryXSheetsValue(); }
            _saveProcessor.AddToConfigurationList("EveryXSheets," + RecordsOfSheets.ToString());

            if (_matrixProcessor.GetTextBetweenSheets() != "")
            { TextBetweenSheetsToShow = _matrixProcessor.GetTextBetweenSheets(); }
            _saveProcessor.AddToConfigurationList("TextBetweenSheetsToShow," + TextBetweenSheetsToShow);

            if (_matrixProcessor.GetMultiplyByColumnValue() != 1)
            { MultiplyByColumn = _matrixProcessor.GetMultiplyByColumnValue(); }
            _saveProcessor.AddToConfigurationList("MultiplyByColumn," + MultiplyByColumn.ToString());

            if (_matrixProcessor.GetMultiplyBySheetValue() != 1)
            { MultiplyBySheet = _matrixProcessor.GetMultiplyBySheetValue(); }
            _saveProcessor.AddToConfigurationList("MultiplyBySheet," + MultiplyBySheet.ToString());

            if (_matrixProcessor.GetTextBetweenPacks() != "None")
            { TextBetweenPacksToKeep = _matrixProcessor.GetTextBetweenPacks(); }
            _saveProcessor.AddToConfigurationList("TextBetweenPacksToKeep," + TextBetweenPacksToKeep);

            if (_matrixProcessor.GetRecordsOfPacksNumber() != 0)
            { RecordsOfPacksToKeep = _matrixProcessor.GetRecordsOfPacksNumber(); }
            _saveProcessor.AddToConfigurationList("RecordsOfPacksToKeep," + RecordsOfPacksToKeep.ToString());

            if (_matrixProcessor.GetEveryXSheetsValue() != 0)
            { EveryXSheetsToKeep = _matrixProcessor.GetEveryXSheetsValue(); }
            _saveProcessor.AddToConfigurationList("EveryXSheetsToKeep," + EveryXSheetsToKeep.ToString());

            if (_matrixProcessor.GetTextBetweenSheets() != "None")
            { TextBetweenSheetsToKeep = _matrixProcessor.GetTextBetweenSheets(); }
            _saveProcessor.AddToConfigurationList("TextBetweenSheetsToKeep," + TextBetweenSheetsToKeep);

            if (_matrixProcessor.GetRecordsOfSheetsNumber() != 0)
            { RecordsOfSheetToKeep = _matrixProcessor.GetRecordsOfSheetsNumber(); }
            _saveProcessor.AddToConfigurationList("RecordsOfSheetToKeep," + RecordsOfSheetToKeep.ToString());
            


        }

        public void ReadConfigurationFile(string filename)
            {
            string[] words;
            List<string> _configFileCopy = new List<string>();
            foreach (string line in File.ReadLines(filename))
            {     words=line.Split(',');
                if (words[0] == "Rows")
                {
                    _matrixProcessor.SetRow(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "Columns")
                {
                    _matrixProcessor.SetColumn(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "Sheets")
                {
                    _matrixProcessor.SetSheet(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "Repetition")
                {
                    _matrixProcessor.SetRepetitionNumber(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "ChoosenOrder")
                {
                    _matrixProcessor.SetOrder(words[1]);
                }
                if (words[0] == "LeadIn")
                {
                    _matrixProcessor.SetLeadInArray(words[1].ToCharArray());
                }
                if (words[0] == "LeadInNumber")
                {
                    _matrixProcessor.SetNumberOfLeadIn(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "LeadOut")
                {
                    _matrixProcessor.SetLeadOutArray(words[1].ToCharArray());
                }
                if (words[0] == "LeadOutNumber")
                {
                    _matrixProcessor.SetNumberOfLeadOut(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "Alphabet")
                {
                    _sequenceProcessor.SetAlphabet ((string)words[1]);
                }
                if (words[0] == "AlphabetC")
                {
                    _sequenceProcessor.SetAlphabetC((string)words[1]);
                }
                if (words[0] == "Sequence")
                {
                    _sequenceProcessor.SetSequence((string)words[1]);
                }
                if (words[0] == "Modulus")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "ModulusTypeSelectedName")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "ModulusTypePositionValue")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "RecordsOfPacksValue")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "TextBetweenPacks")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "EveryXSheets")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "RecordsOfSheets")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "TextBetweenSheetsToShow")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "MultiplyByColumn")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "MultiplyBySheet")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "TextBetweenPacksToKeep")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "RecordsOfPacksToKeep")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "EveryXSheetsToKeep")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "TextBetweenSheetsToKeep")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
                if (words[0] == "RecordsOfSheetToKeep")
                {
                    _matrixProcessor.SetBitCheck((string)words[1]);
                }
            }

        }


        public void WriteListTofile(string filename)
        {
            string pathBis = filename;

            using (StreamWriter twBis = File.CreateText(pathBis))
                foreach (var elem in _saveProcessor.GetConfigurationList())
            {
                twBis.WriteLine("{0}", elem);
            }
        }

        public string fromCharArrayToString(char[] input)
        {
            string result = string.Join("", input);
            return result;
        }
    }
}
