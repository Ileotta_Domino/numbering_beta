﻿using Numbering3.Helper.Interface;
using Numbering3.Model;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper
{
    public class AlgorithmCalculatorHelper : IAlgorithmCalculatorHelper
    {
        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        private readonly ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        private readonly IListRowsProcessor _listRowsProcessor = new ListRowsProcessor();
        private readonly IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        private readonly IAncillaryFunctions _ancillaryFunctions = new AncillaryFunctions();
        private string ValueStringtoAdd = "";
        public ValuesToShow _ValueToShow { get; set; }


        public void Calculator()
        {
            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string pathBis = Path.Combine(location, "NumberingTestBis.txt");
            //    using (StreamWriter twBis = File.CreateText(pathBis))
            if (_matrixProcessor.GetAlignment() == "Vertical")
            { NewCalculatorVertical(); }
             else {
             //    CalculatorHorizontal();
                NewCalculatorHorizontal();
            }
            
        }
        
        public void NewCalculatorHorizontal()
        {
            int[,,] Matrix;
            Matrix = _matrixProcessor.GetMatrix();
            int sh = Matrix.GetUpperBound(2);
            int col = Matrix.GetUpperBound(1);
            int ro = Matrix.GetUpperBound(0);
            int pa = _matrixProcessor.GetPack();
            int sta = _matrixProcessor.GetStack();
            int StartPoint = 0;
            int Gridcounter;
            int ResultLength = _sequenceProcessor.GetSequenceLength();
            int step;
            int LeadInNumber = _matrixProcessor.GetNumberOfLeadIn();
            int val;
            int MaxValuePossible = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
            int correction = 0;
            int[] flexdigits;
            int[] ArrayBase = new int[ResultLength];
            bool BasechararrayOption = _matrixProcessor.GetAddZeroeValue();
            string finalstring = "";
            string index;
            string unpaddedelement;
            string elementstring;
            string newLine;
            string modulus = "";
            string LeadInLine = "";
            string LeadOutLine = "";
            int NumberOfRecordsPerSheet = _matrixProcessor.GetRecordsOfSheetsNumber();
            string TextBetweenSheets= _matrixProcessor.GetTextBetweenSheets();
            string TestBetweenSheetsLine = "";
            string LeadInValue = _matrixProcessor.GetLeadInToKeepValue();
            string LeadOutValue = _matrixProcessor.GetLeadOutToKeepValue();
            int ColumnMultiplierInt = _matrixProcessor.GetMultiplyByColumnValue();
            string ColumnMultiplier = FromintToStringWithZero(2, ColumnMultiplierInt);
            int _SheeMultiplier = _matrixProcessor.GetMultiplyBySheetValue();
            int everyx = _matrixProcessor.GetEveryXSheetsValue();
            int LeadOutNumber = _matrixProcessor.GetNumberOfLeadOut();
            int numberofRecordAfterPack = _matrixProcessor.GetRecordsOfPacksNumber();
            string afterEndLine = "";
            string AfterPackText = _matrixProcessor.GetTextBetweenPacks();

            //   string pathBis = @".\NewNumberingTestBisTest.txt";

            StartPoint = (_alphabetConverterHelper.UnformatValue(_matrixProcessor.GetStartPoint().ToString()));
            step = 1;
            Gridcounter = 0;
            using (StreamWriter streamwriter = File.CreateText(@".\NewNumberingTestBisTest.txt"))
           // using (StreamWriter streamwriter = File.AppendText(pathBis))
                for (int p = 0; p < pa; p++)
                {

                    for (int v =0; v < sta; v++)
                    {
                        
                        for (int k = 0; k < LeadInNumber; k++)
                        {
                            
                            for (int j = 0; j <= col; j++)
                            {

                                LeadInLine = "";
                                for ( int i = 0; i <= ro; i++)
                                {
                                    LeadInLine= LeadInLine+ LeadInValue.PadLeft(16, ' '); 

                                }

                                index = Gridcounter.ToString();
                                newLine = index.PadLeft(8, '0');
                                streamwriter.WriteLine(newLine + "  000000     " + ColumnMultiplier + "      " + LeadInLine);
                                _listRowsProcessor.ListOfRowsAdd(newLine + "  000000     " + ColumnMultiplier + "      " + LeadInLine);
                                Gridcounter++;
                            }
                        }
                    

                            for (int n = 0; n <= sh; n++)
                        {
                            //Sheet Multiplier 
                            for (int mult = 1; mult <= _SheeMultiplier; mult++)
                            {

                                for (int j = 0; j <= col; j++)
                                {
                                    finalstring = "";
                                    for (int i = 0; i <= ro; i++)
                                    {
                                        correction = v * ((col + 1) * (ro + 1) * (sh + 1)) + p * (sta* ((col + 1) * (ro + 1) * (sh + 1)));
                                        val = StartPoint + i + (ro + 1) * j + ((col + 1) * (ro + 1) * n) + correction;

                                        ///Calculate Weighting and Modulus
                                        ///
                                        val = _ancillaryFunctions.CalculateWightedResult(val);

                                        if (val >= MaxValuePossible)
                                        {
                                            while (val >= MaxValuePossible)
                                            {
                                                val = val - MaxValuePossible;
                                            }
                                        }
                                        if (_matrixProcessor.GetCheckDigitValue())
                                        {
                                            if (_matrixProcessor.GetModulusType() == "Module")
                                            {
                                                modulus = CalculateModulus(val);
                                            }
                                            else if (_matrixProcessor.GetModulusType() == "Modulus w/o 0")
                                            {
                                                modulus = CalculateModulus(val - 1);
                                            }
                                            else if (_matrixProcessor.GetModulusType() == "Complement")
                                            {
                                                modulus = CalculateComplement(val);
                                            }
                                            else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
                                            {
                                                modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
                                            }
                                            else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
                                            {
                                                modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
                                            }
                                        }
                                        ///End Calculate Weighting and Modulus



                                        flexdigits = new int[val.ToString().Length];
                                        for (int d = flexdigits.Length - 1; d >= 0; d--)
                                        {
                                            flexdigits[d] = val % 10;
                                            val /= 10;
                                        }

                                        elementstring = fromArrayToString(flexdigits);



                                        if (_matrixProcessor.GetModulusPosition() == "Beginning")
                                        {
                                            elementstring = modulus + elementstring; 
                                        }
                                        else
                                        {
                                            elementstring = modulus + elementstring; 
                                        }


                                        if (_matrixProcessor.GetAddZeroeValue())
                                        {

                                            ////Check Zero or not padded
                                            elementstring = elementstring.PadLeft(ResultLength, '0');
                                        }
                                        else {
                                            elementstring = elementstring.PadLeft(ResultLength, ' ');
                                        }

                                        //Assign Labels

                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueAlphaValue(elementstring);
                                            _matrixProcessor.SetValueRolledAlphaValue(elementstring);
                                        }
                                        if (i == 0 && j == 1 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueAlphaZeroOneValue(elementstring);
                                        }
                                        if (i == 1 && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueAlphaOneZeroValue(elementstring);
                                            _matrixProcessor.SetValueRolledBetaValue(elementstring);
                                        }
                                        if (i == 0 && j == col && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueBetaValue(elementstring);
                                        }

                                        if (i == ro && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueGammaValue(elementstring);
                                            _matrixProcessor.SetValueRolledGammaValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueDeltaValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueEpsilonValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == sh && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaValue(elementstring);
                                            _matrixProcessor.SetValueRolledEpsilonValue(elementstring);
                                        }
                                        if (i == 0 && j == 1 && n == sh && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaZeroOneValue(elementstring);
                                        }
                                        if (i == 0 && j == col && n == sh && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaZeroLastValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == 0)
                                        {
                                            _matrixProcessor.SetValueZetaValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && sta !=0 && v == sta - 1 && sta - 1 > 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueEtaValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == sta - 1 && sta - 1>0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueThetaValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == 1 )
                                        {
                                            _matrixProcessor.SetValueIotaValue(elementstring);
                                            _matrixProcessor.SetValueRolledZetaValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == 0 && p == 1 )
                                        {
                                            _matrixProcessor.SetValueKappaValue(elementstring);
                                        }

                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == 1 )
                                        {
                                            _matrixProcessor.SetValueLambdaValue(elementstring);
                                        }

                                        if (i == 0 && j == 0 && n == 0 && v == sta-1 && sta - 1 > 0 && p == 1 )
                                        {
                                            _matrixProcessor.SetValueMuValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == sta - 1 && sta - 1 > 0 && p == 1 )
                                        {
                                            _matrixProcessor.SetValueNuValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == pa - 1 && pa - 1 > 0)
                                        {
                                            _matrixProcessor.SetValueXiValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == 0 && p == pa-1 && pa - 1 > 0)
                                        {
                                            _matrixProcessor.SetValueOmicronValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == pa - 1 && pa - 1 > 0)
                                        {
                                            _matrixProcessor.SetValuePiValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == sta - 1 && sta - 1 >0 && p == pa-1 && pa - 1 > 0)
                                        {
                                            _matrixProcessor.SetValueRoValue(elementstring);
                                        }
                                        //if (i == ro && j == 0 && n == 0 && v == 0 && p == pa - 1)
                                        //{
                                        //    _matrixProcessor.SetValueSigmaValue(elementstring);
                                        //}
                                        if (i == ro && j == col && n == sh && v == sta-1 && sta - 1 > 0 && p == pa - 1 && pa - 1 > 0)
                                        {
                                            _matrixProcessor.SetValueOmegaValue(elementstring);
                                        }

                                        //End Assign Labels

                                        ///  Assign Labels Rolled Paper Part/////

                                        if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledDeltaValue(elementstring);
                                        }

                                        if (i == ro && j == 0 && n == 0 && p == 1 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledEtaValue(elementstring);
                                        }
                                        /// End Assign Labels Rolled Paper Part/////
                                                                                
                                    //    finalstring = finalstring + "      "  + elementstring;
                                        finalstring = finalstring + elementstring.PadLeft(16, ' ');
                                    }
                                    index = Gridcounter.ToString();
                                    newLine = index.PadLeft(8, '0');
                                    streamwriter.WriteLine(newLine + "  000000     " + ColumnMultiplier + "      " + finalstring);
                                    _listRowsProcessor.ListOfRowsAdd(newLine + "  000000     " + ColumnMultiplier + "      " + finalstring);
                                    Gridcounter++;
                                }
                            }

                        ///test every sheet
                       
                        if (everyx != 0 && (n + 1) % everyx == 0 && (n + 1) % sh != 0)

                        {
                               
                            for (int k = 0; k < NumberOfRecordsPerSheet; k++)
                            {
                                //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                                //           {
                                for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                {

                                        TestBetweenSheetsLine = "";
                                        for (int i = 0; i <= ro; i++)
                                    {
                                        TestBetweenSheetsLine = TestBetweenSheetsLine + TextBetweenSheets.PadLeft(16, ' ');
                                    }
                                        index = Gridcounter.ToString();
                                        index = index.PadLeft(8, '0');
                                        streamwriter.WriteLine(index + "  000000     " + ColumnMultiplier + "      " + TestBetweenSheetsLine);
                                        _listRowsProcessor.ListOfRowsAdd(index + "  000000     " + ColumnMultiplier + "      " + TestBetweenSheetsLine);

                                        Gridcounter++;
                                }
                            }
                        }

                        }
                        ///end test every sheet


                        //////////////////////////Lead out //////////////////////////////////////


                        
                                for (int k = 0; k < LeadOutNumber; k++)
                                {
                                    for (int j = 0; j <= col; j++)
                                    {

                                LeadOutLine = "";
                                newLine = "";
                                for (int i = 0; i <= ro; i++)
                                        {
                                            LeadOutLine = LeadOutLine + LeadOutValue.PadLeft(16, ' ');

                                        }

                                        index = Gridcounter.ToString();
                                        index = index.PadLeft(8, '0') ;
                                        streamwriter.WriteLine(index + "  000000     " + ColumnMultiplier + "      " + LeadOutLine);
                                _listRowsProcessor.ListOfRowsAdd(index + "  000000     " + ColumnMultiplier + "      " + LeadOutLine);
                                Gridcounter++;
                                    }
                        }
                        /////////////////End  Lead out //////////////////////////////////////


                        ///////////After Pack Text//////////////////////////////////////////


                        for (int k = 0; k < numberofRecordAfterPack; k++)
                        {
                            //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                            //           {
                            for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                            {
                                afterEndLine = "";
                                index = Gridcounter.ToString();
                                index = index.PadLeft(8, '0');
                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                {

                                    afterEndLine= afterEndLine + AfterPackText.PadLeft(16, ' ');
                                    //calculate space evener here
                                }
                                streamwriter.WriteLine(index+ "  000000     " + ColumnMultiplier + "      " + afterEndLine);
                                _listRowsProcessor.ListOfRowsAdd(index + "  000000     " + ColumnMultiplier + "      " + afterEndLine);
                                Gridcounter++;
                            }
                        }

                        ///////////End After Pack Text//////////////////////////////////////
                    }
                }
        }



        public void AssignValueToLabel()
        {
            int[,,] Matrix;
            Matrix = _matrixProcessor.GetMatrix();
            int sh = Matrix.GetUpperBound(0);
            int ro = Matrix.GetUpperBound(1);
            int col = Matrix.GetUpperBound(2);
            int pa = _matrixProcessor.GetPack();
            int sta = _matrixProcessor.GetStack();

            int correction = 0;
            int ValueStringtoAdd = 0;
           int  StartPoint = (_alphabetConverterHelper.UnformatValue(_matrixProcessor.GetStartPoint().ToString()));

    //        ValueStringtoAdd = StartPoint + i + (ro + 1) * j + ((col + 1) * (ro + 1) * n) + correction;
            _matrixProcessor.SetValueAlphaValue(ValueStringtoAdd.ToString());

        }




            public void CalculatorHorizontal()
        {
            int[,,] Matrix;
            int index = 0;
            int indexLength = 7;
            string indexstr;
            int ArrayInLength = _matrixProcessor.GetLeadInArrayLength();
            int ArrayDigitLength = _matrixProcessor.GetArrayDigitLength();
            int[] digits = new int[ArrayDigitLength];
            int[] lineIndex = new int[indexLength];
            int[] zeroarr = new int[ArrayDigitLength];
            char[] LeadIn = new char[_matrixProcessor.GetLeadInArray().Length];
            char[] LeadOut = new char[_matrixProcessor.GetLeadOutArray().Length];
            char[] tempchararray = new char[ArrayInLength];
            int LeadInNumber = _matrixProcessor.GetNumberOfLeadIn();
            int LeadOutNumber = _matrixProcessor.GetNumberOfLeadOut();
            int step = _matrixProcessor.GetStepValue();
            List<string> ListofRows = new List<string>();
            string tempRow;
            int indexval;
            int indexCount = 0;
            int Gridcounter = 0;
            string IndexRow;
            int Max = _matrixProcessor.GetMaxNumber();
            string modulus = "";
            int MaxValuePossible = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
            int StartPoint = 0;
            _ValueToShow = new ValuesToShow(_matrixProcessor);
            int max = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
            int NumberOfRecordsPerSheet = _matrixProcessor.GetRecordsOfSheetsNumber();
            int everyx = _matrixProcessor.GetEveryXSheetsValue();
            string AfterPackText = _matrixProcessor.GetTextBetweenPacks();
            int numberofRecordAfterPack = _matrixProcessor.GetRecordsOfPacksNumber();
            int negativeflag = 0;
            int stringtoint = 0;
            int outputlength = _sequenceProcessor.GetSequenceLength();
            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            //  string pathBis = Path.Combine(location, "NumberingTestBis.txt");
            string pathBis = @".\NumberingTestBis.txt";
            char[] tempcharArray;
            char[] baseCharArray;
            int spacenumbertoadd = 14 - LeadIn.Length;
            bool BasechararrayOption = _matrixProcessor.GetAddZeroeValue();
            string Alignment = _matrixProcessor.GetAlignment();
            int pmax = _matrixProcessor.GetStack();
            int vmax = _matrixProcessor.GetPack();
            int ColumnMultiplierInt = _matrixProcessor.GetMultiplyByColumnValue();
            string ColumnMultiplier = FromintToStringWithZero(2, ColumnMultiplierInt);
            int _SheeMultiplier = _matrixProcessor.GetMultiplyBySheetValue();
            int sheetcounter = 0;
            if (_matrixProcessor.GetStepOrder() != "+")
            {

                step = -step;
            }
            StartPoint = (_alphabetConverterHelper.UnformatValue(_matrixProcessor.GetStartPoint().ToString()));
            Array.Copy(_matrixProcessor.GetLeadInArray(), LeadIn, _matrixProcessor.GetLeadInArray().Length);
            Array.Copy(_matrixProcessor.GetLeadOutArray(), LeadOut, _matrixProcessor.GetLeadOutArray().Length);
            //  BasechararrayOption = _matrixProcessor.GetIsChecked();
            //   using (StreamWriter twBis = File.CreateText(pathBis))



            Matrix = _matrixProcessor.GetMatrix();
            int ro = Matrix.GetUpperBound(1);
            int col = Matrix.GetUpperBound(2);
            int pa = _matrixProcessor.GetPack();
            int sta = _matrixProcessor.GetStack();
            for (int p = 0; p < _matrixProcessor.GetPack(); p++)
            {
                for (int v = 0; v < _matrixProcessor.GetStack(); v++)
                {

                    using (StreamWriter twBis = File.AppendText(pathBis))
                        for (int k = 0; k < LeadInNumber; k++)
                        {
                            //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                            //           {
                            for (int j = 0; j <= ro; j++)
                            {
                                tempRow = string.Empty;
                                Array.Clear(lineIndex, 0, lineIndex.Length);
                                indexval = Gridcounter;
                                indexCount = 0;
                                for (; indexval != 0; indexval /= 10)
                                {
                                    lineIndex[indexCount] = indexval % 10;
                                    indexCount++;
                                }
                                Array.Reverse(lineIndex);
                                IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

                                //index adder end
                                //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                int actualLength = 0;
                                string spacestoadd = "";
                                while (spacestoadd.Length < spacenumbertoadd)
                                {
                                    spacestoadd += " ";
                                }

                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                {
                                    // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
                                    tempRow = tempRow + spacestoadd + fromCharArrayToString(LeadIn);
                                    int spacelen = spacestoadd.Length;
                                    int length = tempRow.Length;
                                    //calculate space evener here
                                }

                                Gridcounter++;
                                twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
                                //if (Int32.TryParse(IndexRow, out index))
                                //{
                                //    //    if (index == 63999)
                                //    //    { string msg = "Hello"; }
                                //    //     if (index < 63000)
                                //}
                              _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);
                                

                            }
                        }


                    using (StreamWriter twBis = File.AppendText(pathBis))


                        // For number of Sheets
                        for (int n = 0; n <= Matrix.GetUpperBound(2); n++)
                        {
                            for (int mult = 1; mult <= _SheeMultiplier; mult++)
                            {

                                //for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                {
                                    tempRow = string.Empty;
                                    //Index adder start
                                    Array.Clear(lineIndex, 0, lineIndex.Length);
                                    indexval = Gridcounter;

                                    indexCount = 0;
                                    for (; indexval != 0; indexval /= 10)
                                    {
                                        lineIndex[indexCount] = indexval % 10;
                                        indexCount++;
                                    }
                                    Array.Reverse(lineIndex);
                                     indexstr = fromArrayToString(lineIndex);
                                    IndexRow = indexstr + "   " + fromArrayToString(zeroarr) + "   ";

                                    //index adder end
                                    //    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                    for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                    {

                                        Array.Clear(digits, 0, digits.Length);
                                        int count = 0;
                                        negativeflag = 0;

                                        //   int val = StartPoint + step * ((v * (_matrixProcessor.GetStack() * (Max + 1))) + p * Max + p + (Matrix[i, j, n]));
                                        int val = StartPoint + step * ((v * (_matrixProcessor.GetPack() * (Max + 1))) + p * Max + p + (Matrix[i, j, n]));

                                        ///Calculate Weighting
                                        ///
                                        val = _ancillaryFunctions.CalculateWightedResult(val);
                                        ///

                                        if (val >= MaxValuePossible)
                                        {
                                            while (val >= MaxValuePossible)
                                            {
                                                val = val - MaxValuePossible;
                                            }
                                        }
                                        if (_matrixProcessor.GetModulusType() == "Modulus")
                                        {
                                            modulus = CalculateModulus(val);
                                        }
                                        else if (_matrixProcessor.GetModulusType() == "Modulus w/o 0")
                                        {
                                            modulus = CalculateModulus(val - 1);
                                        }
                                        else if (_matrixProcessor.GetModulusType() == "Complement")
                                        {
                                            modulus = CalculateComplement(val);
                                        }
                                        else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
                                        {
                                            modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
                                        }
                                        else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
                                        {
                                            modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
                                        }

                                        if (val < 0)
                                        {
                                            negativeflag = 1;
                                            val = max - (-1 * val) + 1;
                                        }

                                        int[] flexdigits = new int[val.ToString().Length];
                                        for (int d = flexdigits.Length - 1; d >= 0; d--)
                                        {
                                            flexdigits[d] = val % 10;
                                            val /= 10;
                                        }

                                        Int32.TryParse(fromArrayToString(flexdigits), out stringtoint);


                                        //fill Array and turn into string
                                        baseCharArray = _alphabetConverterHelper.makeBaseArray();
                                        int indexbase = baseCharArray.Length - 1;
                                        ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(stringtoint, 0, "");

                                        if (BasechararrayOption == true)
                                        {
                                            tempcharArray = ValueStringtoAdd.ToCharArray();
                                            for (int q = tempcharArray.Length - 1; q >= 0; q--)
                                            {
                                                baseCharArray[indexbase--] = tempcharArray[q];
                                            }
                                            //    Array.Reverse(baseCharArray);
                                            ValueStringtoAdd = fromCharArrayToString(baseCharArray);
                                        }
                                        if (_matrixProcessor.GetModulusPosition() == "Beginning")
                                        {
                                            ValueStringtoAdd = modulus + ValueStringtoAdd;
                                        }
                                        else
                                        {
                                            ValueStringtoAdd = ValueStringtoAdd + modulus;
                                        }
                                        //   if (negativeflag == 1)

                                        if (int.TryParse(ValueStringtoAdd, out int x) == true && negativeflag == 1)
                                        { ValueStringtoAdd = "-" + ValueStringtoAdd; }

                                        if (i == 0 && j == 0 && n == 0 && v==0 && p==0)
                                        {
                                            _ValueToShow.ValueZeroZero = ValueStringtoAdd;
                                            _matrixProcessor.SetValueZeroZero(ValueStringtoAdd);
                                        }

                                        if (i == 0 && j == Matrix.GetUpperBound(1) && n == 0 && v == 0 && p == 0)
                                        {
                                            _ValueToShow.ValueZeroLast = ValueStringtoAdd;
                                            _matrixProcessor.SetValueZeroLast(ValueStringtoAdd);
                                        }

                                        if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _ValueToShow.ValueLastZero = ValueStringtoAdd;
                                        }

                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueLastLast(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == Matrix.GetUpperBound(2))
                                        {
                                            _matrixProcessor.SetValueZeroZeroLast(ValueStringtoAdd);
                                        }

                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == 0)
                                        {
                                            _matrixProcessor.SetValueZeroZeroOneStack(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == 0)
                                        {
                                            _matrixProcessor.SetValueLastSheetFirstStackFirstElement(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == Matrix.GetUpperBound(2) && v == _matrixProcessor.GetStack()-1 && p == 0)
                                        {
                                            _matrixProcessor.SetValueLastLastPack(ValueStringtoAdd);
                                        }


                                        //Position Labels


                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueAlphaValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j ==1 && n == 0 && v == 0 && p == 0)
                                        {
                                            int ValueAlphaZeroOne = Matrix[i, j, n];
                                            _matrixProcessor.SetValueAlphaZeroOneValue(ValueStringtoAdd);
                                        }
                                        if (i == 1 && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            int ValueAlphaOneZero = Matrix[i, j, n]; 
                                            _matrixProcessor.SetValueAlphaOneZeroValue(ValueStringtoAdd);
                                        }

                                        if (i == 0 && j == Matrix.GetUpperBound(1) && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueBetaValue(ValueStringtoAdd);
                                        }

                                        if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueGammaValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueDeltaValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueEpsilonValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j ==0 && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 1 && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaZeroOneValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaZeroLastValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == 1 && v ==  0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueZetaValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == _matrixProcessor.GetStack() - 1 && _matrixProcessor.GetStack() - 1>0 && p == 0)
                                        {
                                           // int eta = Matrix[i, j, n];
                                            _matrixProcessor.SetValueEtaValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == _matrixProcessor.GetStack() - 1 &&  _matrixProcessor.GetStack()-1>0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueThetaValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == 1 && _matrixProcessor.GetPack() - 1 != 0)
                                        {
                                            _matrixProcessor.SetValueIotaValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == 0 && p == 1 && _matrixProcessor.GetPack() - 1 != 0)
                                        {
                                            _matrixProcessor.SetValueKappaValue(ValueStringtoAdd);
                                        }

                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == 1 && _matrixProcessor.GetStack() - 1 != 0 && _matrixProcessor.GetPack() - 1 != 0)
                                        {
                                            _matrixProcessor.SetValueLambdaValue(ValueStringtoAdd);
                                        }

                                        if (i == 0 && j ==0 && n == 0 && v == _matrixProcessor.GetStack() - 1 && p == 1 && _matrixProcessor.GetPack() - 1 !=0)
                                        {
                                            _matrixProcessor.SetValueMuValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == _matrixProcessor.GetStack() - 1 && p == 1 && _matrixProcessor.GetPack() - 1 != 0 && _matrixProcessor.GetStack() - 1 != 0)
                                        {
                                            _matrixProcessor.SetValueNuValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == _matrixProcessor.GetPack() - 1)
                                        {
                                            _matrixProcessor.SetValueXiValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == 0 && p == _matrixProcessor.GetPack() - 1)
                                        {
                                            _matrixProcessor.SetValueOmicronValue(ValueStringtoAdd);
                                        }
                                        if (i ==0 && j == 0 && n == 0 && v == 1 && p == _matrixProcessor.GetPack() - 1)
                                        {
                                            _matrixProcessor.SetValuePiValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == 0 && v == _matrixProcessor.GetStack() - 1 && p == _matrixProcessor.GetPack() - 1)
                                        {
                                            _matrixProcessor.SetValueRoValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && v == 0 && p == _matrixProcessor.GetPack() - 1)
                                        {
                                            _matrixProcessor.SetValueSigmaValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == _matrixProcessor.GetStack()-1  && p == _matrixProcessor.GetPack()-1 )
                                        {
                                            int omega = Matrix[i, j, n];
                                            _matrixProcessor.SetValueOmegaValue(ValueStringtoAdd);
                                        }

                                        ///Rolled Paper Part/////

                                        if (i == 0 && j == 0 && n == 0 && p == 0 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledAlphaValue(ValueStringtoAdd);
                                        }
                                        if (i == 1 && j == 0 && n == 0 && p == 0 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledBetaValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && p == 0 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledGammaValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledDeltaValue(ValueStringtoAdd);
                                        }

                                        if (i == 0 && j == 0 && n == Matrix.GetUpperBound(2) && p == 0 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledEpsilonValue(ValueStringtoAdd);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && p == 1 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledZetaValue(ValueStringtoAdd);
                                        }
                                        if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && p == 1 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledEtaValue(ValueStringtoAdd);
                                        }
                                        //////////////////////


                                        //End Position Labels


                                        int actualLength = 0;
                                        string spacestoadd = "";
                                        while (spacestoadd.Length + ValueStringtoAdd.Length < 14)
                                        {
                                            spacestoadd += " ";
                                        }
                                        //  string spacestoadd = SpacesToadd(14, ValueStringtoAdd);
                                        tempRow = tempRow + spacestoadd + ValueStringtoAdd;
                                        int spacelen = spacestoadd.Length;
                                        int length = tempRow.Length;
                                        //even the spaces
                                    }
                                    Gridcounter++;


                                    twBis.WriteLine("{0}{1}", IndexRow, ColumnMultiplier + "      " + tempRow);
                                    if (Int32.TryParse(indexstr, out index))
                                    {
                                    //    if (index == 63999)
                                    //    { string msg = "Hello"; }
                                     //       if (index < 63000)
                                                _listRowsProcessor.ListOfRowsAdd(IndexRow + ColumnMultiplier + "       " + tempRow);
                                    }
                                    //

                                    //if (indexval < 63000)
                                    //        _listRowsProcessor.ListOfRowsAdd(IndexRow + ColumnMultiplier + "       " + tempRow);
                                    //twBis.WriteLine("{0}", IndexRow + ColumnMultiplier + "       " + tempRow);
                                }
                            }

                            ////Text every X sheets
                            int upperb = Matrix.GetUpperBound(2);

                            //  if (everyx!=0  && (n+1)% everyx == 0 && n != upperb+1)
                            if (everyx != 0 && (n + 1) % everyx == 0 && (n + 1) % upperb != 0)

                            {
                                for (int k = 0; k < NumberOfRecordsPerSheet; k++)
                                {
                                    //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                                    //           {
                                    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                    {
                                        tempRow = string.Empty;
                                        Array.Clear(lineIndex, 0, lineIndex.Length);
                                        indexval = Gridcounter;
                                        indexCount = 0;
                                        for (; indexval != 0; indexval /= 10)
                                        {
                                            lineIndex[indexCount] = indexval % 10;
                                            indexCount++;
                                        }
                                        Array.Reverse(lineIndex);
                                        IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

                                        //index adder end
                                        //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                        int actualLength = 0;
                                        string spacestoadd = "";
                                        while (spacestoadd.Length < 14 - _matrixProcessor.GetTextBetweenSheets().Length)
                                        {
                                            spacestoadd += " ";
                                        }

                                        for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                        {
                                            // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
                                            tempRow = tempRow + spacestoadd + _matrixProcessor.GetTextBetweenSheets();
                                            int spacelen = spacestoadd.Length;
                                            int length = tempRow.Length;
                                            //calculate space evener here
                                        }

                                        Gridcounter++;
                                  //      if (index == 63999)
                                  //      { string msg = "Hello"; }
                                        twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
                               //         if (index < 63000)
                                                _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);

                                    }
                                }
                            }

                        }

                    //////////////////////////Lead out //////////////////////////////////////

                    using (StreamWriter twBis = File.AppendText(pathBis))
                        for (int k = 0; k < LeadOutNumber; k++)
                        {
                            //                 for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                            //                 {
                            //   for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                            for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                            {
                                tempRow = string.Empty;

                                //Index adder start
                                Array.Clear(lineIndex, 0, lineIndex.Length);
                                indexval = Gridcounter;

                                indexCount = 0;
                                for (; indexval != 0; indexval /= 10)
                                {
                                    lineIndex[indexCount] = indexval % 10;
                                    indexCount++;
                                }
                                Array.Reverse(lineIndex);
                                IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";


                                int actualLength = 0;
                                string spacestoadd = "";
                                while (spacestoadd.Length < 14 - LeadOut.Length)
                                {
                                    spacestoadd += " ";
                                }
                                //index adder end
                                // for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                {
                                    //  string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadOut));
                                    //  tempRow = tempRow + fromCharArrayToString(LeadIn) + spacestoadd + "   ";
                                    //  string spacestoadd = SpacesToadd(ArrayInLength, fromCharArrayToString(LeadOut));
                                    tempRow = tempRow + spacestoadd + fromCharArrayToString(LeadOut);
                                    int length = tempRow.Length;
                                }
                                Gridcounter++;

                                twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
                         //       if (index == 63999)
                          //      { string msg = "Hello"; }
                                    if (index < 630000)
                                _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);

       
                            }
                        }
                }

                //Text after pack here

                using (StreamWriter twBis = File.AppendText(pathBis))

                    for (int k = 0; k < numberofRecordAfterPack; k++)
                    {
                        //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                        //           {
                        for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                        {
                            tempRow = string.Empty;
                            Array.Clear(lineIndex, 0, lineIndex.Length);
                            indexval = Gridcounter;
                            indexCount = 0;
                            for (; indexval != 0; indexval /= 10)
                            {
                                lineIndex[indexCount] = indexval % 10;
                                indexCount++;
                            }
                            Array.Reverse(lineIndex);
                            IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

                            //index adder end
                            //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                            int actualLength = 0;
                            string spacestoadd = "";
                            while (spacestoadd.Length < spacenumbertoadd)
                            {
                                spacestoadd += " ";
                            }

                            for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                            {
                                // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
                                tempRow = tempRow + spacestoadd + AfterPackText;
                                int spacelen = spacestoadd.Length;
                                int length = tempRow.Length;
                                //calculate space evener here
                            }

                            Gridcounter++;
                            twBis.WriteLine("{0}", IndexRow + "01      " + tempRow);
                     //       if (index == 63999)
                   //         { string msg = "Hello"; }
                   //         if (index < 630000)
                                _listRowsProcessor.ListOfRowsAdd(IndexRow + "01      " + tempRow);


                        }
                    }
            }
        }



        public void NewCalculatorVertical()
        {
            int[,,] Matrix;
            Matrix = _matrixProcessor.GetMatrix();
            int sh = Matrix.GetUpperBound(2);
            int col = Matrix.GetUpperBound(1);
            int ro = Matrix.GetUpperBound(0);
            int pa = _matrixProcessor.GetPack();
            int sta = _matrixProcessor.GetStack();
            int StartPoint = 0;
            int Gridcounter;
            int ResultLength = _sequenceProcessor.GetSequenceLength();
            int step;
            int LeadInNumber = _matrixProcessor.GetNumberOfLeadIn();
            int val;
            int MaxValuePossible = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
            int correction = 0;
            int[] flexdigits;
            int[] ArrayBase = new int[ResultLength];
            bool BasechararrayOption = _matrixProcessor.GetAddZeroeValue();
            string finalstring = "";
            string index;
            string unpaddedelement;
            string elementstring;
            string newLine;
            string modulus = "";
            string LeadInLine = "";
            string LeadOutLine = "";
            int NumberOfRecordsPerSheet = _matrixProcessor.GetRecordsOfSheetsNumber();
            string TextBetweenSheets = _matrixProcessor.GetTextBetweenSheets();
            string TestBetweenSheetsLine = "";
            string LeadInValue = _matrixProcessor.GetLeadInToKeepValue();
            string LeadOutValue = _matrixProcessor.GetLeadOutToKeepValue();
            int ColumnMultiplierInt = _matrixProcessor.GetMultiplyByColumnValue();
            string ColumnMultiplier = FromintToStringWithZero(2, ColumnMultiplierInt);
            int _SheeMultiplier = _matrixProcessor.GetMultiplyBySheetValue();
            int everyx = _matrixProcessor.GetEveryXSheetsValue();
            int LeadOutNumber = _matrixProcessor.GetNumberOfLeadOut();
            int numberofRecordAfterPack = _matrixProcessor.GetRecordsOfPacksNumber();
            string afterEndLine = "";
            string AfterPackText = _matrixProcessor.GetTextBetweenPacks();

            //   string pathBis = @".\NewNumberingTestBisTest.txt";

            StartPoint = (_alphabetConverterHelper.UnformatValue(_matrixProcessor.GetStartPoint().ToString()));
            step = 1;
            Gridcounter = 0;
            using (StreamWriter streamwriter = File.CreateText(@".\NewNumberingTestBisTest.txt"))
                // using (StreamWriter streamwriter = File.AppendText(pathBis))
                for (int p = 0; p < pa; p++)
                {

                    for (int v = 0; v < sta; v++)
                    {

                        for (int k = 0; k < LeadInNumber; k++)
                        {

                            for (int j = 0; j <= col; j++)
                            {

                                LeadInLine = "";
                                for (int i = 0; i <= ro; i++)
                                {
                                    LeadInLine = LeadInLine + LeadInValue.PadLeft(16, ' ');

                                }
                                index = Gridcounter.ToString();
                                newLine = index.PadLeft(8, '0');
                                streamwriter.WriteLine(newLine + "  000000     " + ColumnMultiplier + "      " + LeadInLine);
                                _listRowsProcessor.ListOfRowsAdd(newLine + "  000000     " + ColumnMultiplier + "      " + LeadInLine);
                                Gridcounter++;
                            }
                        }


                        for (int n = 0; n <= sh; n++)
                        {
                            //Sheet Multiplier 
                            for (int mult = 1; mult <= _SheeMultiplier; mult++)
                            {

                                for (int j = 0; j <= col; j++)
                                {
                                    finalstring = "";
                                    for (int i = 0; i <= ro; i++)
                                    {
                                        correction = v * ((col + 1) * (ro + 1) * (sh + 1)) + p * (sta * ((col + 1) * (ro + 1) * (sh + 1)));
                                        val = StartPoint + j + (col + 1) * i + ((col + 1) * (ro + 1) * n) + correction;

                                        ///Calculate Weighting and Modulus
                                        ///
                                        val = _ancillaryFunctions.CalculateWightedResult(val);

                                        if (val >= MaxValuePossible)
                                        {
                                            while (val >= MaxValuePossible)
                                            {
                                                val = val - MaxValuePossible;
                                            }
                                        }
                                        if (_matrixProcessor.GetCheckDigitValue())
                                        {
                                            if (_matrixProcessor.GetModulusType() == "Module")
                                            {
                                                modulus = CalculateModulus(val);
                                            }
                                            else if (_matrixProcessor.GetModulusType() == "Modulus w/o 0")
                                            {
                                                modulus = CalculateModulus(val - 1);
                                            }
                                            else if (_matrixProcessor.GetModulusType() == "Complement")
                                            {
                                                modulus = CalculateComplement(val);
                                            }
                                            else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
                                            {
                                                modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
                                            }
                                            else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
                                            {
                                                modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
                                            }
                                        }
                                        ///End Calculate Weighting and Modulus
                                        
                                        flexdigits = new int[val.ToString().Length];
                                        for (int d = flexdigits.Length - 1; d >= 0; d--)
                                        {
                                            flexdigits[d] = val % 10;
                                            val /= 10;
                                        }

                                        elementstring = fromArrayToString(flexdigits);

                                    if (_matrixProcessor.GetModulusPosition() == "Beginning")
                                        {
                                            elementstring = modulus + elementstring;
                                        }
                                        else
                                        {
                                            elementstring = modulus + elementstring;
                                        }



                                        if (_matrixProcessor.GetAddZeroeValue())
                                        {
                                            ////Check Zero or not padded
                                            elementstring = elementstring.PadLeft(ResultLength, '0');
                                        }
                                        else
                                        {
                                            elementstring = elementstring.PadLeft(ResultLength, ' ');
                                        }

                                        //Assign Labels

                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueAlphaValue(elementstring);
                                            _matrixProcessor.SetValueRolledAlphaValue(elementstring);
                                        }
                                        if (i == 0 && j == 1 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueAlphaZeroOneValue(elementstring);
                                        }
                                        if (i == 1 && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueAlphaOneZeroValue(elementstring);
                                            _matrixProcessor.SetValueRolledBetaValue(elementstring);
                                        }
                                        if (i == 0 && j == col && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueBetaValue(elementstring);
                                        }

                                        if (i == ro && j == 0 && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueGammaValue(elementstring);
                                            _matrixProcessor.SetValueRolledGammaValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == 0 && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueDeltaValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueEpsilonValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == sh && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaValue(elementstring);
                                            _matrixProcessor.SetValueRolledEpsilonValue(elementstring);
                                        }
                                        if (i == 0 && j == 1 && n == sh && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaZeroOneValue(elementstring);
                                        }
                                        if (i == 0 && j == col && n == sh && v == 0 && p == 0)
                                        {
                                            _matrixProcessor.SetValueSigmaZeroLastValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == 0)
                                        {
                                            _matrixProcessor.SetValueZetaValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && sta != 0 && v == sta - 1 && p == 0)
                                        {
                                            _matrixProcessor.SetValueEtaValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == sta - 1 && p == 0)
                                        {
                                            _matrixProcessor.SetValueThetaValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == 1)
                                        {
                                            _matrixProcessor.SetValueIotaValue(elementstring);
                                            _matrixProcessor.SetValueRolledZetaValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == 0 && p == 1)
                                        {
                                            _matrixProcessor.SetValueKappaValue(elementstring);
                                        }

                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == 1)
                                        {
                                            _matrixProcessor.SetValueLambdaValue(elementstring);
                                        }

                                        if (i == 0 && j == 0 && n == 0 && v == sta - 1 && p == 1)
                                        {
                                            _matrixProcessor.SetValueMuValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == sta - 1 && p == 1)
                                        {
                                            _matrixProcessor.SetValueNuValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 0 && p == pa - 1)
                                        {
                                            _matrixProcessor.SetValueXiValue(elementstring);
                                        }
                                        if (i == ro && j == col && n == sh && v == 0 && p == pa - 1)
                                        {
                                            _matrixProcessor.SetValueOmicronValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == 1 && p == pa - 1)
                                        {
                                            _matrixProcessor.SetValuePiValue(elementstring);
                                        }
                                        if (i == 0 && j == 0 && n == 0 && v == sta - 1 && p == pa - 1)
                                        {
                                            _matrixProcessor.SetValueRoValue(elementstring);
                                        }
                                        //if (i == ro && j == 0 && n == 0 && v == 0 && p == pa - 1)
                                        //{
                                        //    _matrixProcessor.SetValueSigmaValue(elementstring);
                                        //}
                                        if (i == ro && j == col && n == sh && v == sta - 1 && p == pa - 1)
                                        {
                                            _matrixProcessor.SetValueOmegaValue(elementstring);
                                        }

                                        //End Assign Labels

                                        ///  Assign Labels Rolled Paper Part/////

                                        if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledDeltaValue(elementstring);
                                        }

                                        if (i == ro && j == 0 && n == 0 && p == 1 && v == 0)
                                        {
                                            _matrixProcessor.SetValueRolledEtaValue(elementstring);
                                        }
                                        /// End Assign Labels Rolled Paper Part/////

                                        finalstring = finalstring + elementstring.PadLeft(16, ' ');
                                    }
                                    index = Gridcounter.ToString();
                                    newLine = index.PadLeft(8, '0');
                                    streamwriter.WriteLine(newLine + "  000000     " + ColumnMultiplier + "      " + finalstring);
                                    _listRowsProcessor.ListOfRowsAdd(index + "  000000     " + ColumnMultiplier + "      " + finalstring);
                                    Gridcounter++;
                                }
                            }

                            ///test every sheet

                            if (everyx != 0 && (n + 1) % everyx == 0 && (n + 1) % sh != 0)

                            {

                                for (int k = 0; k < NumberOfRecordsPerSheet; k++)
                                {
                                    //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                                    //           {
                                    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                    {
                                        index = Gridcounter.ToString();
                                        TestBetweenSheetsLine = "";
                                        for (int i = 0; i <= ro; i++)
                                        {
                                            TestBetweenSheetsLine = TestBetweenSheetsLine + TextBetweenSheets.PadLeft(16, ' ');

                                        }
                                        index=index.PadLeft(8, '0');
                                        streamwriter.WriteLine(index + "  000000     " + ColumnMultiplier + "      " + TestBetweenSheetsLine);
                                        _listRowsProcessor.ListOfRowsAdd(index + "  000000     " + ColumnMultiplier + "      " + TestBetweenSheetsLine);
                                        Gridcounter++;
                                    }
                                }
                            }

                        }
                        ///end test every sheet


                        //////////////////////////Lead out //////////////////////////////////////



                        for (int k = 0; k < LeadOutNumber; k++)
                        {
                            for (int j = 0; j <= col; j++)
                            {

                                LeadOutLine = "";
                                newLine = "";
                                for (int i = 0; i <= ro; i++)
                                {
                                    LeadOutLine = LeadOutLine + LeadOutValue.PadLeft(16, ' ');

                                }

                                index = Gridcounter.ToString();
                                index = index.PadLeft(8, '0');
                                streamwriter.WriteLine(index + "  000000     " + ColumnMultiplier + "      " + LeadOutLine);
                                _listRowsProcessor.ListOfRowsAdd(index + "  000000     " + ColumnMultiplier + "      " + LeadOutLine);
                                Gridcounter++;
                            }
                        }
                        /////////////////End  Lead out //////////////////////////////////////


                        ///////////After Pack Text//////////////////////////////////////////
                        for (int k = 0; k < numberofRecordAfterPack; k++)
                        {
                            //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                            //           {
                            for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                            {
                                afterEndLine = "";
                                index = Gridcounter.ToString();
                                index = index.PadLeft(8, '0');
                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                {
                                    afterEndLine = afterEndLine + AfterPackText.PadLeft(16, ' ');
                                    //calculate space evener here
                                }
                                streamwriter.WriteLine(index + "  000000     " + ColumnMultiplier + "      " + afterEndLine);
                                _listRowsProcessor.ListOfRowsAdd(index + "  000000     " + ColumnMultiplier + "      " + afterEndLine);
                                Gridcounter++;
                            }
                        }

                        ///////////End After Pack Text//////////////////////////////////////
                    }
                }

        }


            public void CalculatorVertical()
        {
            int[,,] Matrix;
            int index = 0;
            int indexLength = 7;
            int ArrayInLength = _matrixProcessor.GetLeadInArrayLength();
            int ArrayDigitLength = _matrixProcessor.GetArrayDigitLength();
            int[] digits = new int[ArrayDigitLength];
            int[] lineIndex = new int[indexLength];
            int[] zeroarr = new int[ArrayDigitLength];
            char[] LeadIn = new char[_matrixProcessor.GetLeadInArray().Length];
            char[] LeadOut = new char[_matrixProcessor.GetLeadOutArray().Length];
            char[] tempchararray = new char[ArrayInLength];
            int LeadInNumber = _matrixProcessor.GetNumberOfLeadIn();
            int LeadOutNumber = _matrixProcessor.GetNumberOfLeadOut();
            int step = _matrixProcessor.GetStepValue();
            List<string> ListofRows = new List<string>();
            string tempRow;
            int indexval;
            int indexCount = 0;
            int Gridcounter = 0;
            string IndexRow;
            int Max = _matrixProcessor.GetMaxNumber();
            string modulus = "";
            int MaxValuePossible = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
            int StartPoint = 0;
            _ValueToShow = new ValuesToShow(_matrixProcessor);
            int max = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
            int NumberOfRecordsPerSheet = _matrixProcessor.GetRecordsOfSheetsNumber();
            int everyx = _matrixProcessor.GetEveryXSheetsValue();
            string AfterPackText = _matrixProcessor.GetTextBetweenPacks();
            int numberofRecordAfterPack = _matrixProcessor.GetRecordsOfPacksNumber();
            int negativeflag = 0;
            int stringtoint = 0;
            int outputlength = _sequenceProcessor.GetSequenceLength();
            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string pathBis = @".\NumberingTestBis.txt";
            //    string pathBis = Path.Combine(location, "NumberingTestBis.txt");
            char[] tempcharArray;
            char[] baseCharArray;
            int spacenumbertoadd = 14 - LeadIn.Length;
            bool BasechararrayOption = _matrixProcessor.GetAddZeroeValue();
            string Alignment = _matrixProcessor.GetAlignment();
            int pmax = _matrixProcessor.GetStack();
            int vmax = _matrixProcessor.GetPack();
            int ColumnMultiplierInt = _matrixProcessor.GetMultiplyByColumnValue();
            string ColumnMultiplier = FromintToStringWithZero(2, ColumnMultiplierInt);
            int _SheeMultiplier = _matrixProcessor.GetMultiplyBySheetValue();
            if (_matrixProcessor.GetStepOrder() != "+")
            {

                step = -step;
            }
            StartPoint = (_alphabetConverterHelper.UnformatValue(_matrixProcessor.GetStartPoint().ToString()));
            Array.Copy(_matrixProcessor.GetLeadInArray(), LeadIn, _matrixProcessor.GetLeadInArray().Length);
            Array.Copy(_matrixProcessor.GetLeadOutArray(), LeadOut, _matrixProcessor.GetLeadOutArray().Length);
          //  BasechararrayOption = _matrixProcessor.GetIsChecked();
            //   using (StreamWriter twBis = File.CreateText(pathBis))


            Matrix = _matrixProcessor.GetMatrix();

            for (int v = 0; v < _matrixProcessor.GetPack(); v++)
            {
                for (int p = 0; p < _matrixProcessor.GetStack(); p++)
                {
                    using (StreamWriter twBis = File.AppendText(pathBis))
                        for (int k = 0; k < LeadInNumber; k++)
                        {
                            //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                            //           {
                            for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                            {
                                tempRow = string.Empty;
                                Array.Clear(lineIndex, 0, lineIndex.Length);
                                indexval = Gridcounter;
                                indexCount = 0;
                                for (; indexval != 0; indexval /= 10)
                                {
                                    lineIndex[indexCount] = indexval % 10;
                                    indexCount++;
                                }
                                Array.Reverse(lineIndex);
                                IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

                                //index adder end
                                //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                int actualLength = 0;
                                string spacestoadd = "";
                                while (spacestoadd.Length < 14 - _matrixProcessor.GetTextBetweenSheets().Length)
                                {
                                    spacestoadd += " ";
                                }

                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                {
                                    // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
                                    tempRow = tempRow + spacestoadd + fromCharArrayToString(LeadIn);
                                    int spacelen = spacestoadd.Length;
                                    int length = tempRow.Length;
                                    //calculate space evener here
                                }

                                Gridcounter++;
                                _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);
                                twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
                            }
                            //          }
                        }
                    using (StreamWriter twBis = File.AppendText(pathBis))
                        //for (int mult = 0; mult < _SheeMultiplier; mult++)
                        //{
                        for (int n = 0; n <= Matrix.GetUpperBound(2); n++)
                        {
                            //for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                            for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                            {
                                tempRow = string.Empty;
                                //Index adder start
                                Array.Clear(lineIndex, 0, lineIndex.Length);
                                indexval = Gridcounter;

                                indexCount = 0;
                                for (; indexval != 0; indexval /= 10)
                                {
                                    lineIndex[indexCount] = indexval % 10;
                                    indexCount++;
                                }
                                Array.Reverse(lineIndex);
                                IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

                                //index adder end
                                //    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                {

                                    Array.Clear(digits, 0, digits.Length);
                                    int count = 0;
                                    negativeflag = 0;

                                    int val = StartPoint + step * ((v * (_matrixProcessor.GetStack() * (Max + 1))) + p * Max + p + (Matrix[i, j, n]));


                                    ///Calculate Weighting
                                    ///
                                    val = _ancillaryFunctions.CalculateWightedResult(val);
                                    ///

                                    if (val >= MaxValuePossible)
                                    {
                                        while (val >= MaxValuePossible)
                                        {
                                            val = val - MaxValuePossible;
                                        }
                                    }
                                    if (_matrixProcessor.GetModulusType() == "Modulus")
                                    {
                                        modulus = CalculateModulus(val);
                                    }
                                    else if (_matrixProcessor.GetModulusType() == "Modulus w/o 0")
                                    {
                                        modulus = CalculateModulus(val - 1);
                                    }
                                    else if (_matrixProcessor.GetModulusType() == "Complement")
                                    {
                                        modulus = CalculateComplement(val);
                                    }
                                    else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
                                    {
                                        modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
                                    }
                                    else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
                                    {
                                        modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
                                    }

                                    //  int val = 100;
                                    // digits = val.ToString().Select(o => Convert.ToInt32(o)).ToArray();
                                    //for (; val != 0; val /= 10)
                                    //{
                                    //    digits[count] = val % 10;
                                    //    count++;
                                    //}

                                    if (val < 0)
                                    {
                                        negativeflag = 1;
                                        val = max - (-1 * val) + 1;
                                    }

                                    int[] flexdigits = new int[val.ToString().Length];
                                    for (int d = flexdigits.Length - 1; d >= 0; d--)
                                    {
                                        flexdigits[d] = val % 10;
                                        val /= 10;
                                    }
                                    //Array.Reverse(digits);

                                    //if (_matrixProcessor.GetBitCheck() != "None")
                                    //{
                                    //    if (_matrixProcessor.GetBitCheck() == "Modulus 2")
                                    //    {
                                    //     //   modulus = "2";
                                    //        flexdigits[0] = Matrix[i, j, n] % 2;
                                    //    }
                                    //    else { flexdigits[0] = Matrix[i, j, n] % 3; }

                                    //}


                                    Int32.TryParse(fromArrayToString(flexdigits), out stringtoint);


                                    //fill Array and turn into string
                                    baseCharArray = _alphabetConverterHelper.makeBaseArray();
                                    int indexbase = baseCharArray.Length - 1;
                                    ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(stringtoint, 0, "");

                                    if (BasechararrayOption == true)
                                    {
                                        tempcharArray = ValueStringtoAdd.ToCharArray();
                                        for (int q = tempcharArray.Length - 1; q >= 0; q--)
                                        {
                                            baseCharArray[indexbase--] = tempcharArray[q];
                                        }
                                        //    Array.Reverse(baseCharArray);
                                        ValueStringtoAdd = fromCharArrayToString(baseCharArray);
                                    }
                                    if (_matrixProcessor.GetModulusPosition() == "Beginning")
                                    {
                                        ValueStringtoAdd = modulus + ValueStringtoAdd;
                                    }
                                    else
                                    {
                                        ValueStringtoAdd = ValueStringtoAdd + modulus;
                                    }
                                    //   if (negativeflag == 1)

                                    if (int.TryParse(ValueStringtoAdd, out int x) == true && negativeflag == 1)
                                    { ValueStringtoAdd = "-" + ValueStringtoAdd; }




                                    ///////////////////////

                                    if (i == 0 && j == 0 && n == 0 && v == 0 && p == 0)
                                    {
                                        _matrixProcessor.SetValueAlphaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 1 && n == 0 && v == 0 && p == 0)
                                    {
                                        int ValueAlphaZeroOne = Matrix[i, j, n];
                                        _matrixProcessor.SetValueAlphaZeroOneValue(ValueStringtoAdd);
                                    }
                                    if (i == 1 && j == 0 && n == 0 && v == 0 && p == 0)
                                    {
                                        int ValueAlphaOneZero = Matrix[i, j, n];
                                        _matrixProcessor.SetValueAlphaOneZeroValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == Matrix.GetUpperBound(1) && n == 0 && v == 0 && p == 0)
                                    {
                                        _matrixProcessor.SetValueBetaValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && v == 0 && p == 0)
                                    {
                                        _matrixProcessor.SetValueGammaValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == 0 && v == 0 && p == 0)
                                    {
                                        _matrixProcessor.SetValueDeltaValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
                                    {
                                        int eps = Matrix[i, j, n];
                                        _matrixProcessor.SetValueEpsilonValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
                                    {
                                        _matrixProcessor.SetValueSigmaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 1 && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
                                    {
                                        _matrixProcessor.SetValueSigmaZeroOneValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
                                    {
                                        _matrixProcessor.SetValueSigmaZeroLastValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
                                    {
                                        _matrixProcessor.SetValueZetaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n == 0 && p == _matrixProcessor.GetStack() - 1 && v == 0)
                                    {
                                        int eta = Matrix[i, j, n];
                                        _matrixProcessor.SetValueEtaValue(ValueStringtoAdd);
                                    }


                                    if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == _matrixProcessor.GetStack()-1 && v == 0)
                                    {
                                        _matrixProcessor.SetValueThetaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n == 0 && p == 0 && v == 1)
                                    {
                                        _matrixProcessor.SetValueIotaValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == 0 && v == 1)
                                    {
                                        _matrixProcessor.SetValueKappaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n ==1 && p == 1 && v == 1 && _matrixProcessor.GetStack() - 1 != 0 && _matrixProcessor.GetPack() - 1 != 0)
                                    {
                                        _matrixProcessor.SetValueLambdaValue(ValueStringtoAdd);
                                    }



                                    if (i == 0 && j == 0 && n == 0 && p == _matrixProcessor.GetStack()-1 && v == 1 && _matrixProcessor.GetPack() - 1 != 0 && _matrixProcessor.GetStack() - 1 != 0)
                                    {
                                        _matrixProcessor.SetValueMuValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == _matrixProcessor.GetStack() - 1 && v == 1 && _matrixProcessor.GetPack() - 1 != 0 && _matrixProcessor.GetStack() - 1 != 0)
                                    {
                                        _matrixProcessor.SetValueNuValue(ValueStringtoAdd);
                                    }


                                    if (i == 0 && j == 0 && n == 0 && p == 0 && v == _matrixProcessor.GetPack() - 1)
                                    {
                                        _matrixProcessor.SetValueXiValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == 0 && v == _matrixProcessor.GetPack() - 1)
                                    {
                                        _matrixProcessor.SetValueOmicronValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n == 0 && p == 1 && v == _matrixProcessor.GetPack() - 1)
                                    {
                                        int pi = Matrix[i, j, n];
                                        _matrixProcessor.SetValuePiValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == _matrixProcessor.GetStack() - 1 && v == _matrixProcessor.GetPack() - 1)
                                    {
                                        int ro = Matrix[i, j, n];
                                        _matrixProcessor.SetValueRoValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && p == 0 && v == _matrixProcessor.GetPack() - 1)
                                    {
                                        _matrixProcessor.SetValueSigmaValue(ValueStringtoAdd);
                                    }
                                    if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == _matrixProcessor.GetStack() - 1 && v == _matrixProcessor.GetPack() - 1)
                                    {
                                        int omega = Matrix[i, j, n];
                                        _matrixProcessor.SetValueOmegaValue(ValueStringtoAdd);
                                    }



                                    ///Rolled Paper Part/////

                                    if (i == 0 && j == 0 && n == 0 && p == 0 && v == 0)
                                    {
                                        _matrixProcessor.SetValueRolledAlphaValue(ValueStringtoAdd);
                                    }
                                    if (i == 1 && j == 0 && n == 0 && p == 0 && v == 0)
                                    {
                                        _matrixProcessor.SetValueRolledBetaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == Matrix.GetUpperBound(1) && n == 0 && p == 0 && v == 0)
                                    {
                                        _matrixProcessor.SetValueRolledGammaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
                                    {

                                        _matrixProcessor.SetValueRolledDeltaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
                                    {
                                        _matrixProcessor.SetValueRolledDeltaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == Matrix.GetUpperBound(1) && n == 1 && p == 0 && v == 0)
                                    {
                                        _matrixProcessor.SetValueRolledEpsilonValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == 0 && n == 1 && p == 1 && v == 0)
                                    {
                                        _matrixProcessor.SetValueRolledZetaValue(ValueStringtoAdd);
                                    }
                                    if (i == 0 && j == Matrix.GetUpperBound(1) && n == 1 && p == 1 && v == 0)
                                    {
                                        _matrixProcessor.SetValueRolledEtaValue(ValueStringtoAdd);
                                    }
                                    //////////////////////
                                    //////////////////////



                                    int actualLength = 0;
                                    string spacestoadd = "";
                                    while (spacestoadd.Length + ValueStringtoAdd.Length < 14)
                                    {
                                        spacestoadd += " ";
                                    }
                                    //  string spacestoadd = SpacesToadd(14, ValueStringtoAdd);
                                    tempRow = tempRow + spacestoadd + ValueStringtoAdd;
                                    int spacelen = spacestoadd.Length;
                                    int length = tempRow.Length;
                                    //   tempRow = tempRow + ValueStringtoAdd + spacestoadd + " ";
                                    //    tempRow = tempRow + ValueStringtoAdd + "   ";
                                    //even the spaces
                                }
                                Gridcounter++;

                                _listRowsProcessor.ListOfRowsAdd(IndexRow + ColumnMultiplier + "       " + tempRow);
                                twBis.WriteLine("{0}", IndexRow + ColumnMultiplier + "       " + tempRow);
                            }



                            ////Text every X sheets

                            // if (everyx!=0 && n !=0 && (n+1)% everyx == 0)
                            //if (everyx != 0 && (n + 1) % everyx == 0 &&  n+1 % (Matrix.GetUpperBound(2)) != 0)
                            if (everyx != 0 && (n + 1) % everyx == 0 && n != Matrix.GetUpperBound(2))
                            {

                                for (int k = 0; k < NumberOfRecordsPerSheet; k++)
                                {
                                    //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                                    //           {
                                    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                    {
                                        tempRow = string.Empty;
                                        Array.Clear(lineIndex, 0, lineIndex.Length);
                                        indexval = Gridcounter;
                                        indexCount = 0;
                                        for (; indexval != 0; indexval /= 10)
                                        {
                                            lineIndex[indexCount] = indexval % 10;
                                            indexCount++;
                                        }
                                        Array.Reverse(lineIndex);
                                        IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

                                        //index adder end
                                        //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                        int actualLength = 0;
                                        string spacestoadd = "";
                                        while (spacestoadd.Length < spacenumbertoadd)
                                        {
                                            spacestoadd += " ";
                                        }

                                        for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                        {
                                            // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
                                            tempRow = tempRow + spacestoadd + _matrixProcessor.GetTextBetweenSheets();
                                            int spacelen = spacestoadd.Length;
                                            int length = tempRow.Length;
                                            //calculate space evener here
                                        }

                                        Gridcounter++;
                                        _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);
                                        twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
                                    }
                                }

                            }
                        }

                    //////////////////////////Lead out //////////////////////////////////////

                    using (StreamWriter twBis = File.AppendText(pathBis))
                        for (int k = 0; k < LeadOutNumber; k++)
                        {
                            //                 for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                            //                 {
                            //   for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                            for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                            {
                                tempRow = string.Empty;

                                //Index adder start
                                Array.Clear(lineIndex, 0, lineIndex.Length);
                                indexval = Gridcounter;

                                indexCount = 0;
                                for (; indexval != 0; indexval /= 10)
                                {
                                    lineIndex[indexCount] = indexval % 10;
                                    indexCount++;
                                }
                                Array.Reverse(lineIndex);
                                IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";


                                int actualLength = 0;
                                string spacestoadd = "";
                                while (spacestoadd.Length < 14 - LeadOut.Length)
                                {
                                    spacestoadd += " ";
                                }
                                //index adder end
                                // for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                                {
                                    //  string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadOut));
                                    //  tempRow = tempRow + fromCharArrayToString(LeadIn) + spacestoadd + "   ";
                                    //  string spacestoadd = SpacesToadd(ArrayInLength, fromCharArrayToString(LeadOut));
                                    tempRow = tempRow + spacestoadd + fromCharArrayToString(LeadOut);
                                    int length = tempRow.Length;
                                }
                                Gridcounter++;
                                _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);

                                twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
                            }
                        }
                }

                //Text after pack here

                using (StreamWriter twBis = File.AppendText(pathBis))

                    for (int k = 0; k < numberofRecordAfterPack; k++)
                    {
                        //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
                        //           {
                        for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                        {
                            tempRow = string.Empty;
                            Array.Clear(lineIndex, 0, lineIndex.Length);
                            indexval = Gridcounter;
                            indexCount = 0;
                            for (; indexval != 0; indexval /= 10)
                            {
                                lineIndex[indexCount] = indexval % 10;
                                indexCount++;
                            }
                            Array.Reverse(lineIndex);
                            IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

                            //index adder end
                            //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                            int actualLength = 0;
                            string spacestoadd = "";
                            while (spacestoadd.Length < spacenumbertoadd)
                            {
                                spacestoadd += " ";
                            }

                            for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                            {
                                // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
                                tempRow = tempRow + spacestoadd + AfterPackText;
                                int spacelen = spacestoadd.Length;
                                int length = tempRow.Length;
                                //calculate space evener here
                            }

                            Gridcounter++;
                            _listRowsProcessor.ListOfRowsAdd(IndexRow + "01      " + tempRow);
                            twBis.WriteLine("{0}", IndexRow + "01      " + tempRow);

                            //          }
                        }
                    }
            }
        }




        //public void CalculatorRolledVertical()
        //{
        //    int[,,] Matrix;
        //    int ArrayInLength = _matrixProcessor.GetLeadInArrayLength();
        //    int ArrayDigitLength = _matrixProcessor.GetArrayDigitLength();
        //    int[] digits = new int[ArrayDigitLength];
        //    int[] lineIndex = new int[ArrayDigitLength];
        //    int[] zeroarr = new int[ArrayDigitLength];
        //    char[] LeadIn = new char[_matrixProcessor.GetLeadInArray().Length];
        //    char[] LeadOut = new char[_matrixProcessor.GetLeadOutArray().Length];
        //    char[] tempchararray = new char[ArrayInLength];
        //    int LeadInNumber = _matrixProcessor.GetNumberOfLeadIn();
        //    int LeadOutNumber = _matrixProcessor.GetNumberOfLeadOut();
        //    int step = _matrixProcessor.GetStepValue();
        //    List<string> ListofRows = new List<string>();
        //    string tempRow;
        //    int indexval;
        //    int indexCount = 0;
        //    int Gridcounter = 0;
        //    string IndexRow;
        //    int Max = _matrixProcessor.GetMaxNumber();
        //    string modulus = "";
        //    int MaxValuePossible = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
        //    int StartPoint = 0;
        //    _ValueToShow = new ValuesToShow(_matrixProcessor);
        //    int max = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
        //    int NumberOfRecordsPerSheet = _matrixProcessor.GetRecordsOfSheetsNumber();
        //    int everyx = _matrixProcessor.GetEveryXSheetsValue();
        //    string AfterPackText = _matrixProcessor.GetTextBetweenPacks();
        //    int numberofRecordAfterPack = _matrixProcessor.GetRecordsOfPacksNumber();
        //    int negativeflag = 0;
        //    int stringtoint = 0;
        //    int outputlength = _sequenceProcessor.GetSequenceLength();
        //    string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
        //    string pathBis = @".\NumberingTestBis.txt";
        //    //    string pathBis = Path.Combine(location, "NumberingTestBis.txt");
        //    char[] tempcharArray;
        //    char[] baseCharArray;
        //    int spacenumbertoadd = 14 - LeadIn.Length;
        //    bool BasechararrayOption = _matrixProcessor.GetAddZeroeValue();
        //    string Alignment = _matrixProcessor.GetAlignment();
        //    int pmax = _matrixProcessor.GetStack();
        //    int vmax = _matrixProcessor.GetPack();
        //    int ColumnMultiplierInt = _matrixProcessor.GetMultiplyByColumnValue();
        //    string ColumnMultiplier = FromintToStringWithZero(2, ColumnMultiplierInt);
        //    int _SheeMultiplier = _matrixProcessor.GetMultiplyBySheetValue();
        //    if (_matrixProcessor.GetStepOrder() != "+")
        //    {

        //        step = -step;
        //    }
        //    StartPoint = (_alphabetConverterHelper.UnformatValue(_matrixProcessor.GetStartPoint().ToString()));
        //    Array.Copy(_matrixProcessor.GetLeadInArray(), LeadIn, _matrixProcessor.GetLeadInArray().Length);
        //    Array.Copy(_matrixProcessor.GetLeadOutArray(), LeadOut, _matrixProcessor.GetLeadOutArray().Length);
        //    //  BasechararrayOption = _matrixProcessor.GetIsChecked();
        //    //   using (StreamWriter twBis = File.CreateText(pathBis))


        //    Matrix = _matrixProcessor.GetMatrix();

        //    for (int v = 0; v < _matrixProcessor.GetPack(); v++)
        //    {
        //        for (int p = 0; p < _matrixProcessor.GetStack(); p++)
        //        {
        //            using (StreamWriter twBis = File.AppendText(pathBis))
        //                for (int k = 0; k < LeadInNumber; k++)
        //                {
        //                    //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
        //                    //           {
        //                    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                    {
        //                        tempRow = string.Empty;
        //                        Array.Clear(lineIndex, 0, lineIndex.Length);
        //                        indexval = Gridcounter;
        //                        indexCount = 0;
        //                        for (; indexval != 0; indexval /= 10)
        //                        {
        //                            lineIndex[indexCount] = indexval % 10;
        //                            indexCount++;
        //                        }
        //                        Array.Reverse(lineIndex);
        //                        IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

        //                        //index adder end
        //                        //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                        int actualLength = 0;
        //                        string spacestoadd = "";
        //                        while (spacestoadd.Length < 14 - _matrixProcessor.GetTextBetweenSheets().Length)
        //                        {
        //                            spacestoadd += " ";
        //                        }

        //                        for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
        //                        {
        //                            // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
        //                            tempRow = tempRow + spacestoadd + fromCharArrayToString(LeadIn);
        //                            int spacelen = spacestoadd.Length;
        //                            int length = tempRow.Length;
        //                            //calculate space evener here
        //                        }

        //                        Gridcounter++;
        //                        _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);
        //                        twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
        //                    }
        //                    //          }
        //                }
        //            using (StreamWriter twBis = File.AppendText(pathBis))
        //                //for (int mult = 0; mult < _SheeMultiplier; mult++)
        //                //{
        //                for (int n = 0; n <= Matrix.GetUpperBound(2); n++)
        //                {
        //                    //for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
        //                    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                    {
        //                        tempRow = string.Empty;
        //                        //Index adder start
        //                        Array.Clear(lineIndex, 0, lineIndex.Length);
        //                        indexval = Gridcounter;

        //                        indexCount = 0;
        //                        for (; indexval != 0; indexval /= 10)
        //                        {
        //                            lineIndex[indexCount] = indexval % 10;
        //                            indexCount++;
        //                        }
        //                        Array.Reverse(lineIndex);
        //                        IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

        //                        //index adder end
        //                        //    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                        for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
        //                        {

        //                            Array.Clear(digits, 0, digits.Length);
        //                            int count = 0;
        //                            negativeflag = 0;

        //                            int val = StartPoint + step * ((v * (_matrixProcessor.GetStack() * (Max + 1))) + p * Max + p + (Matrix[i, j, n]));


        //                            ///Calculate Weighting
        //                            ///
        //                            val = _ancillaryFunctions.CalculateWightedResult(val);
        //                            ///

        //                            if (val >= MaxValuePossible)
        //                            {
        //                                while (val >= MaxValuePossible)
        //                                {
        //                                    val = val - MaxValuePossible;
        //                                }
        //                            }
        //                            if (_matrixProcessor.GetModulusType() == "Modulus")
        //                            {
        //                                modulus = CalculateModulus(val);
        //                            }
        //                            else if (_matrixProcessor.GetModulusType() == "Modulus w/o 0")
        //                            {
        //                                modulus = CalculateModulus(val - 1);
        //                            }
        //                            else if (_matrixProcessor.GetModulusType() == "Complement")
        //                            {
        //                                modulus = CalculateComplement(val);
        //                            }
        //                            else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
        //                            {
        //                                modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
        //                            }
        //                            else if (_matrixProcessor.GetModulusType() == "Mod10 Weighted3*1")
        //                            {
        //                                modulus = _ancillaryFunctions.CheckDigitMod103and1Calculator(val).ToString();
        //                            }

        //                            //  int val = 100;
        //                            // digits = val.ToString().Select(o => Convert.ToInt32(o)).ToArray();
        //                            //for (; val != 0; val /= 10)
        //                            //{
        //                            //    digits[count] = val % 10;
        //                            //    count++;
        //                            //}

        //                            if (val < 0)
        //                            {
        //                                negativeflag = 1;
        //                                val = max - (-1 * val) + 1;
        //                            }

        //                            int[] flexdigits = new int[val.ToString().Length];
        //                            for (int d = flexdigits.Length - 1; d >= 0; d--)
        //                            {
        //                                flexdigits[d] = val % 10;
        //                                val /= 10;
        //                            }
        //                            //Array.Reverse(digits);

        //                            //if (_matrixProcessor.GetBitCheck() != "None")
        //                            //{
        //                            //    if (_matrixProcessor.GetBitCheck() == "Modulus 2")
        //                            //    {
        //                            //     //   modulus = "2";
        //                            //        flexdigits[0] = Matrix[i, j, n] % 2;
        //                            //    }
        //                            //    else { flexdigits[0] = Matrix[i, j, n] % 3; }

        //                            //}


        //                            Int32.TryParse(fromArrayToString(flexdigits), out stringtoint);


        //                            //fill Array and turn into string
        //                            baseCharArray = _alphabetConverterHelper.makeBaseArray();
        //                            int indexbase = baseCharArray.Length - 1;
        //                            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(stringtoint, 0, "");

        //                            if (BasechararrayOption == true)
        //                            {
        //                                tempcharArray = ValueStringtoAdd.ToCharArray();
        //                                for (int q = tempcharArray.Length - 1; q >= 0; q--)
        //                                {
        //                                    baseCharArray[indexbase--] = tempcharArray[q];
        //                                }
        //                                //    Array.Reverse(baseCharArray);
        //                                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
        //                            }
        //                            if (_matrixProcessor.GetModulusPosition() == "Beginning")
        //                            {
        //                                ValueStringtoAdd = modulus + ValueStringtoAdd;
        //                            }
        //                            else
        //                            {
        //                                ValueStringtoAdd = ValueStringtoAdd + modulus;
        //                            }
        //                            //   if (negativeflag == 1)

        //                            if (int.TryParse(ValueStringtoAdd, out int x) == true && negativeflag == 1)
        //                            { ValueStringtoAdd = "-" + ValueStringtoAdd; }




        //                            ///////////////////////

        //                            if (i == 0 && j == 0 && n == 0 && v == 0 && p == 0)
        //                            {
        //                                _matrixProcessor.SetValueAlphaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 1 && n == 0 && v == 0 && p == 0)
        //                            {
        //                                int ValueAlphaZeroOne = Matrix[i, j, n];
        //                                _matrixProcessor.SetValueAlphaZeroOneValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 1 && j == 0 && n == 0 && v == 0 && p == 0)
        //                            {
        //                                int ValueAlphaOneZero = Matrix[i, j, n];
        //                                _matrixProcessor.SetValueAlphaOneZeroValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == Matrix.GetUpperBound(1) && n == 0 && v == 0 && p == 0)
        //                            {
        //                                _matrixProcessor.SetValueBetaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && v == 0 && p == 0)
        //                            {
        //                                _matrixProcessor.SetValueGammaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == 0 && v == 0 && p == 0)
        //                            {
        //                                _matrixProcessor.SetValueDeltaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
        //                            {
        //                                int eps = Matrix[i, j, n];
        //                                _matrixProcessor.SetValueEpsilonValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
        //                            {
        //                                _matrixProcessor.SetValueSigmaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 1 && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
        //                            {
        //                                _matrixProcessor.SetValueSigmaZeroOneValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && v == 0 && p == 0)
        //                            {
        //                                _matrixProcessor.SetValueSigmaZeroLastValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueZetaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == 0 && p == _matrixProcessor.GetStack() - 1 && v == 0)
        //                            {
        //                                int eta = Matrix[i, j, n];
        //                                _matrixProcessor.SetValueEtaValue(ValueStringtoAdd);
        //                            }


        //                            if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == _matrixProcessor.GetStack() - 1 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueThetaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == 0 && p == 0 && v == 1)
        //                            {
        //                                _matrixProcessor.SetValueIotaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == 0 && v == 1)
        //                            {
        //                                _matrixProcessor.SetValueKappaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == 1 && p == 1 && v == 1 && _matrixProcessor.GetStack() - 1 != 0 && _matrixProcessor.GetPack() - 1 != 0)
        //                            {
        //                                _matrixProcessor.SetValueLambdaValue(ValueStringtoAdd);
        //                            }



        //                            if (i == 0 && j == 0 && n == 0 && p == _matrixProcessor.GetStack() - 1 && v == 1 && _matrixProcessor.GetPack() - 1 != 0 && _matrixProcessor.GetStack() - 1 != 0)
        //                            {
        //                                _matrixProcessor.SetValueMuValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == _matrixProcessor.GetStack() - 1 && v == 1 && _matrixProcessor.GetPack() - 1 != 0 && _matrixProcessor.GetStack() - 1 != 0)
        //                            {
        //                                _matrixProcessor.SetValueNuValue(ValueStringtoAdd);
        //                            }


        //                            if (i == 0 && j == 0 && n == 0 && p == 0 && v == _matrixProcessor.GetPack() - 1)
        //                            {
        //                                _matrixProcessor.SetValueXiValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == 0 && v == _matrixProcessor.GetPack() - 1)
        //                            {
        //                                _matrixProcessor.SetValueOmicronValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == 0 && p == 1 && v == _matrixProcessor.GetPack() - 1)
        //                            {
        //                                int pi = Matrix[i, j, n];
        //                                _matrixProcessor.SetValuePiValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == _matrixProcessor.GetStack() - 1 && v == _matrixProcessor.GetPack() - 1)
        //                            {
        //                                int ro = Matrix[i, j, n];
        //                                _matrixProcessor.SetValueRoValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == 0 && n == 0 && p == 0 && v == _matrixProcessor.GetPack() - 1)
        //                            {
        //                                _matrixProcessor.SetValueSigmaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == Matrix.GetUpperBound(0) && j == Matrix.GetUpperBound(1) && n == Matrix.GetUpperBound(2) && p == _matrixProcessor.GetStack() - 1 && v == _matrixProcessor.GetPack() - 1)
        //                            {
        //                                int omega = Matrix[i, j, n];
        //                                _matrixProcessor.SetValueOmegaValue(ValueStringtoAdd);
        //                            }



        //                            ///Rolled Paper Part/////

        //                            if (i == 0 && j == 0 && n == 0 && p == 0 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueRolledAlphaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 1 && j == 0 && n == 0 && p == 0 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueRolledBetaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == Matrix.GetUpperBound(1) && n == 0 && p == 0 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueRolledGammaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
        //                            {

        //                                _matrixProcessor.SetValueRolledDeltaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == 1 && p == 0 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueRolledDeltaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == Matrix.GetUpperBound(1) && n == 1 && p == 0 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueRolledEpsilonValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == 0 && n == 1 && p == 1 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueRolledZetaValue(ValueStringtoAdd);
        //                            }
        //                            if (i == 0 && j == Matrix.GetUpperBound(1) && n == 1 && p == 1 && v == 0)
        //                            {
        //                                _matrixProcessor.SetValueRolledEtaValue(ValueStringtoAdd);
        //                            }
        //                            //////////////////////
        //                            //////////////////////



        //                            int actualLength = 0;
        //                            string spacestoadd = "";
        //                            while (spacestoadd.Length + ValueStringtoAdd.Length < 14)
        //                            {
        //                                spacestoadd += " ";
        //                            }
        //                            //  string spacestoadd = SpacesToadd(14, ValueStringtoAdd);
        //                            tempRow = tempRow + spacestoadd + ValueStringtoAdd;
        //                            int spacelen = spacestoadd.Length;
        //                            int length = tempRow.Length;
        //                            //   tempRow = tempRow + ValueStringtoAdd + spacestoadd + " ";
        //                            //    tempRow = tempRow + ValueStringtoAdd + "   ";
        //                            //even the spaces
        //                        }
        //                        Gridcounter++;

        //                        _listRowsProcessor.ListOfRowsAdd(IndexRow + ColumnMultiplier + "       " + tempRow);
        //                        twBis.WriteLine("{0}", IndexRow + ColumnMultiplier + "       " + tempRow);
        //                    }



        //                    ////Text every X sheets

        //                    // if (everyx!=0 && n !=0 && (n+1)% everyx == 0)
        //                    //if (everyx != 0 && (n + 1) % everyx == 0 &&  n+1 % (Matrix.GetUpperBound(2)) != 0)
        //                    if (everyx != 0 && (n + 1) % everyx == 0 && n != Matrix.GetUpperBound(2))
        //                    {

        //                        for (int k = 0; k < NumberOfRecordsPerSheet; k++)
        //                        {
        //                            //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
        //                            //           {
        //                            for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                            {
        //                                tempRow = string.Empty;
        //                                Array.Clear(lineIndex, 0, lineIndex.Length);
        //                                indexval = Gridcounter;
        //                                indexCount = 0;
        //                                for (; indexval != 0; indexval /= 10)
        //                                {
        //                                    lineIndex[indexCount] = indexval % 10;
        //                                    indexCount++;
        //                                }
        //                                Array.Reverse(lineIndex);
        //                                IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

        //                                //index adder end
        //                                //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                                int actualLength = 0;
        //                                string spacestoadd = "";
        //                                while (spacestoadd.Length < spacenumbertoadd)
        //                                {
        //                                    spacestoadd += " ";
        //                                }

        //                                for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
        //                                {
        //                                    // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
        //                                    tempRow = tempRow + spacestoadd + _matrixProcessor.GetTextBetweenSheets();
        //                                    int spacelen = spacestoadd.Length;
        //                                    int length = tempRow.Length;
        //                                    //calculate space evener here
        //                                }

        //                                Gridcounter++;
        //                                _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);
        //                                twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
        //                            }
        //                        }

        //                    }
        //                }

        //            //////////////////////////Lead out //////////////////////////////////////

        //            using (StreamWriter twBis = File.AppendText(pathBis))
        //                for (int k = 0; k < LeadOutNumber; k++)
        //                {
        //                    //                 for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
        //                    //                 {
        //                    //   for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
        //                    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                    {
        //                        tempRow = string.Empty;

        //                        //Index adder start
        //                        Array.Clear(lineIndex, 0, lineIndex.Length);
        //                        indexval = Gridcounter;

        //                        indexCount = 0;
        //                        for (; indexval != 0; indexval /= 10)
        //                        {
        //                            lineIndex[indexCount] = indexval % 10;
        //                            indexCount++;
        //                        }
        //                        Array.Reverse(lineIndex);
        //                        IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";


        //                        int actualLength = 0;
        //                        string spacestoadd = "";
        //                        while (spacestoadd.Length < 14 - LeadOut.Length)
        //                        {
        //                            spacestoadd += " ";
        //                        }
        //                        //index adder end
        //                        // for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                        for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
        //                        {
        //                            //  string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadOut));
        //                            //  tempRow = tempRow + fromCharArrayToString(LeadIn) + spacestoadd + "   ";
        //                            //  string spacestoadd = SpacesToadd(ArrayInLength, fromCharArrayToString(LeadOut));
        //                            tempRow = tempRow + spacestoadd + fromCharArrayToString(LeadOut);
        //                            int length = tempRow.Length;
        //                        }
        //                        Gridcounter++;
        //                        _listRowsProcessor.ListOfRowsAdd(IndexRow + "01       " + tempRow);

        //                        twBis.WriteLine("{0}", IndexRow + "01       " + tempRow);
        //                    }
        //                }
        //        }

        //        //Text after pack here

        //        using (StreamWriter twBis = File.AppendText(pathBis))

        //            for (int k = 0; k < numberofRecordAfterPack; k++)
        //            {
        //                //           for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
        //                //           {
        //                for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                {
        //                    tempRow = string.Empty;
        //                    Array.Clear(lineIndex, 0, lineIndex.Length);
        //                    indexval = Gridcounter;
        //                    indexCount = 0;
        //                    for (; indexval != 0; indexval /= 10)
        //                    {
        //                        lineIndex[indexCount] = indexval % 10;
        //                        indexCount++;
        //                    }
        //                    Array.Reverse(lineIndex);
        //                    IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

        //                    //index adder end
        //                    //      for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
        //                    int actualLength = 0;
        //                    string spacestoadd = "";
        //                    while (spacestoadd.Length < spacenumbertoadd)
        //                    {
        //                        spacestoadd += " ";
        //                    }

        //                    for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
        //                    {
        //                        // string spacestoadd = SpacesToadd(14, fromCharArrayToString(LeadIn));
        //                        tempRow = tempRow + spacestoadd + AfterPackText;
        //                        int spacelen = spacestoadd.Length;
        //                        int length = tempRow.Length;
        //                        //calculate space evener here
        //                    }

        //                    Gridcounter++;
        //                    _listRowsProcessor.ListOfRowsAdd(IndexRow + "01      " + tempRow);
        //                    twBis.WriteLine("{0}", IndexRow + "01      " + tempRow);

        //                    //          }
        //                }
        //            }
        //    }
        //}




        public string FromintToStringWithZero(int ZeroArrayLength, int input)
        {
            string result = "";

            int[] ZeroArray = new int[ZeroArrayLength];
            int indexCount = 0;
            for (; input != 0; input /= 10)
            {
                ZeroArray[indexCount] = input % 10;
                indexCount++;
            }
            Array.Reverse(ZeroArray);
            result = fromArrayToString(ZeroArray);
            return result;
        }

        public string CalculateComplement(int input)
        {
            int result;
            int mod = _matrixProcessor.GetModulus();
            int modlength = mod.ToString().Length;
            int[] inputarray = new int[modlength];
            int inputarraylength = inputarray.Length;
            result = input;
            if (input.ToString().Length > modlength)
            {
                while (modlength > 0)
                {
                    inputarray[--inputarraylength] = input % 10;
                    input /= 10;
                    modlength--;
                }
                result = FromArrayofIntToInt(inputarray);
            }
            int complement = Math.Abs(_matrixProcessor.GetModulus() - (result % 10));

            return complement.ToString();
        }


        public int FromArrayofIntToInt(int[] inputarray)
        {
            int finalScore = 0;
            for (int i = 0; i < inputarray.Length; i++)
            {
                finalScore += inputarray[i] * Convert.ToInt32(Math.Pow(10, inputarray.Length - i - 1));
            }
            return finalScore;
        }

        public string CalculateModulus(int input)
        {
            string result = "";
            int modulus = _matrixProcessor.GetModulus();

            result = (input % modulus).ToString();
            return result;
        }
        public string fromArrayToString(int[] input)
        {
            string result = string.Join("", input);
            return result;
        }
        public string fromCharArrayToString(char[] input)
        {
            string result = string.Join("", input);
            return result;
        }
        public string SpacesToadd(int generalArraylength, string tosubtract)
        {
            string spacesline = "";
            int spaces = generalArraylength - tosubtract.Length;
            for (int h = 0; h < spaces; h++)
            {
                spacesline = spacesline + "  ";
            }
            return spacesline;
        }
    }
}