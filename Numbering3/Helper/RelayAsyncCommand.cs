﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Numbering3.Helper
{
    public class RelayAsyncCommand : ICommand
    {
        #region Fields

        //readonly Action<object> _execute;
        //readonly Predicate<object> _canExecute;

        #endregion // Fields

        #region Constructors

        Func<object, Task> _asyncexecute;
        Func<object, bool> _canexecute;

        public RelayAsyncCommand(Func<object, Task> execute) : this(execute, null) { }
        public RelayAsyncCommand(Func<object, Task> execute, Func<object, bool> canexecute)
        {
            _asyncexecute = execute;
            _canexecute = canexecute;
        }

        //public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        //{
        //    if (execute == null)
        //        throw new ArgumentNullException("execute");

        //    _execute = execute;
        //    _canexecute = canexecute;
        //}
        #endregion // Constructors

        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            if (_canexecute != null)
            {
                return _canexecute(parameter);
            }
            else
            {
                return false;
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }


        public async void Execute(object parameter) => await this._asyncexecute((object)parameter);



        #endregion // ICommand Members
    }
}
