﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Helper
{
    public class TypeElement
    {
        private int lengthofsamelement;
        private char type;

        public TypeElement(int lengthofsamelement, char type)
        {
            this.lengthofsamelement = lengthofsamelement;
            this.type = type;
        }

        public int LengtOfSameElement
        {
            get { return lengthofsamelement; }
            set { lengthofsamelement = value; }
        }
        public char Type
        {
            get { return type; }
            set { type = value; }
        }
    }
}
