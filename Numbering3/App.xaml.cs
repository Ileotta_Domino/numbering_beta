﻿using Numbering3.View;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const int splash_time = 4000;

        protected override void OnStartup(StartupEventArgs e)
        {
            SplashPage splash = new SplashPage();
            splash.Show();
            Stopwatch timer = new Stopwatch();
            timer.Start();
            base.OnStartup(e);
            MainWindow main = new MainWindow();
            timer.Stop();
            int remaining_time = splash_time - (int)timer.ElapsedMilliseconds;
            if (remaining_time > 0)
                Thread.Sleep(remaining_time);
            splash.Close();
        }
    }
}
