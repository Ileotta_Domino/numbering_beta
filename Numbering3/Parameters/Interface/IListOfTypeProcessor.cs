﻿using Numbering3.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters.Interface
{
    public interface IListOfTypeProcessor
    {
        void CreateListOfType();
        List<TypeElement> ListOfTypeValue { get; set; }
        void AddToList(TypeElement input);

        void RemoveLastInList();
        //void CreateStackOfType();
        //void AddToStackOfType(TypeElement input);
        //TypeElement PeekStackValue();
        //TypeElement PopStackValue();
        //int CountStack();
        //Stack<TypeElement> StackOfTypeGet { get; }
        void ReverseTypeList();
        int CountTypeList();
    }
}
