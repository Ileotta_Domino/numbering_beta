﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters.Interface
{
    public interface IWeightingProcessor
    {
        int[] GetWeightingArray();
        void SetWeightingArray(int[] input);
        void AddValueToWeightinhArray(int pos, int input);
    }
}
