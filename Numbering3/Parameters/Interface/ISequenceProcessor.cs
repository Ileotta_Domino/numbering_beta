﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters.Interface
{
    public interface ISequenceProcessor
    {
        void AssignAlphabet(string input);
        string GetAlphabet();
        void SetAlphabet(string input);
        string GetAlphabetC();
        void SetAlphabetC(string input);
        string GetSequence();
        void SetSequence(string input);
        int GetSequenceLength();
        void SetSequenceLength(int input);
        bool GetIsSequenceValue();
        void SetIsSequenceValue(bool input);
        void CreateSequenceArray(int length, string input);
        char[] GetSequenceArray();
        void SetSequenceToKeepValue(string input);
        string GetSequenceToKeepValue();
        string GetAlphabetToKeepValue();
        void SetAlphabetToKeepValue(string input);
        string GetAlphabetCToKeepValue();
        void SetAlphabetCToKeepValue(string input);
    }
}
