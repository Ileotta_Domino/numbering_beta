﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters.Interface
{
    public interface IListRowsProcessor
    {
        void ListOfRowsCreate();
        void ListOfRowsCopy(List<string> input);
        void ListOfRowsAppend(List<string> input);
        List<string> ListOfRowsGet();
        void ListOfRowsReset();
        void ListOfRowsAdd(string row);
    }
}
