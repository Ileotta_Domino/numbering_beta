﻿using Numbering3.Helper;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class ListOfTypeProcessor : IListOfTypeProcessor
    {

        //public void CreateStackOfType()
        //{
        //    ListOfTypeKeeper.CreateStackOfType();
        //}

        //public void AddToStackOfType(TypeElement input)
        //{
        //    ListOfTypeKeeper.AddToStack(input);
        //}

        //public TypeElement PeekStackValue()
        //{
        //    return ListOfTypeKeeper.PeekStack();
        //}

        //public TypeElement PopStackValue()
        //{
        //    return ListOfTypeKeeper.PopTypeStack();
        //}

        //public Stack<TypeElement> StackOfTypeGet
        //{
        //    get
        //    { return ListOfTypeKeeper.TypeStackValue; }

        //}


        //public int CountStack()
        //{
        //    return ListOfTypeKeeper.CountStack();
        //}

        public void CreateListOfType()
        {
            ListOfTypeKeeper.CreateListOfType();
        }


        public List<TypeElement> ListOfTypeValue
        {
            get
            { return ListOfTypeKeeper.TypeListValue; }
            set { ListOfTypeKeeper.TypeListValue = value; }
        }


        public void AddToList(TypeElement input)
        {
            ListOfTypeKeeper.AddToList(input);
        }

        public void RemoveLastInList()
        {
            int length = ListOfTypeValue.Count();
            ListOfTypeKeeper.RemoveAt(length - 1);
        }

        public void ReverseTypeList()
        {
            ListOfTypeKeeper.ReverseList();
        }
        public int CountTypeList()
        {
          return  ListOfTypeKeeper.CountList();
        }
    }
}
