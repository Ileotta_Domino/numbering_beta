﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class SaveProcessor : ISaveProcessor
    {
        public string GetSaveName()
        {
            return SaveKeeper.SaveNameValue;
        }
        public void SetSaveName(string input)
        {
            SaveKeeper.SaveNameValue = input;
        }

        public List<string> GetConfigurationList()
        {
        return    SaveKeeper.ConfigudationListValue;
        }
        public void CreateList()
        {
            SaveKeeper.CreateConfigurationList();
        }

        public void AddToConfigurationList(string input)
        {
            SaveKeeper.AddToConfigurationList(input);
        }

    }
}
