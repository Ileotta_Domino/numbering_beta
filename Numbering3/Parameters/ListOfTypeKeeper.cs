﻿using Numbering3.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class ListOfTypeKeeper
    {
        private static List<TypeElement> TypeList; //=new List<TypeElement>();


        public static Stack<TypeElement> TypeStack = new Stack<TypeElement>();


        public static void CreateStackOfType()
        {
            Stack<TypeElement> StackOfType = new Stack<TypeElement>();
        }


        public static void AddToStack(TypeElement input)
        {
            TypeStack.Push(input);
        }

        public static TypeElement PeekStack()
        {
            return TypeStack.Peek();
        }

        public static TypeElement PopTypeStack()
        {
            return TypeStack.Pop();
        }

        public static int CountStack()
        {
            return TypeStack.Count();
        }

        public static Stack<TypeElement> TypeStackValue
        { get; set; }



        public static void CreateListOfType()
        {
             TypeList = new List<TypeElement>();
        }
        public static List<TypeElement> TypeListValue
        {
            get
            { return TypeList; }
                set { TypeList = value; } }

        public static void AddToList(TypeElement input)
        {
            TypeList.Add(input);
        }

        public static void RemoveAt(int input)
        {
            TypeList.RemoveAt(input);
        }

        public static int CountList()
        {
            return TypeList.Count();
        }

        public static void ReverseList()
        {
           TypeList.Reverse();
        }

    }
}
