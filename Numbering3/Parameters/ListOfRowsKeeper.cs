﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class ListOfRowsKeeper
    {
        private static List<string> ListRows;

        public static void CreateListOfRows()
        {
            ListRows = new List<string>();
        }

        public static void AddtoListOfRow(string row)
        {
            ListRows.Add(row);
        }

        public static List<string> ListOfRowsValue
        {
            get { return ListRows;}
            set { ListRows = value; }
        }

        public static void ResetListOfRows()
        {
            ListRows.Clear();
        }
    }
}
