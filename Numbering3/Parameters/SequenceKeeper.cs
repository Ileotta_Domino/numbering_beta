﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class SequenceKeeper
    {
        private static string Alphabet;
        private static string AlphabetC;
        private static int SequenceLength;
        private static bool IsSequence;
        private static char[] SequenceArray;
        private static string SequenceToKeep;
        private static string AlphabetToKeep;
        private static string AlphabetCToKeep;
        private static string Sequence = "NNN";

        public static void CreateAlphabet(string input)
        {
            Alphabet = input;
        }

        public static string SequenceValue
        {
            get { return Sequence; }
            set { Sequence = value; }
        }

        public static int SequenceLengthValue
        {
            get { return SequenceValue.Length; }
            set { SequenceLength = value; }
        }

        public static string AlphabetValue
        {
            get { return Alphabet; }
            set { Alphabet = value; }
        }
        public static string AlphabetCValue
        {
            get { return AlphabetC; }
            set { AlphabetC = value; }
        }

        

        public static bool IsSequenceValue
        {
            get { return IsSequence; }
            set { IsSequence = value; }
        }

        public static void CreateSequenceArrayValue(int length, string input)
        {
            SequenceArray = new char[length];
            SequenceArray = input.ToCharArray();
        }

        public static char[] SequenceArrayValue
        {
            get { return SequenceArray; }
            set { SequenceArray = value; }
        }
        public static string SequenceToKeepValue
        {
            get { return SequenceToKeep; }
            set { SequenceToKeep = value; }
        }
        public static string AlphabetToKeepValue
        {
            get { return AlphabetToKeep; }
            set { AlphabetToKeep = value; }
        }
        public static string AlphabetCToKeepValue
        {
            get { return AlphabetCToKeep; }
            set { AlphabetCToKeep = value; }
        }
        
    }
}
