﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{

    public class WeightingProcessor: IWeightingProcessor
    {
        public int[] GetWeightingArray()
        {
            return WeightingKeeper.WeightArrayValue ;
        }
        public void SetWeightingArray(int[] input)
        {
            WeightingKeeper.WeightArrayValue = input;
        }

        public void AddValueToWeightinhArray (int pos, int input)
        {
            WeightingKeeper.WeightArrayAddValue(pos, input);
        }
    }
}
