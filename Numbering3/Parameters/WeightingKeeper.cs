﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class WeightingKeeper
    {
        private static int[] WeightArray=new int[14] { 1,1,1,1,1,1,1,1,1,1,1,1,1,1};

        public static int[] WeightArrayValue
        {
            get { return WeightArray; }
            set { WeightArray = value; }
        }

        public static void WeightArrayAddValue(int pos, int input)
        {
            WeightArray[pos]= input;
        }
    }
}
