﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class MatrixKeeper
    {
        private static int[,,] Matrix;
        private static int[,,] RolledMatrix;
        private static int[,] MatrixTest;
        //int[,,] Matrix = new int[,,] { { { 0, 0, 0 }, { 0, 0, 0 } },
        //    { { 0, 0, 0 }, { 0, 0, 0 } },
        //    { { 0, 0, 0 }, { 0, 0, 0 } } };
        private static int Row;
        private static int Column;
        private static int Sheet;
        private static int Stack=1;
        private static int Pack=1;
        private static string OrderSelected;
        private static string AlignmentSelected;
        private static string StepOrderSelected="+";
        private static int ArrayDigitLength;
        private static int ArrayInLength;
        private static int ArrayOutLength;
        private static int LeadInNumber = 0;
        private static int LeadOutNumber = 0;
        private static string BitCheck;
        private static char[] LeadInArray;
        private static char[] LeadOutArray;
        private static List<List<string>> OutputList;
        private static List<string> Outputsublist;
        private static int RepetitionNumber = 0;
        private static int StepNumber=1;
        private static int[,,] DefaultMatrix = new int[1, 1, 1] { { { 0 } } };
        private static string LeadInToKeep ;
        private static string LeadOutToKeep;
        private static int LeadInNumberToKeep;
        private static int LeadOutNumberToKeep;
        private static int StepKeep=1;
        private static int MaxNumber;
        private static string StartPoint = "1";
        private static string StartPointToKeep = "1";

        private static bool LeadInFlag = false;
        private static bool LeadOutFlag;
        private static bool IsChecked ;
        private static string ValueZeroZeroValue="0";
        private static string ValueZeroLastValue="0";
        private static string ValueLastLastValue = "0";
        private static string ValueZeroZeroLastValue = "0";
        private static string ValueZeroZeroOneStackValue = "0";
        private static string ValueLastLastPackValue = "0";
        private static string ValueFirstSheetFirstStackLastEleValue = "0";
        private static string ValueLastSheetFirstStackFirstElementValue = "0";
        private static string StepOrderValue;
        private static int ModulusValue=1;
        private static string ModulusTypeSelectedValue = "Modulus";
        private static string ModulusTypePositionValue = "Beginning";
        private static int RecordsOfPacks = 0;
        private static string TextBetweenPacksToShow = "";
        private static int EveryXSheets = 0;
        private static int RecordsOfSheets = 0;
        private static string TextBetweenSheetsToShow = "";
        private static int MultiplyByColumn = 1;
        private static int MultiplyBySheet = 1;
        private static string TextBetweenPacksToKeep="None";
        private static int RecordsOfPacksToKeep = 0;
        private static int EveryXSheetsToKeep=1;
        private static string TextBetweenSheetsToKeep="None";
        private static int RecordsOfSheetToKeep=0;
        private static string ConfigurationFile = "";
        private static bool AddZero=true;
        private static bool CheckDigit = true;
        private static string AlphaValue = "0";
        private static string AlphaZeroOneValue = "0";
        private static string AlphaOneZeroValue = "0";
        private static string BetaValue = "0";
        private static string GammaValue = "0";
        private static string DeltaValue = "0";
        private static string EpsilonValue = "0";
        private static string ZetaValue = "0";
        private static string EtaValue = "0";
        private static string ThetaValue = "0";
        private static string IotaValue = "0";
        private static string KappaValue = "0";
        private static string LambdaValue = "0";
        private static string MuValue = "0";
        private static string NuValue = "0";
        private static string XiValue = "0";
        private static string OmicronValue = "0";
        private static string PiValue = "0";
        private static string RoValue = "0";
        private static string SigmaValue = "0";
        private static string SigmaZeroOneValue = "0";
        private static string SigmaZeroLastValue = "0";
        private static string OmegaValue = "0";
        private static string FirstNewSheetValue ="0";


        private static string RolledAlphaValue = "0";
        private static string RolledBetaValue = "0";
        private static string RolledGammaValue = "0";
        private static string RolledDeltaValue = "0";
        private static string RolledEpsilonValue = "0";
        private static string RolledZetaValue = "0";
        private static string RolledEtaValue = "0";
        private static string RolledThetaValue = "0";
        private static string RolledIotaValue = "0";
        private static string RolledKappaValue = "0";

        public static string BitCheckValue
        {
            get { return BitCheck; }
            set { BitCheck = value; }
        }

        public static int[,,] MatrixValue
        {
            get { return Matrix; }
            set { Matrix = value; }
        }
        public static int[,,] RolledMatrixValue
        {
            get { return RolledMatrix; }
            set { RolledMatrix = value; }
        }

        public static int[,] MatrixTestValue
        {
            get { return MatrixTest; }
            set { MatrixTest = value; }
        }

        public static int RowValue
        {
            get { return Row; }
            set { Row = value; }
        }

        public static int ColumnValue
        {
            get { return Column; }
            set { Column = value; }
        }

        public static int SheetValue
        {
            get { return Sheet; }
            set { Sheet = value; }
        }
        public static int StackValue
        {
            get { return Stack; }
            set { Stack = value; }
        }

        public static int PackValue
        {
            get { return Pack; }
            set { Pack = value; }
        }
        public static string AlignmentSelectedName
        {
            get { return AlignmentSelected; }
            set { AlignmentSelected = value; }
        }
        public static string OrderSelectedName
        {
            get { return OrderSelected; }
            set { OrderSelected = value; }
        }
        public static void CreateLeadInArray()
        {
            LeadInArray = new char[ArrayInLengthValue];
        }
        public static void CreateLeadOutArray()
        {
            LeadOutArray = new char[ArrayOutLengthValue];
        }
        
        public static int ArrayDigitLengthValue
        {
            get { return ArrayDigitLength; }
            set { ArrayDigitLength = value; }
        }

        public static int ArrayInLengthValue
        {
            get { return ArrayInLength; }
            set { ArrayInLength = value; }
        }

        public static int ArrayOutLengthValue
        {
            get { return ArrayOutLength; }
            set { ArrayOutLength = value; }
        }

        public static int LeadInNumberValue
        {
            get { return LeadInNumber; }
            set { LeadInNumber = value; }
        }
        public static int LeadOutNumberValue
        {
            get { return LeadOutNumber; }
            set { LeadOutNumber = value; }
        }
        public static char[] LeadInArrayValue
        {
            get { return LeadInArray; }
            set { LeadInArray = value; }
        }
        public static char[] LeadOutArrayValue
        {
            get { return LeadOutArray; }
            set { LeadOutArray = value; }
        }
        public static bool LeadInFlagValue
        {
            get { return LeadInFlag; }
            set { LeadInFlag = value; }
        }
        public static bool LeadOutFlagValue
        {
            get { return LeadOutFlag; }
            set { LeadOutFlag = value; }
        }
        public static void CreateJaggedList()
        {
            OutputList = new List<List<String>>();
        }

        public static void CreateSubList()
        {
            Outputsublist = new List<String>();
        }
        public static void AddtoSublist(string input)
        {
            Outputsublist.Add(input);
        }

        public static void ClearSublist()
        {
            Outputsublist.Clear();
        }

        public static void AddToOutputList()
        {
            OutputList.Add(Outputsublist);
        }

        public static void ClearOutputList()
        {
            OutputList.Clear();
        }
        public static int RepetitionNumberValue
        {
            get { return RepetitionNumber; }
            set { RepetitionNumber = value; }
        }
        public static int StepNumberValue
        {
            get { return StepNumber; }
            set { StepNumber = value; }
        }
        public static int StepKeepValue
        {
            get { return StepKeep; }
            set { StepKeep = value; }
        }
        public static int[,,] DefaultMatrixValue
        {
            get { return DefaultMatrix; }
            set { DefaultMatrix = value; }
        }
        public static string LeadInToKeepValue
        {
            get { return LeadInToKeep; }
            set { LeadInToKeep = value; }
        }
        public static string LeadOutToKeepValue
        {
            get { return LeadOutToKeep; }
            set { LeadOutToKeep = value; }
        }
        public static int LeadInNumberToKeepValue
        {
            get { return LeadInNumberToKeep; }
            set { LeadInNumberToKeep = value; }
        }
        public static int LeadOutNumberToKeepValue
        {
            get { return LeadOutNumberToKeep; }
            set { LeadOutNumberToKeep = value; }
        }


        public static int MaxNumberValue
        {
            get { return MaxNumber; }
            set { MaxNumber = value; }
        }
        public static bool IsCheckedValue
        {
            get { return IsChecked; }
            set { IsChecked = value; }
        }
        public static string StartPointValue
        {
            get { return StartPoint; }
            set { StartPoint = value; }
        }
        public static string StartPointToKeepValue
        {
            get { return StartPointToKeep; }
            set { StartPointToKeep = value; }
        }
        public static string StepOrderSelectedName
        {
            get { return StepOrderSelected; }
            set { StepOrderSelected = value; }
        }

        public static string ValueZeroZero
        {
            get { return ValueZeroZeroValue; }
            set { ValueZeroZeroValue = value; }
        }
        public static string ValueZeroLast
        {
            get { return ValueZeroLastValue; }
            set { ValueZeroLastValue = value; }
        }
        public static string ValueLastLast
        {
            get { return ValueLastLastValue; }
            set { ValueLastLastValue = value; }
        }
        public static string ValueZeroZeroLast
        {
            get { return ValueZeroZeroLastValue; }
            set { ValueZeroZeroLastValue = value; }
        }


        public static string ValueZeroZeroOneStack
        {
            get { return ValueZeroZeroOneStackValue; }
            set { ValueZeroZeroOneStackValue = value; }
        }
        public static string ValueLastLastPack
        {
            get { return ValueLastLastPackValue; }
            set { ValueLastLastPackValue = value; }
        }
        public static string ValueFirstSheetFirstStackLastEle
        {
            get { return ValueFirstSheetFirstStackLastEleValue; }
            set { ValueFirstSheetFirstStackLastEleValue = value; }
        }
        public static string ValueLastSheetFirstStackFirstElement
        {
            get { return ValueLastSheetFirstStackFirstElementValue; }
            set { ValueLastSheetFirstStackFirstElementValue = value; }
        }


        public static string ValueAlphaValue
        {
            get { return AlphaValue; }
            set { AlphaValue = value; }
        }
        public static string ValueAlphaZeroOneValue
                    {
            get { return AlphaZeroOneValue; }
    set { AlphaZeroOneValue = value; }
        }
        public static string ValueAlphaOneZeroValue
        {
            get { return AlphaOneZeroValue; }
            set { AlphaOneZeroValue = value; }
        }


        
        public static string ValueBetaValue
        {
            get { return BetaValue; }
            set { BetaValue = value; }
        }

        public static string ValueGammaValue
        {
            get { return GammaValue; }
            set { GammaValue = value; }
        }

        public static string ValueDeltaValue
        {
            get { return DeltaValue; }
            set { DeltaValue = value; }
        }
        public static string ValueEpsilonValue
        {
            get { return EpsilonValue; }
            set { EpsilonValue = value; }
        }
        public static string ValueZetaValue
        {
            get { return ZetaValue; }
            set { ZetaValue = value; }
        }
        public static string ValueEtaValue
        {
            get { return EtaValue; }
            set { EtaValue = value; }
        }
        public static string ValueThetaValue
        {
            get { return ThetaValue; }
            set { ThetaValue = value; }
        }
        public static string ValueIotaValue
        {
            get { return IotaValue; }
            set { IotaValue = value; }
        }
        public static string ValueKappaValue
        {
            get { return KappaValue; }
            set { KappaValue = value; }
        }
        public static string ValueLambdaValue
        {
            get { return LambdaValue; }
            set { LambdaValue = value; }
        }
        public static string ValueMuValue
        {
            get { return MuValue; }
            set { MuValue = value; }
        }
        public static string ValueNuValue
        {
            get { return NuValue; }
            set { NuValue = value; }
        }
        public static string ValueXiValue
        {
            get { return XiValue; }
            set { XiValue = value; }
        }
        public static string ValueOmicronValue
        {
            get { return OmicronValue; }
            set { OmicronValue = value; }
        }
        public static string ValuePiValue
        {
            get { return PiValue; }
            set { PiValue = value; }
        }
        public static string ValueRoValue
        {
            get { return RoValue; }
            set { RoValue = value; }
        }
        public static string ValueSigmaValue
        {
            get { return SigmaValue; }
            set { SigmaValue = value; }
        }
        public static string ValueSigmaZeroOneValue
        {
            get { return SigmaZeroOneValue; }
            set { SigmaZeroOneValue = value; }
        }

        public static string ValueSigmaZeroLastValue
        {
            get { return SigmaZeroLastValue; }
            set { SigmaZeroLastValue = value; }
        }
        
        public static string ValueOmegaValue
        {
            get { return OmegaValue; }
            set { OmegaValue = value; }
        }


        public static string ValueRolledAlphaValue
        {
            get { return RolledAlphaValue; }
            set { RolledAlphaValue = value; }
        }
        public static string ValueRolledBetaValue
        {
            get { return RolledBetaValue; }
            set { RolledBetaValue = value; }
        }
        public static string ValueRolledGammaValue
        {
            get { return RolledGammaValue; }
            set { RolledGammaValue = value; }
        }
        public static string ValueRolledDeltaValue
        {
            get { return RolledDeltaValue; }
            set { RolledDeltaValue = value; }
        }
        public static string ValueRolledEpsilonValue
        {
            get { return RolledEpsilonValue; }
            set { RolledEpsilonValue = value; }
        }
        public static string ValueRolledZetaValue
        {
            get { return RolledZetaValue; }
            set { RolledZetaValue = value; }
        }
        public static string ValueRolledEtaValue
        {
            get { return RolledEtaValue; }
            set { RolledEtaValue = value; }
        }
        public static string ValueRolledThetaValue
        {
            get { return RolledThetaValue; }
            set { RolledThetaValue = value; }
        }
        public static string ValueRolledKappaValue
        {
            get { return RolledKappaValue; }
            set { RolledKappaValue = value; }
        }

        public static string ValueRolledIotaValue
        {
            get { return RolledIotaValue; }
            set { RolledIotaValue = value; }
        }

        public static string ValueFirstNewSheetValue
        {
            get { return FirstNewSheetValue; }
            set { FirstNewSheetValue = value; }
        }
        
        public static string StepOrder
        {
            get { return StepOrderValue; }
            set { StepOrder = value; }
        }
        public static int Modulus
        {
            get { return ModulusValue; }
            set { ModulusValue = value; }
        }
        public static string ModulusTypeSelectedName
        {
            get { return ModulusTypeSelectedValue; }
            set { ModulusTypeSelectedValue = value; }
        }
        public static string ModulusPositionSelectedName
        {
            get { return ModulusTypePositionValue; }
            set { ModulusTypePositionValue = value; }
        }
        public static int RecordsOfPacksValue
        {
            get { return RecordsOfPacks; }
            set { RecordsOfPacks = value; }
        }
        public static string TextBetweenPacksToShowValue
        {
            get { return TextBetweenPacksToShow; }
            set { TextBetweenPacksToShow = value; }
        }
        public static int RecordsOfSheetsValue
        {
            get { return RecordsOfSheets; }
            set { RecordsOfSheets = value; }
        }
        public static string TextBetweenSheetsToShowValue
        {
            get { return TextBetweenSheetsToShow; }
            set { TextBetweenSheetsToShow = value; }
        }
        public static int EveryXSheetsValue
        {
            get { return EveryXSheets; }
            set { EveryXSheets = value; }
        }
        public static int MultiplyByColumnValue
        {
            get { return MultiplyByColumn; }
            set { MultiplyByColumn = value; }
        }
        public static int MultiplyBySheetValue
        {
            get { return MultiplyBySheet; }
            set { MultiplyBySheet = value; }
        }

        public static string TextBetweenPacksToKeepValue
        {
            get { return TextBetweenPacksToKeep; }
            set { TextBetweenPacksToKeep = value; }
        }
        public static int RecordsOfPacksToKeepValue
        {
            get { return RecordsOfPacksToKeep; }
            set { RecordsOfPacksToKeep = value; }
        }

        public static int EveryXSheetsToKeepToKeepValue
        {
            get { return EveryXSheetsToKeep; }
            set { EveryXSheetsToKeep = value; }
        }
        public static string TextBetweenSheetsToKeepValue
        {
            get { return TextBetweenSheetsToKeep; }
            set { TextBetweenSheetsToKeep = value; }
        }
        public static int RecordsOfSheetToKeepValue
        {
            get { return RecordsOfSheetToKeep; }
            set { RecordsOfSheetToKeep = value; }
        }
        public static string ConfigurationFileValue
        {
            get { return ConfigurationFile; }
            set { ConfigurationFile = value; }
        }
        public static bool AddZeroValue
        {
            get { return AddZero; }
            set { AddZero = value; }
        }
        public static bool CheckDigitValue
        {
            get { return CheckDigit; }
            set { CheckDigit = value; }
        }
    }
}
