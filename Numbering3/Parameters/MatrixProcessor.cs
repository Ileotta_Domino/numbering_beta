﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class MatrixProcessor : IMatrixProcessor
    {

        int X;
        int Y;
        int Z;
        int LengthOfArray;
        char[] ArrayTemp;

        //int[,] Matrix;

        public int GetRow()
        {
            return MatrixKeeper.RowValue;
        }
        public void SetRow(int input)
        {
            MatrixKeeper.RowValue = input;
        }
        public int GetColumn()
        {
            return MatrixKeeper.ColumnValue;
        }
        public void SetColumn(int input)
        {
            MatrixKeeper.ColumnValue = input;
        }
        public int GetSheet()
        {
            return MatrixKeeper.SheetValue;
        }
        public void SetSheet(int input)
        {
            MatrixKeeper.SheetValue = input;
        }
        public int GetStack()
        {
            return MatrixKeeper.StackValue;
        }
        public void SetStack(int input)
        {
            MatrixKeeper.StackValue = input;
        }
        public int GetPack()
        {
            return MatrixKeeper.PackValue;
        }
        public void SetPack(int input)
        {
            MatrixKeeper.PackValue = input;
        }
        public string GetAlignment()
        {
            return MatrixKeeper.AlignmentSelectedName;
        }
        public void SetAlignment(string input)
        {
            MatrixKeeper.AlignmentSelectedName = input;
        }
        public string GetOrder()
        {
            return MatrixKeeper.OrderSelectedName;
        }
        
        public void SetOrder(string input)
        {
            MatrixKeeper.OrderSelectedName = input;
        }
        public int GetArrayDigitLength()
        {
            return MatrixKeeper.ArrayDigitLengthValue;
        }
        public void SetArrayDigitLength(int input)
        {
            MatrixKeeper.ArrayDigitLengthValue = input;
        }
        public int GetArrayInLength()
        {
            return MatrixKeeper.ArrayInLengthValue;
        }
        public int GetArrayOutLength()
        {
            return MatrixKeeper.ArrayOutLengthValue;
        }
        public void SetArrayInLength(int input)
        {
            MatrixKeeper.ArrayInLengthValue = input;
        }
        public void SetArrayOutLength(int input)
        {
            MatrixKeeper.ArrayOutLengthValue = input;
        }
        public void CreateMatrix(int X, int Y)
        {
            // int[,,] Matrix3D = new int[X, Y, Z];
         
            MatrixKeeper.MatrixTestValue = new int[X, Y];
        }
        public void CreateMatrix(int X, int Y, int Z)
        {
            MatrixKeeper.MatrixValue = new int[X, Y, Z];
        }

        public void CreateRolledMatrix(int X, int Y, int Z)
        {
            MatrixKeeper.RolledMatrixValue = new int[X, Y, Z];
        }
        public void SetRolledMatrix(int input, int i, int j, int n)
        {
            MatrixKeeper.RolledMatrixValue[i, j, n] = input;
        }
        public int[,,] GetRolledMatrix()
        {
            return MatrixKeeper.RolledMatrixValue;
        }
        public void SetMatrix(int input, int i, int j, int n)
        {
            MatrixKeeper.MatrixValue[i, j, n] = input;
        }
        public int[,,] GetMatrix()
        {
            return MatrixKeeper.MatrixValue;
        }
        public int[,] GetMatrixTest()
        {
            return MatrixKeeper.MatrixTestValue;
        }

        public void SetMatrixTest(int input, int i, int j)
        {
            MatrixKeeper.MatrixTestValue[i, j] = input;
        }

        public string GetBitCheck()
        {
            return MatrixKeeper.BitCheckValue;
        }

        public void SetBitCheck(string input)
        {
            MatrixKeeper.BitCheckValue = input;
        }
        public char[] GetLeadInArray()
        {
            ArrayTemp = new char[MatrixKeeper.LeadInArrayValue.Length];
            Array.Copy(MatrixKeeper.LeadInArrayValue, ArrayTemp, MatrixKeeper.LeadInArrayValue.Length);
            return ArrayTemp;
        }
        public char[] GetLeadOutArray()
        {
            ArrayTemp = new char[MatrixKeeper.LeadOutArrayValue.Length];
            Array.Copy(MatrixKeeper.LeadOutArrayValue, ArrayTemp, MatrixKeeper.LeadOutArrayValue.Length);
            return ArrayTemp;
        }
        public void SetLeadInArray(char[] input)
        {
            Array.Copy(input, MatrixKeeper.LeadInArrayValue, input.Length); ;
        }
        public void SetLeadOutArray(char[] input)
        {
            Array.Copy(input, MatrixKeeper.LeadOutArrayValue, input.Length); ;
        }
        public void SetLeadInArrayLength(int input)
        {
            MatrixKeeper.ArrayInLengthValue = input;
        }
        public void SetLeadOutArrayLength(int input)
        {
            MatrixKeeper.ArrayOutLengthValue = input;
        }
        public int GetLeadInArrayLength()
        {
            return MatrixKeeper.ArrayInLengthValue;
        }
        public int GetLeadOutArrayLength()
        {
            return MatrixKeeper.ArrayOutLengthValue;
        }
        public bool GetLeadInFlag()
        {
            return MatrixKeeper.LeadInFlagValue;
        }
        public bool GetLeadOutFlag()
        {
            return MatrixKeeper.LeadInFlagValue;
        }
        public void SetLeadInFlag(bool input)
        {
            MatrixKeeper.LeadInFlagValue=input;
        }
        public void CreateLeadInArray()
        {
            MatrixKeeper.CreateLeadInArray();
        }
        public void CreateLeadOutArray()
        {
            MatrixKeeper.CreateLeadOutArray();
        }
        public void CreateListOfOutput()
        {
            MatrixKeeper.CreateJaggedList();
        }

        public void CreateSublist()
        {
            MatrixKeeper.CreateSubList();
        }

        public void SublistAdd(string input)
        {
            MatrixKeeper.AddtoSublist(input);
        }

        public void OutputListAdd()
        {
            MatrixKeeper.AddToOutputList();
        }

        public void SublistClear()
        {
            MatrixKeeper.ClearSublist();
        }
        public void OuputListClear()
        {
            MatrixKeeper.ClearOutputList();
        }
        public int GetNumberOfLeadIn()
        {
            return MatrixKeeper.LeadInNumberValue;
        }
        public int GetNumberOfLeadOut()
        {
            return MatrixKeeper.LeadOutNumberValue;
        }
        public void SetNumberOfLeadIn(int input)
        {
            MatrixKeeper.LeadInNumberValue = input;
        }
        public void SetNumberOfLeadOut(int input)
        {
            MatrixKeeper.LeadOutNumberValue = input;
        }

        public int GetRepetitionNumber()
        {
            return MatrixKeeper.RepetitionNumberValue;
        }

        public void SetRepetitionNumber(int input)
        {
            MatrixKeeper.RepetitionNumberValue = input;
        }

        public int GetStepValue()
        {
           return MatrixKeeper.StepNumberValue;
        }
        public void SetStepValue(int input)
        {
            MatrixKeeper.StepNumberValue = input;
        }
        public void SetDefaultMatrix(int[,,] input)
        {
            MatrixKeeper.DefaultMatrixValue = input;
        }
        public int[,,] GetDefaultMatrix()
        {
            return MatrixKeeper.DefaultMatrixValue;
        }
        public void SetLeadInToKeepValue(string input)
        {
            MatrixKeeper.LeadInToKeepValue = input;
        }
        public string GetLeadInToKeepValue()
        {
            return MatrixKeeper.LeadInToKeepValue;
        }
        public void SetLeadOutToKeepValue(string input)
        {
            MatrixKeeper.LeadOutToKeepValue  = input;
        }
        public string GetLeadOutToKeepValue()
        {
            return MatrixKeeper.LeadOutToKeepValue ;
        }

        public void SetLeadInNumberToKeepValue(int input)
        {
            MatrixKeeper.LeadInNumberToKeepValue = input;
        }
        public int GetLeadInNumberToKeepValue()
        {
            return MatrixKeeper.LeadInNumberToKeepValue;
        }
        public void SetLeadOutNumberToKeepValue(int input)
        {
            MatrixKeeper.LeadOutNumberToKeepValue = input;
        }
        public int GetLeadOutNumberToKeepValue()
        {
            return MatrixKeeper.LeadOutNumberToKeepValue;
        }
        public void SetStepToKeepValue(int input)
        {
            MatrixKeeper.StepKeepValue = input;
        }
        public int GetStepToKeepValue()
        {
            return MatrixKeeper.StepKeepValue;
        }
        public void SetMaxNumber(int input)
        {
            MatrixKeeper.MaxNumberValue = input;
        }
        public int GetMaxNumber()
        {
            return MatrixKeeper.MaxNumberValue;
        }
        public bool GetIsChecked()
        {
            return MatrixKeeper.IsCheckedValue;
        }

        public void SetIsChecked(bool input)
        {
            MatrixKeeper.IsCheckedValue = input;
        }
        public string GetStartPoint()
        {
            return MatrixKeeper.StartPointValue;
        }
        public void SetStartPoint(string input)
        {
            MatrixKeeper.StartPointValue = input;
        }
        public string GetStartPointToKeep()
        {
            return MatrixKeeper.StartPointToKeepValue;
        }
        public void SetStartPointToKeep(string input)
        {
            MatrixKeeper.StartPointToKeepValue = input;
        }
        public string GetStepOrder()
        {
            return MatrixKeeper.StepOrderSelectedName;
        }
        public void SetStepOrder(string input)
        {
            MatrixKeeper.StepOrderSelectedName = input;
        }
        public string GetValueZeroZero()
        {
            return MatrixKeeper.ValueZeroZero;
        }
        public void SetValueZeroZero(string input)
        {
            MatrixKeeper.ValueZeroZero = input;
        }
        public string GetValueZeroLast()
        {
            return MatrixKeeper.ValueZeroLast;
        }
        public void SetValueZeroLast(string input)
        {
            MatrixKeeper.ValueZeroLast = input;
        }
        public string GetValueLastLast()
        {
            return MatrixKeeper.ValueLastLast;
        }
        public void SetValueLastLast(string input)
        {
            MatrixKeeper.ValueLastLast = input;
        }
        public string GetValueZeroZeroLast()
        {
            return MatrixKeeper.ValueZeroZeroLast;
        }
        public void SetValueZeroZeroLast(string input)
        {
            MatrixKeeper.ValueZeroZeroLast = input;
        }
        public string GetValueZeroZeroOneStack()
        {
            return MatrixKeeper.ValueZeroZeroOneStack;
        }
        public void SetValueZeroZeroOneStack(string input)
        {
            MatrixKeeper.ValueZeroZeroOneStack = input;
        }
        public string GetValueFirstSheetFirstStackLastEle()
        {
            return MatrixKeeper.ValueFirstSheetFirstStackLastEle;
        }
        public void SetValueFirstSheetFirstStackLastEle(string input)
        {
            MatrixKeeper.ValueFirstSheetFirstStackLastEle = input;
        }
        public string GetValueLastLastPack()
        {
            return MatrixKeeper.ValueLastLastPack;
        }
        public void SetValueLastLastPack(string input)
        {
            MatrixKeeper.ValueLastLastPack  = input;
        }

        public string GetValueFirstSheetFirstStackFirstElement()
        {
            return MatrixKeeper.ValueLastSheetFirstStackFirstElement;
        }
        public void SetValueLastSheetFirstStackFirstElement(string input)
        {
            MatrixKeeper.ValueLastSheetFirstStackFirstElement = input;
        }

        public string GetValueAlphaValue()
        {
            return MatrixKeeper.ValueAlphaValue;
        }
        public void SetValueAlphaValue(string input)
        {
            MatrixKeeper.ValueAlphaValue = input;
        }

        public string GetValueAlphaZeroOneValue()
        {
            return MatrixKeeper.ValueAlphaZeroOneValue;
        }
        public void SetValueAlphaZeroOneValue(string input)
        {
            MatrixKeeper.ValueAlphaZeroOneValue = input;
        }
        public string GetValueAlphaOneZeroValue()
        {
            return MatrixKeeper.ValueAlphaOneZeroValue;
        }
        public void SetValueAlphaOneZeroValue(string input)
        {
            MatrixKeeper.ValueAlphaOneZeroValue = input;
        }               
        public string GetValueBetaValue()
        {
            return MatrixKeeper.ValueBetaValue;
        }
        public void SetValueBetaValue(string input)
        {
            MatrixKeeper.ValueBetaValue = input;
        }

        public string GetValueGammaValue()
        {
            return MatrixKeeper.ValueGammaValue;
        }
        public void SetValueGammaValue(string input)
        {
            MatrixKeeper.ValueGammaValue = input;
        }



        public void SetValueDeltaValue(string input)
        {
            MatrixKeeper.ValueDeltaValue = input;
        }
        public string GetValueDeltaValue()
        {
            return MatrixKeeper.ValueDeltaValue;
        }
        public void SetValueEpsilonValue(string input)
        {
            MatrixKeeper.ValueEpsilonValue = input;
        }
        public string GetValueEpsilonValue()
        {
            return MatrixKeeper.ValueEpsilonValue;
        }

        public void SetValueZetaValue(string input)
        {
            MatrixKeeper.ValueZetaValue = input;
        }
        public string GetValueZetaValue()
        {
            return MatrixKeeper.ValueZetaValue;
        }


        public void SetValueEtaValue(string input)
        {
            MatrixKeeper.ValueEtaValue = input;
        }
        public string GetValueEtaValue()
        {
            return MatrixKeeper.ValueEtaValue;
        }
        public void SetValueThetaValue(string input)
        {
            MatrixKeeper.ValueThetaValue = input;
        }
        public string GetValueThetaValue()
        {
            return MatrixKeeper.ValueThetaValue;
        }
        public void SetValueIotaValue(string input)
        {
            MatrixKeeper.ValueIotaValue = input;
        }
        public string GetValueIotaValue()
        {
            return MatrixKeeper.ValueIotaValue;
        }
        public void SetValueKappaValue(string input)
        {
            MatrixKeeper.ValueKappaValue = input;
        }
        public string GetValueKappaValue()
        {
            return MatrixKeeper.ValueKappaValue;
        }
        public void SetValueLambdaValue(string input)
        {
            MatrixKeeper.ValueLambdaValue = input;
        }
        public string GetValueLambdaValue()
        {
            return MatrixKeeper.ValueLambdaValue;
        }
        public void SetValueMuValue(string input)
        {
            MatrixKeeper.ValueMuValue = input;
        }
        public string GetValueMuValue()
        {
            return MatrixKeeper.ValueMuValue;
        }
        public void SetValueNuValue(string input)
        {
            MatrixKeeper.ValueNuValue = input;
        }
        public string GetValueNuValue()
        {
            return MatrixKeeper.ValueNuValue;
        }


        public void SetValueXiValue(string input)
        {
            MatrixKeeper.ValueXiValue = input;
        }
        public string GetValueXiValue()
        {
            return MatrixKeeper.ValueXiValue;
        }
        public void SetValueOmicronValue(string input)
        {
            MatrixKeeper.ValueOmicronValue = input;
        }
        public string GetValueOmicronValue()
        {
            return MatrixKeeper.ValueOmicronValue;
        }
        public void SetValuePiValue(string input)
        {
            MatrixKeeper.ValuePiValue = input;
        }
        public string GetValuePiValue()
        {
            return MatrixKeeper.ValuePiValue;
        }
        public void SetValueRoValue(string input)
        {
            MatrixKeeper.ValueRoValue = input;
        }
        public string GetValueRoValue()
        {
            return MatrixKeeper.ValueRoValue;
        }
        public void SetValueSigmaValue(string input)
        {
            MatrixKeeper.ValueSigmaValue = input;
        }
        public string GetValueSigmaValue()
        {
            return MatrixKeeper.ValueSigmaValue;
        }
        public void SetValueSigmaZeroOneValue(string input)
        {
            MatrixKeeper.ValueSigmaZeroOneValue = input;
        }
        public string GetValueSigmaZeroOneValue()
        {
            return MatrixKeeper.ValueSigmaZeroOneValue;
        }
        public void SetValueSigmaZeroLastValue(string input)
        {
            MatrixKeeper.ValueSigmaZeroLastValue = input;
        }
        public string GetValueSigmaZeroLastValue()
        {
            return MatrixKeeper.ValueSigmaZeroLastValue;
        }
        public void SetValueOmegaValue(string input)
        {
            MatrixKeeper.ValueOmegaValue = input;
        }
        public string GetValueOmegaValue()
        {
            return MatrixKeeper.ValueOmegaValue;
        }



        public void SetValueRolledAlphaValue(string input)
        {
            MatrixKeeper.ValueRolledAlphaValue = input;
        }
        public string GetValueRolledAlphaValue()
        {
            return MatrixKeeper.ValueRolledAlphaValue;
        }
        public void SetValueRolledBetaValue(string input)
        {
            MatrixKeeper.ValueRolledBetaValue = input;
        }
        public string GetValueRolledBetaValue()
        {
            return MatrixKeeper.ValueRolledBetaValue;
        }
        public void SetValueRolledGammaValue(string input)
        {
            MatrixKeeper.ValueRolledGammaValue = input;
        }
        public string GetValueRolledGammaValue()
        {
            return MatrixKeeper.ValueRolledGammaValue;
        }
        public void SetValueRolledDeltaValue(string input)
        {
            MatrixKeeper.ValueRolledDeltaValue = input;
        }
        public string GetValueRolledDeltaValue()
        {
            return MatrixKeeper.ValueRolledDeltaValue;
        }
        public void SetValueRolledEpsilonValue(string input)
        {
            MatrixKeeper.ValueRolledEpsilonValue = input;
        }
        public string GetValueRolledEpsilonValue()
        {
            return MatrixKeeper.ValueRolledEpsilonValue;
        }
        public void SetValueRolledZetaValue(string input)
        {
            MatrixKeeper.ValueRolledZetaValue = input;
        }
        public string GetValueRolledZetaValue()
        {
            return MatrixKeeper.ValueRolledZetaValue;
        }
        public void SetValueRolledEtaValue(string input)
        {
            MatrixKeeper.ValueRolledEtaValue = input;
        }
        public string GetValueRolledEtaValue()
        {
            return MatrixKeeper.ValueRolledEtaValue;
        }
        public void SetValueRolledThetaValue(string input)
        {
            MatrixKeeper.ValueRolledThetaValue = input;
        }
        public string GetValueRolledThetaValue()
        {
            return MatrixKeeper.ValueRolledThetaValue;
        }
        public void SetValueRolledKappaValue(string input)
        {
            MatrixKeeper.ValueRolledKappaValue = input;
        }
        public string GetValueRolledKappaValue()
        {
            return MatrixKeeper.ValueRolledEtaValue;
        }
        public void SetValueRolledIotaValue(string input)
        {
            MatrixKeeper.ValueRolledIotaValue = input;
        }
        public string GetValueRolledIotaValue()
        {
            return MatrixKeeper.ValueRolledIotaValue;
        }




        public void SetModulus(int input)
        {
            MatrixKeeper.Modulus = input;
        }
        public int GetModulus()
        {
            return MatrixKeeper.Modulus;
        }
        public string GetModulusType()
        {
            return MatrixKeeper.ModulusTypeSelectedName;
        }
        public void SetModulusType(string input)
        {
            MatrixKeeper.ModulusTypeSelectedName = input;
        }
        public string GetModulusPosition()
        {
            return MatrixKeeper.ModulusPositionSelectedName;
        }
        public void SetModulusPosition(string input)
        {
            MatrixKeeper.ModulusPositionSelectedName = input;
        }
        public int  GetRecordsOfPacksNumber()
        {
            return MatrixKeeper.RecordsOfPacksValue;
        }
        public void SetRecordsOfPacksNumber(int input)
        {
            MatrixKeeper.RecordsOfPacksValue = input;
        }
        public string GetTextBetweenPacks()
        {
            return MatrixKeeper.TextBetweenPacksToShowValue; 
        }
        public void SetTextBetweenPacks(string input)
        {
            MatrixKeeper.TextBetweenPacksToShowValue = input;
        }
        public string GetTextBetweenPacksToKeepValue()
        {
            return MatrixKeeper.TextBetweenPacksToKeepValue;
        }
        public void SetTextBetweenPacksToKeepValue(string input )
        {
             MatrixKeeper.TextBetweenPacksToKeepValue=input;
        }
        public int GetRecordsOfSheetsNumber()
        {
            return MatrixKeeper.RecordsOfSheetsValue;
        }
        public void SetRecordsOfSheetsNumber(int input)
        {
            MatrixKeeper.RecordsOfSheetsValue = input;
        }
        public string GetTextBetweenSheets()
        {
            return MatrixKeeper.TextBetweenSheetsToShowValue;
        }
        public void SetTextBetweenSheets(string input)
        {
            MatrixKeeper.TextBetweenSheetsToShowValue = input;
        }
        public int GetEveryXSheetsValue()
        {
            return MatrixKeeper.EveryXSheetsValue;
        }
        public void SetEveryXSheetsValue(int input)
        {
            MatrixKeeper.EveryXSheetsValue = input;
        }
        public int GetMultiplyByColumnValue()
        {
            return MatrixKeeper.MultiplyByColumnValue;
        }
        public void SetMultiplyByColumnValue(int input)
        {
            MatrixKeeper.MultiplyByColumnValue = input;
        }
        public int GetMultiplyBySheetValue()
        {
            return MatrixKeeper.MultiplyBySheetValue;
        }
        public void SetMultiplyBySheetValue(int input)
        {
            MatrixKeeper.MultiplyBySheetValue = input;
        }

        public int GetRecordsOfPacksToKeepValue()
        {
            return MatrixKeeper.RecordsOfPacksToKeepValue;
        }
        public void SetRecordsOfPacksToKeepValue(int input)
        {
            MatrixKeeper.RecordsOfPacksToKeepValue = input;
        }
        public int GetEveryXSheetsToKeepToKeepValue()
        {
            return MatrixKeeper.EveryXSheetsToKeepToKeepValue;
        }
        public void SetEveryXSheetsToKeepToKeepValue(int input)
        {
            MatrixKeeper.EveryXSheetsToKeepToKeepValue = input;
        }

        public string GetTextBetweenSheetToKeepValue()
        {
            return MatrixKeeper.TextBetweenSheetsToKeepValue;
        }
        public void SetTextBetweenSheetToKeepValue(string input)
        {
            MatrixKeeper.TextBetweenSheetsToKeepValue = input;
        }
        public int GetRecordOfSheetToKeepValue()
        {
            return MatrixKeeper.RecordsOfSheetToKeepValue;
        }
        public void SetRecordOfSheetToKeepValue(int input)
        {
            MatrixKeeper.RecordsOfSheetToKeepValue = input;
        }
        public string GetConfigurationFileValue()
        {
            return MatrixKeeper.ConfigurationFileValue;
        }
        public void SetConfigurationFileValue(string input)
        {
            MatrixKeeper.ConfigurationFileValue = input;
        }

        public bool GetAddZeroeValue()
        {
            return MatrixKeeper.AddZeroValue;
        }
        public void SetAddZeroeValue(bool input)
        {
            MatrixKeeper.AddZeroValue = input;
        }
        public bool GetCheckDigitValue()
        {
            return MatrixKeeper.CheckDigitValue;
        }
        public void SetCheckDigitValue(bool input)
        {
            MatrixKeeper.CheckDigitValue = input;
        }
    }
}
