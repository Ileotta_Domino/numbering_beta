﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
    public class SequenceProcessor: ISequenceProcessor
    {
        public void AssignAlphabet(string input)
        {
            SequenceKeeper.CreateAlphabet(input);
        }
        public string GetAlphabet()
        {
           return SequenceKeeper.AlphabetValue;
        }
        public string GetAlphabetC()
        {
            return SequenceKeeper.AlphabetCValue;
        }


        public void SetAlphabet(string input)
        {
            SequenceKeeper.AlphabetValue = input;
        }
        public void SetAlphabetC(string input)
        {
            SequenceKeeper.AlphabetCValue = input;
        }


        public string GetSequence()
        {
           return SequenceKeeper.SequenceValue;
        }

        public void SetSequence(string input)
        {
            SequenceKeeper.SequenceValue = input;
        }
        public int GetSequenceLength()
        {
            return SequenceKeeper.SequenceLengthValue;
        }
        public void SetSequenceLength(int input)
        {
            SequenceKeeper.SequenceLengthValue = input;
        }

        public bool GetIsSequenceValue()
        {
            return SequenceKeeper.IsSequenceValue;
        }

        public void SetIsSequenceValue(bool input)
        {
            SequenceKeeper.IsSequenceValue = input;
        }

        public void CreateSequenceArray(int length, string input)
        {
            SequenceKeeper.CreateSequenceArrayValue(length,input);
        }

        public char[] GetSequenceArray()
        {
            return SequenceKeeper.SequenceArrayValue;
        }
        public string GetSequenceToKeepValue()
        {
            return SequenceKeeper.SequenceToKeepValue;
        }
        public void SetSequenceToKeepValue(string input)
        {
            SequenceKeeper.SequenceToKeepValue = input;
        }

        public string GetAlphabetToKeepValue()
        {
            return SequenceKeeper.AlphabetToKeepValue;
        }
        public string GetAlphabetCToKeepValue()
        {
            return SequenceKeeper.AlphabetCToKeepValue;
        }
        public void SetAlphabetToKeepValue(string input)
        {
            SequenceKeeper.AlphabetToKeepValue =  input;
        }
        public void SetAlphabetCToKeepValue(string input)
        {
            SequenceKeeper.AlphabetCToKeepValue = input;
        }
    }
}
