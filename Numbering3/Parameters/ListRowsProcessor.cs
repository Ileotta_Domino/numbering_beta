﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Parameters
{
   public class ListRowsProcessor: IListRowsProcessor
    {
        public void ListOfRowsCreate()
        {
            ListOfRowsKeeper.CreateListOfRows();
        }

        public void ListOfRowsCopy(List<string> input)
        {
            ListOfRowsKeeper.ListOfRowsValue = input;
        }

        public void ListOfRowsAdd(string row)
        {
            ListOfRowsKeeper.AddtoListOfRow(row);
        }

        public void ListOfRowsAppend(List<string> input)
        {
            ListOfRowsKeeper.ListOfRowsValue.AddRange(input);
        }

        public List<string> ListOfRowsGet()
        {
           return ListOfRowsKeeper.ListOfRowsValue;
        }

        public void ListOfRowsReset()
        {
            ListOfRowsKeeper.ResetListOfRows();
        }


    }
}
