﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Numbering3.ViewModel
{
    class ViewModelLeadingZero: ViewModelBase, IClosableViewModel
    {
        IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        public StartPointValue _StartPointValue { get; set; }
        public StepOrder _StepOrderName { get; set; }

        

        public RelayCommand TakeLeadingZeroCommand { get; set; }
        public event EventHandler CloseWindowEvent;
        int LeadInLength = 0;
        int LeadOutLength = 0;
        char[] LeadOutArr;
        char[] LeadInArr;
        private string _TextBetweenPacksToShow;
        private string _LeadInToShow;
        private string _LeadOutToShow;
        private int _LeadInNumberToShow;
        private int _LeadOutNumberToShow;
        private int _ModulusToShow;
        private int _StepToShow;
        private int _RecordsOfPacks;
        private int _EveryXSheetsNumberToShow;
        private string _TextBetweenSheetsToShow;
        private int _RecordsOfSheets;
        private string NoneString = "None";
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<StepOrder> StepOrderSelection { get; set; }
        StepOrder _SelectedStepOrder;
        
     public int RecordsOfPacks
        {
            get
            {
                return _RecordsOfPacks;
            }
            set
            {
                if (_RecordsOfPacks != value)
                {
                    _RecordsOfPacks = value;
                    _matrixProcessor.SetRecordsOfPacksNumber(_RecordsOfPacks);
                    OnPropertyChanged("RecordsOfPacks");
                }
            }
        }

        public String TextBetweenPacksToShow
        {
            get { return _TextBetweenPacksToShow; }

            set
            {
                this._TextBetweenPacksToShow = value;
                _matrixProcessor.SetTextBetweenPacks(value);
                RaisePropertyChanged("TextBetweenPacksToShow");


            }
        }

        public String TextBetweenSheetsToShow
        {
            get { return _TextBetweenSheetsToShow; }

            set
            {
                this._TextBetweenSheetsToShow = value;
                _matrixProcessor.SetTextBetweenSheets(value);
                RaisePropertyChanged("TextBetweenSheetsToShow");


            }
        }

        public int RecordsOfSheets
        {
            get
            {
                return _RecordsOfSheets;
            }
            set
            {
                if (_RecordsOfSheets != value)
                {
                    _RecordsOfSheets = value;
                    _matrixProcessor.SetRecordsOfSheetsNumber(_RecordsOfSheets);
                    OnPropertyChanged("RecordsOfSheets");
                }
            }
        }
        public int EveryXSheetsNumberToShow
        {
            get
            {
                return _EveryXSheetsNumberToShow;
            }
            set
            {
                if (_EveryXSheetsNumberToShow != value)
                {
                    _EveryXSheetsNumberToShow = value;
                    _matrixProcessor.SetEveryXSheetsValue(_EveryXSheetsNumberToShow);
                    OnPropertyChanged("EveryXSheetsNumberToShow");
                }
            }
        }
        public StepOrder SelectedStepOrder
        {
            get
            {
                return _SelectedStepOrder;
            }
            set
            {
                if (_SelectedStepOrder != value)
                {
                    _SelectedStepOrder = value;
                    _matrixProcessor.SetStepOrder(_SelectedStepOrder.StepOrderName);
                    OnPropertyChanged("_SelectedStepOrder");
                }
            }
        }



        public String LeadInToShow
        {
            get { return _LeadInToShow; }

            set
            {
                this._LeadInToShow = value;
                _matrixProcessor.SetLeadInToKeepValue(value);
           //     MainWindow mw = (MainWindow)Application.Current.MainWindow;
             //   mw.LeadInText.Text = value;
                //  var win = new MainWindow { DataContext = new ViewModelMain() };
                //   win.LeadInText.Text = value;
                RaisePropertyChanged("LeadInUpdate");

                
            }
        }
        public String LeadOutToShow
        {
            get { return _LeadOutToShow; }

            set
            {
                this._LeadOutToShow = value;
                RaisePropertyChanged("CharNameFromTB");
            }
        }

        public int LeadInNumberToShow
        {
            get { return _LeadInNumberToShow; }

            set
            {
                this._LeadInNumberToShow = value;
                RaisePropertyChanged("CharNameFromTB");
            }
        }
        public int LeadOutNumberToShow
        {
            get { return _LeadOutNumberToShow; }

            set
            {
                this._LeadOutNumberToShow = value;
                RaisePropertyChanged("CharNameFromTB");
            }
        }

        public int StepToShow
        {
            get { return _StepToShow; }

            set
            {
                this._StepToShow = value;
                RaisePropertyChanged("CharNameFromTB");
            }
        }



        public ViewModelLeadingZero()
        {

            LeadInNumberToShow = _matrixProcessor.GetLeadInNumberToKeepValue();
            LeadOutNumberToShow = _matrixProcessor.GetLeadOutNumberToKeepValue();
            if( StepToShow == 0)
                _matrixProcessor.SetStepValue(1) ;
            StepToShow = 1;
            if (_matrixProcessor.GetStepToKeepValue() != 1) 
                StepToShow = _matrixProcessor.GetStepToKeepValue();

            if (_matrixProcessor.GetLeadInToKeepValue() != null)
            { LeadInToShow = _matrixProcessor.GetLeadInToKeepValue(); }
            else { _matrixProcessor.SetLeadOutArray(NoneString.ToCharArray());
                LeadInToShow = NoneString; }
            

            if (_matrixProcessor.GetLeadInToKeepValue() != null)
            { LeadOutToShow = _matrixProcessor.GetLeadOutToKeepValue(); }
            else { _matrixProcessor.SetLeadOutArray(NoneString.ToCharArray());
                LeadOutToShow = NoneString; }


            SelectedStepOrder = _StepOrderName;
            StepOrderSelection = new ObservableCollection<StepOrder>()
            {
                new StepOrder(_matrixProcessor){StepOrderName="+" },
                new StepOrder(_matrixProcessor){StepOrderName="-" }
            };
                 SelectedStepOrder = StepOrderSelection.First();

            if (_matrixProcessor.GetStepToKeepValue() != 1)
                {
                StepToShow = _matrixProcessor.GetStepToKeepValue();
            }


            if (_matrixProcessor.GetTextBetweenPacksToKeepValue() != "None")
            {
                TextBetweenPacksToShow = _matrixProcessor.GetTextBetweenPacksToKeepValue();
            }

            if (_matrixProcessor.GetRecordsOfPacksToKeepValue() != 1)
            {
                RecordsOfPacks = _matrixProcessor.GetRecordsOfPacksToKeepValue();
            }

            if (_matrixProcessor.GetEveryXSheetsToKeepToKeepValue() != 1)
            {
                EveryXSheetsNumberToShow = _matrixProcessor.GetEveryXSheetsToKeepToKeepValue();
            }

            if (_matrixProcessor.GetTextBetweenSheetToKeepValue() != "None")
            {
                TextBetweenSheetsToShow = _matrixProcessor.GetTextBetweenSheetToKeepValue();
            }
            if (_matrixProcessor.GetRecordOfSheetToKeepValue() != 0)
            {
                RecordsOfSheets = _matrixProcessor.GetRecordOfSheetToKeepValue();
            }
            //TextBetweenPacksToShow

            TakeLeadingZeroCommand = new Numbering3.Helper.RelayCommand(TakeLeadingZero, canexecute);


        }

        private bool canexecute(object parameter)
        {
            return true;
        }

        private void TakeLeadingZero(object parameter)
        {
            if (parameter == null) return;
            var values = (object[])parameter;
            if ((string)values[1] != null)
            {
                string leadIn = (string)values[1];
                LeadInArr = leadIn.ToCharArray();   
                LeadInLength = LeadInArr.Length;
                _matrixProcessor.SetLeadInArrayLength(LeadInLength);
                _matrixProcessor.CreateLeadInArray();
                _matrixProcessor.SetLeadInArray(LeadInArr);
                _matrixProcessor.SetLeadInToKeepValue(leadIn);
            }

         //   int num4 = Convert.ToInt32((string)values[0]);
            _matrixProcessor.SetNumberOfLeadIn(Convert.ToInt32((string)values[0]));
            _matrixProcessor.SetLeadInNumberToKeepValue(Convert.ToInt32((string)values[0]));

            if ((string)values[3] != null)
            {
                string leadOut = (string)values[3];
                LeadOutArr = leadOut.ToCharArray();
                LeadOutLength = leadOut.Length;
                _matrixProcessor.SetLeadOutArrayLength(LeadOutLength);
                _matrixProcessor.CreateLeadOutArray();
                _matrixProcessor.SetLeadOutArray(LeadOutArr);
                _matrixProcessor.SetLeadOutToKeepValue(leadOut);
            }
            _matrixProcessor.SetNumberOfLeadOut(Convert.ToInt32((string)values[2]));
            _matrixProcessor.SetLeadOutNumberToKeepValue(Convert.ToInt32((string)values[2]));

            if ((string)values[4] != null)
            {
                _matrixProcessor.SetStepValue(Convert.ToInt32((string)values[4]));
            }
            if ((string)values[5] != null)
            {
                _matrixProcessor.SetTextBetweenPacksToKeepValue((string)values[5]);
            }
            if ((string)values[6] != null)
            {
                _matrixProcessor.SetRecordsOfPacksToKeepValue(Convert.ToInt32((string)values[6]));
            }
            if ((string)values[7] != null)
            {
                _matrixProcessor.SetEveryXSheetsToKeepToKeepValue(Convert.ToInt32((string)values[7]));
            }
            if ((string)values[8] != null)
            {
                _matrixProcessor.SetTextBetweenSheetToKeepValue((string)values[8]);
            }
            if ((string)values[9] != null)
            {
                _matrixProcessor.SetRecordOfSheetToKeepValue(Convert.ToInt32((string)values[9]));
            }
            //_matrixProcessor.SetStepToKeepValue(1);

            //if ((string)values[5] != null)
            //{
            //    _matrixProcessor.SetModulus(Convert.ToInt32((string)values[5]));
            //}
            //_matrixProcessor.SetArrayLength((num4 + _matrixProcessor.GetArrayLength()));
            //     if (CloseWindowEvent == null)
            CloseWindowEvent(this, null);
        }
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
