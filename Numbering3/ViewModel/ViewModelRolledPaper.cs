﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model;
using Numbering3.Model.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using Numbering3.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.ViewModel
{
    class ViewModelRolledPaper : ViewModelBase, IClosableViewModel, INotifyPropertyChanged
    {
        public event EventHandler CloseWindowEvent;
        public event PropertyChangedEventHandler PropertyChanged;
        Order _SelectedRolledOrder;
        IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        ISwitcherContext _switcherContext = new SwitcherContext();
        IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        private bool _showResultsButtonEnabled;
        public ObservableCollection<Alignment> AlignmentToShow { get; set; }
        public ColumnValue _ColumnValeToShow { get; set; }
        public SheetValue _SheetValeToShow { get; set; }
        public RowValue _RowValueToShow { get; set; }
        public SequenceValue _SequenceValueToShow { get; set; }
        public StackValue _StackValueToShow { get; set; }
        public PackValue _PackValueToShow { get; set; }
       public StartPointValue _StartPointValue { get; set; }
        //    public MultiplyByColumn _MultiplyByColumnToShow { get; set; }
        //    public MultiplyBySheet _MultiplyBySheetToShow { get; set; }

        private int _PackValue;
        private int _StackValue;
        string _LeadInToShow;

        Alignment _SelectedAlignment;
        string _ValueRolledAlpha;
        string _ValueRolledBeta;
        string _ValueRolledGamma;
        string _ValueRolledDelta;
        string _ValueRolledEpsilon;
        string _ValueRolledZeta;
        string _ValueRolledEta;
        string _ValueRolledTheta;
        string _ValueRolledIota;
        string _ValueRolledKappa;
     //   string _StartPointValue;
        string _StopPointValue;
        string _CheckDigitValue;
        string _SequenceValue;




        //      IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        //       INumberingProcess _numberingProcess = new NumberingProcess();
        //    ISwitcherContext _switcherContext = new SwitcherContext();

        public Alignment SelectedAlignment
        {
            get
            {
                return _SelectedAlignment;
            }
            set
            {
                if (_SelectedAlignment != value)
                {
                    _SelectedAlignment = value;
                    _matrixProcessor.SetAlignment(_SelectedAlignment.AlignmentName.ToString());
                    OnPropertyChanged("SelectedAlignment");
                }
            }
        }

        public bool showResultsButtonEnabled
        {
            get
            {
                return _showResultsButtonEnabled;
            }
            set
            {
                _showResultsButtonEnabled = value;
                OnPropertyChanged("showResultsButtonEnabled");
            }
        }
        public string ValueRolledAlpha
        {
            get
            {
                return _ValueRolledAlpha;
            }
            set
            {
                if (_ValueRolledAlpha != value)
                {
                    _ValueRolledAlpha = value;
                    OnPropertyChanged("RolledAlphaValue");
                }
            }
        }

        public string ValueRolledBeta
        {
            get
            {
                return _ValueRolledBeta;
            }
            set
            {
                if (_ValueRolledBeta != value)
                {
                    _ValueRolledBeta = value;
                    OnPropertyChanged("ValueRolledBeta");
                }
            }
        }
        public string ValueRolledGamma
        {
            get
            {
                return _ValueRolledGamma;
            }
            set
            {
                if (_ValueRolledGamma != value)
                {
                    _ValueRolledGamma = value;
                    OnPropertyChanged("ValueRolledGamma");
                }
            }
        }
        public string ValueRolledDelta
        {
            get
            {
                return _ValueRolledDelta;
            }
            set
            {
                if (_ValueRolledDelta != value)
                {
                    _ValueRolledDelta = value;
                    OnPropertyChanged("ValueRolledDelta");
                }
            }
        }
        public string ValueRolledEpsilon
        {
            get
            {
                return _ValueRolledEpsilon;
            }
            set
            {
                if (_ValueRolledEpsilon != value)
                {
                    _ValueRolledEpsilon = value;
                    OnPropertyChanged("ValueRolledEpsilon");
                }
            }
        }
        public string ValueRolledZeta
        {
            get
            {
                return _ValueRolledZeta;
            }
            set
            {
                if (_ValueRolledZeta != value)
                {
                    _ValueRolledZeta = value;
                    OnPropertyChanged("ValueRolledZeta");
                }
            }
        }
        public string ValueRolledEta
        {
            get
            {
                return _ValueRolledEta;
            }
            set
            {
                if (_ValueRolledEta != value)
                {
                    _ValueRolledEta = value;
                    OnPropertyChanged("ValueRolledEta");
                }
            }
        }
        public string ValueRolledTheta
        {
            get
            {
                return _ValueRolledTheta;
            }
            set
            {
                if (_ValueRolledTheta != value)
                {
                    _ValueRolledTheta = value;
                    OnPropertyChanged("ValueRolledTheta");
                }
            }
        }
        public string ValueRolledIota
        {
            get
            {
                return _ValueRolledIota;
            }
            set
            {
                if (_ValueRolledIota != value)
                {
                    _ValueRolledIota = value;
                    OnPropertyChanged("ValueRolledIota");
                }
            }
        }
        public string ValueRolledKappa
        {
            get
            {
                return _ValueRolledKappa;
            }
            set
            {
                if (_ValueRolledKappa != value)
                {
                    _ValueRolledKappa = value;
                    OnPropertyChanged("ValueRolledKappa");
                }
            }
        }

        public Order SelectedRolledOrder
        {
            get
            {
                return _SelectedRolledOrder;
            }
            set
            {
                if (_SelectedRolledOrder != value)
                {
                    _SelectedRolledOrder = value;
                    _matrixProcessor.SetOrder(_SelectedRolledOrder.OrderName.ToString());
                    OnPropertyChanged("SelectedOrder");
                }
            }
        }
        public int PackValue
        {
            get
            {
                return _PackValue;
            }
            set
            {
                if (_PackValue != value)
                {
                    _PackValue = value;
                    _matrixProcessor.SetPack(_PackValue);
                    OnPropertyChanged("PackValue");
                }
            }
        }

        public int StackValue
        {
            get
            {
                return _StackValue;
            }
            set
            {
                if (_StackValue != value)
                {
                    _StackValue = value;
                    _matrixProcessor.SetStack(_StackValue);
                    OnPropertyChanged("StackValue");
                }
            }
        }
        //public string StartPointValue
        //{
        //    get
        //    {
        //        return _StartPointValue;
        //    }
        //    set
        //    {
        //        if (_StartPointValue != value)
        //        {
        //            _StartPointValue = value;
        //            OnPropertyChanged("StartPointValue");
        //        }
        //    }
        //}
        public string StopPointValue
        {
            get
            {
                return _StopPointValue;
            }
            set
            {
                if (_StopPointValue != value)
                {
                    _StopPointValue = value;
                    OnPropertyChanged("StopPointValue");
                }
            }
        }
        //public string SequenceValue
        //{
        //    get
        //    {
        //        return _SequenceValue;
        //    }
        //    set
        //    {
        //        if (_SequenceValue != value)
        //        {
        //            _SequenceValue = value;
        //            OnPropertyChanged("SequenceValue");
        //        }
        //    }
        //}
        public string CheckDigitValue
        {
            get
            {
                return _CheckDigitValue;
            }
            set
            {
                if (_CheckDigitValue != value)
                {
                    _CheckDigitValue = value;
                    OnPropertyChanged("CheckDigitValue");
                }
            }
        }
        public ObservableCollection<Order> AvailableOrderings { get; set; }

        public RelayCommand GoHomeCommand { get; set; }
        public RelayCommand LeadingZeroCommand { get; set; }
        public RelayCommand CounterSettingCommand { get; set; }
        public RelayCommand GoShowResultPageCommand { get; set; }
        public RelayCommand GoOpenConfigurationPageCommand { get; set; }
        public RelayCommand GoToModulePageCommand { get; set; }
        public RelayCommand StartRolledCalculationCommand { get; set; }
        private RelayCommand CloseCommand { get; set; }

        public ViewModelRolledPaper()
        {

            _ColumnValeToShow = new ColumnValue(_matrixProcessor);
            _SheetValeToShow = new SheetValue(_matrixProcessor);
            _RowValueToShow = new RowValue(_matrixProcessor);
            _StackValueToShow = new StackValue(_matrixProcessor);
            _PackValueToShow = new PackValue(_matrixProcessor);
            _SequenceValueToShow = new SequenceValue(_sequenceProcessor);
            _StartPointValue = new StartPointValue(_matrixProcessor);

            AvailableOrderings = new ObservableCollection<Order>()
            {
            new Order(_matrixProcessor) { OrderName = "Row,Columns" },
            new Order(_matrixProcessor) { OrderName = "Columns,Row" }
            };

            AlignmentToShow = new ObservableCollection<Alignment>
            {
              new Alignment(_matrixProcessor) { AlignmentName = "Horizontal" },
              new Alignment(_matrixProcessor) { AlignmentName = "Vertical" }
            };

            SelectedAlignment = AlignmentToShow.First();
            SelectedRolledOrder = AvailableOrderings.First();

            _SequenceValueToShow.SequenceValuetoShow = _sequenceProcessor.GetSequenceToKeepValue();
            StackValue = _matrixProcessor.GetStack();
            PackValue = _matrixProcessor.GetPack();
         //   StartPointValue = _matrixProcessor.GetStartPoint();
            StopPointValue = _matrixProcessor.GetMaxNumber().ToString();
        //    SequenceValue = _sequenceProcessor.GetSequence();
            _StartPointValue.StartPointValueToKeep = _matrixProcessor.GetStartPoint();
       //     CheckDigitValue = _matrixProcessor.GetArrayDigitLength();
            //if (_matrixProcessor.GetOrder() != "Columns,Row")
            //{ _matrixProcessor.SetOrder("Row,Columns"); }
            //StartRolledAlgo();

            //ValueRolledAlpha = _matrixProcessor.GetValueRolledAlphaValue();
            //ValueRolledBeta = _matrixProcessor.GetValueRolledBetaValue();
            //ValueRolledGamma = _matrixProcessor.GetValueRolledGammaValue();
            //ValueRolledDelta = _matrixProcessor.GetValueRolledDeltaValue();
            //ValueRolledEpsilon = _matrixProcessor.GetValueRolledEpsilonValue();
            //ValueRolledZeta = _matrixProcessor.GetValueRolledZetaValue();
            //ValueRolledEta = _matrixProcessor.GetValueRolledEtaValue();

     
            ///Default values
            if (_RowValueToShow.RowValueToKeep != 1 && _ColumnValeToShow.ColumnValueToKeep != 1 && _SheetValeToShow.SheetValueToKeep != 1 && _StackValueToShow.StackValueToKeep != 1 && _PackValueToShow.PackValueToKeep != 1)
            {

                _RowValueToShow.RowValuetoShow = _matrixProcessor.GetRow().ToString();
                _RowValueToShow.LastRowColumn = _matrixProcessor.GetSheet().ToString();
                _ColumnValeToShow.ColumnValuetoShow = _matrixProcessor.GetColumn().ToString();
                _SheetValeToShow.SheetValuetoShow = _matrixProcessor.GetSheet().ToString();

                _StackValueToShow.StackValuetoShow = _StackValueToShow.StackValueToKeep.ToString();
                _PackValueToShow.PackValuetoShow = _PackValueToShow.PackValueToKeep.ToString();

                ValueRolledAlpha = _matrixProcessor.GetValueRolledAlphaValue();
                ValueRolledBeta = _matrixProcessor.GetValueRolledBetaValue();
                ValueRolledGamma = _matrixProcessor.GetValueRolledGammaValue();
                ValueRolledDelta = _matrixProcessor.GetValueRolledDeltaValue();
                ValueRolledEpsilon = _matrixProcessor.GetValueRolledEpsilonValue();
                ValueRolledZeta = _matrixProcessor.GetValueRolledZetaValue();
                ValueRolledEta = _matrixProcessor.GetValueRolledEtaValue();
               // StartPointValue = _matrixProcessor.GetStartPoint();
                StopPointValue = _matrixProcessor.GetMaxNumber().ToString();

            }
            else
            {
                _RowValueToShow.RowValuetoShow = _matrixProcessor.GetRow().ToString();
                _RowValueToShow.LastRowColumn = _matrixProcessor.GetSheet().ToString();
                _ColumnValeToShow.ColumnValuetoShow = _matrixProcessor.GetColumn().ToString();
                _SheetValeToShow.SheetValuetoShow = _matrixProcessor.GetSheet().ToString();

                _StackValueToShow.StackValuetoShow = _StackValueToShow.StackValueToKeep.ToString();
                _PackValueToShow.PackValuetoShow = _PackValueToShow.PackValueToKeep.ToString();

                ValueRolledAlpha = _matrixProcessor.GetValueRolledAlphaValue();
                ValueRolledBeta = _matrixProcessor.GetValueRolledBetaValue();
                ValueRolledGamma = _matrixProcessor.GetValueRolledGammaValue();
                ValueRolledDelta = _matrixProcessor.GetValueRolledDeltaValue();
                ValueRolledEpsilon = _matrixProcessor.GetValueRolledEpsilonValue();
                ValueRolledZeta = _matrixProcessor.GetValueRolledZetaValue();
                ValueRolledEta = _matrixProcessor.GetValueRolledEtaValue();
             //   StartPointValue = _matrixProcessor.GetStartPoint();
                StopPointValue = _matrixProcessor.GetMaxNumber().ToString();
            }


            //if (_MultiplyByColumnToShow.MultiplyByColumnValueToKeep != 1)
            //{
            //    _MultiplyByColumnToShow.MultiplyByColumnValueToShow = _MultiplyByColumnToShow.MultiplyByColumnValueToKeep;
            //}
            //else
            //{
            //    _MultiplyByColumnToShow.MultiplyByColumnValueToShow = _matrixProcessor.GetMultiplyByColumnValue();
            //}

            //if (_MultiplyBySheetToShow.MultiplyBySheetValueToKeep != 1)
            //{
            //    _MultiplyBySheetToShow.MultiplyBySheetValueToShow = _MultiplyBySheetToShow.MultiplyBySheetValueToKeep;
            //}
            //else
            //{
            //    _MultiplyBySheetToShow.MultiplyBySheetValueToShow = _matrixProcessor.GetMultiplyBySheetValue();
            //}


            ///Commands
            ///
            StartRolledCalculationCommand = new Numbering3.Helper.RelayCommand(StartRolledCalculation, canexecute);
            GoHomeCommand = new Numbering3.Helper.RelayCommand(GoToMainPage, canexecute);
            LeadingZeroCommand = new Numbering3.Helper.RelayCommand(LeadingZero, canexecute);
            CounterSettingCommand = new Numbering3.Helper.RelayCommand(CounterSetting, canexecute);
            GoShowResultPageCommand = new Numbering3.Helper.RelayCommand(GoToShowResultPagePage, canexecute);
            GoOpenConfigurationPageCommand = new Numbering3.Helper.RelayCommand(GoToConfigurationPage, canexecute);
            GoToModulePageCommand = new Numbering3.Helper.RelayCommand(GoToModulePage, canexecute);
        }

        void AddUser(object parameter)
        {
            if (parameter == null) return;
            AvailableOrderings.Add(new Order(_matrixProcessor) { OrderName = parameter.ToString() });

        }

        private bool canexecute(object parameter)
        {
            //if (Number1 != null || Number2 != null || Number3 != null)
            //{
            return true;
            //}
            //else { return false; }
        }


        private void StartRolledCalculation(object parameter)
        {
          //  if (parameter == null) return;
        //  var values = (object[])parameter;
            int num4 = 0;
            int Maxvalue = 0;
            int Max = 0;

            int num1 = _RowValueToShow.RowValueToKeep;
            int num2 = 1; // _ColumnValeToShow.ColumnValueToKeep;
            int num3 = _SheetValeToShow.SheetValueToKeep;
            int num5 = _StackValueToShow.StackValueToKeep;
           // bool check1 = (bool)values[5];
        //    int num6 = 1;
       //     int num7 = 1;
            if (_sequenceProcessor.GetSequenceLength() > 0)
            {
                num4 = _sequenceProcessor.GetSequenceLength();
            }
            else { num4 = 0; }
            _matrixProcessor.SetRow(num1);

         //   _matrixProcessor.SetColumn(num2);
            _matrixProcessor.SetSheet(num3);
             Max = (num1 * _matrixProcessor.GetColumn() * num3* num5* PackValue) -1+ Int32.Parse( _matrixProcessor.GetStartPoint());
            _matrixProcessor.SetMaxNumber(Max);
            _matrixProcessor.SetPack(PackValue);
            _matrixProcessor.SetStack(StackValue);

            Maxvalue = ((_matrixProcessor.GetPack() * _matrixProcessor.GetStack() * Max + _matrixProcessor.GetPack()) + _matrixProcessor.GetStack() * Max + _matrixProcessor.GetStack() + Max).ToString().Length + 1;
            _matrixProcessor.SetArrayDigitLength(Maxvalue);
            _matrixProcessor.SetArrayDigitLength(Maxvalue);
            _matrixProcessor.SetRepetitionNumber(num5);
            _StartPointValue.StartPointValueToKeep = _matrixProcessor.GetStartPoint();
            _SequenceValueToShow.SequenceValueToKeep = _sequenceProcessor.GetSequence();
            //    _matrixProcessor.SetMultiplyByColumnValue(num6);
            //     _matrixProcessor.SetMultiplyBySheetValue(num7);
            StartRolledAlgo();

        }

        private void StartRolledAlgo()
            {
            int[,,] MatrixtoShow;
            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            int ro = _matrixProcessor.GetRow();
            int co = _matrixProcessor.GetColumn();
            int sh = _matrixProcessor.GetSheet();
            using (StreamWriter twBis = File.CreateText(@".\NumberingTestBis.txt"))


           _matrixProcessor.CreateMatrix(_matrixProcessor.GetRow(), _matrixProcessor.GetColumn(), _matrixProcessor.GetSheet());
            string chosenorder = _matrixProcessor.GetOrder();
            _switcherContext.DoAlgorithm(chosenorder);
            string ValueStringtoAdd = "";
            char[] baseCharArray;
            int indexbase;

            MatrixtoShow = _matrixProcessor.GetMatrix();
            baseCharArray = _alphabetConverterHelper.makeBaseArray();
            char[] tempcharArray = new char[baseCharArray.Length];
            //    using (StreamWriter twBis = File.CreateText(pathBis))
            //    ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(MatrixtoShow[0, 0, 0], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }
                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }

            _RowValueToShow.LastRowColumn = _matrixProcessor.GetValueLastLast();
            _RowValueToShow.RowValuetoShow = ValueStringtoAdd;


            ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(MatrixtoShow[0, MatrixtoShow.GetUpperBound(1), 0], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }
                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }
            _ColumnValeToShow.ColumnValuetoShow = ValueStringtoAdd;

            ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(MatrixtoShow[0, 0, MatrixtoShow.GetUpperBound(2)], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }

                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }
            _SheetValeToShow.SheetValuetoShow = ValueStringtoAdd;

            ValueRolledAlpha = _matrixProcessor.GetValueRolledAlphaValue();
            ValueRolledBeta = _matrixProcessor.GetValueRolledBetaValue();
            ValueRolledGamma = _matrixProcessor.GetValueRolledGammaValue();
            ValueRolledDelta = _matrixProcessor.GetValueRolledDeltaValue();
            ValueRolledEpsilon = _matrixProcessor.GetValueRolledEpsilonValue();
            ValueRolledZeta = _matrixProcessor.GetValueRolledZetaValue();
            ValueRolledEta = _matrixProcessor.GetValueRolledEtaValue();
            ValueRolledTheta = _matrixProcessor.GetValueRolledThetaValue();
            _SequenceValueToShow.SequenceValueToKeep = _sequenceProcessor.GetSequence();
        }

        private void GoToMainPage(object parameter)
        {
            var win = new MainWindow { DataContext = new ViewModelMain() };
            win.Show();
            CloseWindowEvent(this, null);
            CloseWindow();
        }

        void LeadingZero(object parameter)
        {
            var win = new LeadingZero { DataContext = new ViewModelLeadingZero() };
            win.Show();
            //      CloseWindow();
        }

        void CounterSetting(object parameter)
        {
            var win = new CounterSettings { DataContext = new ViewModelCounterSettings() };
            win.Show();
        }


        void GoToModulePage(object parameter)
        {
            var win = new Module { DataContext = new ViewModelModulePage() };
            win.Show();

        }
        void GoToConfigurationPage(object parameter)
        {
            var win = new ConfigurationPage { DataContext = new ViewModelConfigurationPage() };
            win.Show();
            CloseWindowEvent(this, null);
        }
        void GoToShowResultPagePage(object parameter)
        {
            var win = new ShowResult { DataContext = new ViewModelShowResult() };
            CloseWindow();
            win.Show();
            CloseWindowEvent(this, null);
        }
        public void CloseApplication()
        {
            Application.Current.MainWindow.Close();
        }
        public string fromCharArrayToString(char[] input)
        {
            string result = string.Join("", input);
            return result;
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
