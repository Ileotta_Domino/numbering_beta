﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.ViewModel
{
     class ViewModelShowResult : ViewModelBase, IClosableViewModel
    {
        public event EventHandler CloseWindowEvent;
        public RelayCommand GoHomeCommand { get; set; }

        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        private readonly IListRowsProcessor _listRowsProcessor = new ListRowsProcessor();

        //private String[] _columnHeaders;
        //public String[] ColumnHeaders
        //{
        //    get { return _columnHeaders; }
        //    set { _columnHeaders = value; }
        //}

        //private String[] _rowHeaders;
        //public String[] RowHeaders
        //{
        //    get { return _rowHeaders; }
        //    set { _rowHeaders = value; }
        //}

        //private string[,] _data2D;
        //public string[,] Data2D
        //{
        //    get { return _data2D; }
        //    set { _data2D = value; }
        //}
        // private ObservableCollection<RowType> matrix = new ObservableCollection<RowType>();
        //public ObservableCollection<RowType>Mymatrix
        //{
        //    get { return matrix; }
        //    set { matrix = value; }
        //}
        //  private ObservableCollection<List<string>> matrix = new ObservableCollection<List<string>>();
        private ObservableCollection<string> matrix = new ObservableCollection<string>();
        public ObservableCollection<string> Mymatrix
        {
            get { return matrix; }
            set { matrix = value; }
        }



        String[] columnHeaders = { "Prefix", "Separator", "Value" };
        String[] rowHeaders = { "1", "2", "3" };
        string[,] data2D = { { "true"," true", "false" }, { "true", "true", "false" }, { "true", "true", "false" } };
        string testrow1 = "0000000000  12345  wert";
        int[,,] Tempmatrix;


        public ViewModelShowResult()
        {
            Tempmatrix = _matrixProcessor.GetMatrix();
            int X = _matrixProcessor.GetRow();
            int Y = _matrixProcessor.GetColumn();
            int Z = _matrixProcessor.GetSheet();
            List<string> RowsList = new List<string>();

            RowsList = _listRowsProcessor.ListOfRowsGet();
   
            for (int i=0; i< RowsList.Count; i++)
                matrix.Add(RowsList[i]);



            //for (int n = 0; n <= Tempmatrix.GetUpperBound(2); n++)
            //{
            //    for (int j = 0; j <= Tempmatrix.GetUpperBound(1); j++)
            //    {
            //        for (int i = 0; i <= Tempmatrix.GetUpperBound(0); j++)
            //        {

            //        }
            //    }

            //    matrix.Add(testrow1);
            //    matrix.Add(testrow1);
            //    matrix.Add(testrow1);
                //ColumnHeaders = columnHeaders;
                //RowHeaders = rowHeaders;
                //Data2D = data2D;
                GoHomeCommand = new Numbering3.Helper.RelayCommand(GoToMainPage, canexecute);
            
        }

        private bool canexecute(object parameter)
        {
            //if (Number1 != null || Number2 != null || Number3 != null)
            //{
            return true;
            //}
            //else { return false; }
        }

        private void GoToMainPage(object parameter)
        {
            var win = new MainWindow { DataContext = new ViewModelMain() };
            win.Show();
            CloseWindowEvent(this, null);
            CloseWindow();
        }
    }
}
