﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.ViewModel
{
    class ViewModelModulePage: ViewModelBase, IClosableViewModel
    {
        IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        IWeightingProcessor _weightingProcessor = new WeightingProcessor();

        public ModulusType _ModulusTypeName { get; set; }
        public ModulusPosition _ModulusPosition { get; set; }
        public RelayCommand TakeModulusCommand { get; set; }

        private int _ModulusToShow;
        private int _Weight0ToShow;
        private int _Weight1ToShow;
        private int _Weight2ToShow;
        private int _Weight3ToShow;
        private int _Weight4ToShow;
        private int _Weight5ToShow;
        private int _Weight6ToShow;
        private int _Weight7ToShow;
        public event EventHandler CloseWindowEvent;
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<ModulusType> AvailableModulusTypes { get; set; }
        public ObservableCollection<ModulusPosition> AvailableModulusPositions { get; set; }
        ModulusType _SelectedModulusType;
        ModulusPosition _SelectedModulusPosition;
        public ModulusType SelectedModulusType
        {
            get
            {
                return _SelectedModulusType;
            }
            set
            {
                if (_SelectedModulusType != value)
                {
                    _SelectedModulusType = value;
                    _matrixProcessor.SetModulusType(_SelectedModulusType.ModulusTypeName);
                    OnPropertyChanged("_SelectedModulusType");
                }
            }
        }
        public ModulusPosition SelectedModulusPosition
        {
            get
            {
                return _SelectedModulusPosition;
            }
            set
            {
                if (_SelectedModulusPosition != value)
                {
                    _SelectedModulusPosition = value;
                    _matrixProcessor.SetModulusPosition(_SelectedModulusPosition.ModulusPositionName);
                    OnPropertyChanged("_SelectedModulusPosition");
                }
            }
        }

        public int ModulusToShow
        {
            get { return _ModulusToShow; }

            set
            {
                this._ModulusToShow = value;
                RaisePropertyChanged("ModulusToShow");
            }
        }

        public int Weight0ToShow
        {
            get { return _Weight0ToShow; }

            set
            {
                this._Weight0ToShow = value;
                RaisePropertyChanged("Weight0ToShow");
            }
        }
        public int Weight1ToShow
        {
            get { return _Weight1ToShow; }

            set
            {
                this._Weight1ToShow = value;
                RaisePropertyChanged("Weight1ToShow");
            }
        }
        public int Weight2ToShow
        {
            get { return _Weight2ToShow; }

            set
            {
                this._Weight2ToShow = value;
                RaisePropertyChanged("Weight2ToShow");
            }
        }
        public int Weight3ToShow
        {
            get { return _Weight3ToShow; }

            set
            {
                this._Weight3ToShow = value;
                RaisePropertyChanged("Weight3ToShow");
            }
        }

        public int Weight4ToShow
        {
            get { return _Weight4ToShow; }

            set
            {
                this._Weight4ToShow = value;
                RaisePropertyChanged("Weight4ToShow");
            }
        }
        public int Weight5ToShow
        {
            get { return _Weight5ToShow; }

            set
            {
                this._Weight5ToShow = value;
                RaisePropertyChanged("Weight5ToShow");
            }
        }
        public int Weight6ToShow
        {
            get { return _Weight6ToShow; }

            set
            {
                this._Weight6ToShow = value;
                RaisePropertyChanged("Weight6ToShow");
            }
        }
        public int Weight7ToShow
        {
            get { return _Weight7ToShow; }

            set
            {
                this._Weight7ToShow = value;
                RaisePropertyChanged("Weight7ToShow");
            }
        }

        public ViewModelModulePage()
        {
            ModulusToShow = _matrixProcessor.GetModulus();

            Weight0ToShow = _weightingProcessor.GetWeightingArray()[0];
            Weight1ToShow = _weightingProcessor.GetWeightingArray()[1];
            Weight2ToShow = _weightingProcessor.GetWeightingArray()[2];
            Weight3ToShow = _weightingProcessor.GetWeightingArray()[3];
            Weight4ToShow = _weightingProcessor.GetWeightingArray()[4];
            Weight5ToShow = _weightingProcessor.GetWeightingArray()[5];
            Weight6ToShow = _weightingProcessor.GetWeightingArray()[6];
            Weight7ToShow = _weightingProcessor.GetWeightingArray()[7];

            SelectedModulusType = _ModulusTypeName;
            AvailableModulusTypes = new ObservableCollection<ModulusType>()
            {
                new ModulusType(_matrixProcessor){ModulusTypeName="Module" },
                new ModulusType(_matrixProcessor){ModulusTypeName="Complement" },
                new ModulusType(_matrixProcessor){ModulusTypeName="Modulus w/o 0" },
                new ModulusType(_matrixProcessor){ModulusTypeName="Mod10 Weighted3*1" },
                new ModulusType(_matrixProcessor){ModulusTypeName="Mod10 Weighted1*3" }
            };
            SelectedModulusType = AvailableModulusTypes.First();

            SelectedModulusPosition = _ModulusPosition;
            AvailableModulusPositions = new ObservableCollection<ModulusPosition>()
            {
                new ModulusPosition(_matrixProcessor){ModulusPositionName="Beginning" },
                new ModulusPosition(_matrixProcessor){ModulusPositionName="End" }

            };
            SelectedModulusPosition = AvailableModulusPositions.First();


            TakeModulusCommand = new Numbering3.Helper.RelayCommand(TakeModulus, canexecute);
        }

        private bool canexecute(object parameter)
        {
            return true;
        }

        private void TakeModulus(object parameter)
        {
            _matrixProcessor.SetModulus(ModulusToShow);
            _matrixProcessor.SetModulusType(SelectedModulusType.ModulusTypeName);
            _matrixProcessor.SetModulusPosition(SelectedModulusPosition.ModulusPositionName);

            _weightingProcessor.AddValueToWeightinhArray(0, Weight0ToShow);
            _weightingProcessor.AddValueToWeightinhArray(1, Weight1ToShow);
            _weightingProcessor.AddValueToWeightinhArray(2, Weight2ToShow);
            _weightingProcessor.AddValueToWeightinhArray(3, Weight3ToShow);
            _weightingProcessor.AddValueToWeightinhArray(4, Weight4ToShow);
            _weightingProcessor.AddValueToWeightinhArray(5, Weight5ToShow);
            _weightingProcessor.AddValueToWeightinhArray(6, Weight6ToShow);
            _weightingProcessor.AddValueToWeightinhArray(7, Weight6ToShow);
            CloseWindowEvent(this, null);
        }
            public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
