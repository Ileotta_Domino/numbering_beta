﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace Numbering3.ViewModel
{
    class ViewModelCounterSettings : ViewModelBase, IClosableViewModel, INotifyPropertyChanged
    {
        IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        ISequenceTranslatorHelper _sequenceTranslatorHelper = new SequenceTranslatorHelper();
        IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        public string _SequenceToShow = "NNN";
        public string _AlphabetToShow = "abc";
        public event PropertyChangedEventHandler PropertyChanged;
        public SequenceValue _SequenceValueToShow { get; set; }
        public StartPointValue _StartPointValue { get; set; }
        public RelayCommand CounterSettingsCommand { get; set; }
        public event EventHandler CloseWindowEvent;
        public string _SequenceValue;

        public String SequenceValue
        {
            get
            {

                //if (_SequenceValueToShow.SequenceValuetoShow != _SequenceToShow)
                //    _SequenceToShow = _SequenceValueToShow.SequenceValuetoShow;
                return _SequenceValue;
            }

            set
            {
                if (_SequenceToShow != value)
                {
                    this._SequenceValue = value;
                      _SequenceValueToShow.SequenceValuetoShow = value;
                    _sequenceProcessor.SetSequenceToKeepValue(_SequenceValue);
                }
                RaisePropertyChanged("SequencetoShow");
            }
        }

        public String AlphabetToShow
        {
            get { return _AlphabetToShow; }

            set
            {
                this._AlphabetToShow = value;
                RaisePropertyChanged("CharNameFromTB");
            }
        }

        public ViewModelCounterSettings()
        {


            _StartPointValue = new StartPointValue(_matrixProcessor);
            _SequenceValueToShow = new SequenceValue(_sequenceProcessor);

            if (_sequenceProcessor.GetAlphabetToKeepValue() != "abc")
                AlphabetToShow = _sequenceProcessor.GetAlphabetToKeepValue();
            if (_sequenceProcessor.GetAlphabetCToKeepValue() != "abc")
                AlphabetToShow = _sequenceProcessor.GetAlphabetCToKeepValue();
            if (_sequenceProcessor.GetSequenceToKeepValue() != "NNN")
            {
//                _SequenceToShow = _sequenceProcessor.GetSequenceToKeepValue();
               _SequenceValueToShow.SequenceValuetoShow= _sequenceProcessor.GetSequenceToKeepValue();
            }
            CounterSettingsCommand = new Numbering3.Helper.RelayCommand(CounterSettingsStart, canexecute);
            if (_matrixProcessor.GetStartPointToKeep() != "1")
            { _StartPointValue.StartPoint = _matrixProcessor.GetStartPointToKeep();
        }else{
                _StartPointValue.StartPoint = _matrixProcessor.GetStartPoint();
        }


        }



        private bool canexecute(object parameter)
        {
            return true;
        }

        private void CounterSettingsStart(object parameter)
        {
            _sequenceProcessor.SetIsSequenceValue(false);
       //     if (parameter == null) return;

            //set flag to defalult
            var values = (object[])parameter;
            if (values[0] != null)
            {
                //string alphabetType = values[0].ToString();
                _sequenceProcessor.AssignAlphabet(values[0].ToString());
                _sequenceProcessor.SetAlphabetToKeepValue(values[0].ToString());

            }
            if (values[1] != null)
            {
                string sequenceType = values[1].ToString();
                  _sequenceProcessor.SetSequence(values[1].ToString());
                _sequenceProcessor.SetSequenceToKeepValue(values[1].ToString());
                  _sequenceProcessor.SetSequenceLength(values[0].ToString().Length);
                _sequenceProcessor.CreateSequenceArray(_sequenceProcessor.GetSequence().Length, values[1].ToString());
                _SequenceValueToShow.SequenceValueToKeep = _sequenceProcessor.GetSequence();
                _SequenceValueToShow.SequenceValuetoShow= _sequenceProcessor.GetSequence();
                var match = sequenceType.IndexOfAny(new char[] { 'B' }) != -1;
                if (match == true)
                {
                    _sequenceProcessor.SetIsSequenceValue(true);
                }
                var matchC = sequenceType.IndexOfAny(new char[] { 'C' }) != -1;
                if (matchC == true)
                {
                    _sequenceProcessor.SetIsSequenceValue(true);
                }
                //  _sequenceTranslatorHelper.CalculateScalesInArray(sequenceType.ToCharArray());
                _sequenceTranslatorHelper.CalculateScalesInArray(_sequenceProcessor.GetSequence().ToCharArray());
            }


            if (values[2] != null)
            {

                _matrixProcessor.SetStartPoint(  values[2].ToString());

            }
            if (values[3] != null)
            {

                _sequenceProcessor.AssignAlphabet(values[3].ToString());
                _sequenceProcessor.SetAlphabetCToKeepValue(values[3].ToString());

            }
            CloseWindowEvent(this, null);

        }
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        void DataWindow_Closing(object sender)
        {

        }
    }
}
