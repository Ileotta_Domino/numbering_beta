﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model;
using Numbering3.Model.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using Numbering3.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using static Numbering3.Model.SwitcherContext;

namespace Numbering3.ViewModel
{
    class ViewModelMain : ViewModelBase, IClosableViewModel, INotifyPropertyChanged
    {
        IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        INumberingProcess _numberingProcess = new NumberingProcess();
        ISwitcherContext _switcherContext = new SwitcherContext();
        ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        IListRowsProcessor _listRowsProcessor = new ListRowsProcessor();
        ISequenceTranslatorHelper _sequenceTranslatorHelper = new SequenceTranslatorHelper();
        IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        //   public  RowValue ValueofRow;
        private Dictionary<string, int> BitCheck = new Dictionary<string, int>();
        public ObservableCollection<Order> AvailableOrderings { get; set; }
        public ObservableCollection<CheckType> BitChecking { get; set; }
        public ObservableCollection<Alignment> AlignmentToShow { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public RowValue _RowValueToShow { get; set; }
        public ColumnValue _ColumnValueToShow { get; set; }
        public SheetValue _SheetValeToShow { get; set; }
        public StackValue _StackValueToShow { get; set; }
        public PackValue _PackValueToShow { get; set; }
        public MultiplyByColumn _MultiplyByColumnToShow { get; set; }
        public MultiplyBySheet _MultiplyBySheetToShow { get; set; }
        public ValuesToShow _ValueToShow { get; set; }
        public bool _CanMyPropertyBeChanged;
        public bool _CanColumPropertyBeChanged;

        private int _PackValue;
        private int _StackValue;
        private bool _AddZeroesCheck;
        private bool _CheckDigitCheck;
        private bool _showResultsButtonEnabled;
        public string _DateTimeValue;
        Alignment _SelectedAlignment;
        Order _SelectedOrder;
        CheckType _SelectedBitChecking;
        public event EventHandler CloseWindowEvent;
        string _LeadInToShow;
        string _ValueZeroLast;
        string _ValueZeroZero;
        string _ValueLastLast;
        string _ValueZeroZeroLast;
        string _ValueZeroZeroOneStack;
        string _ValueLastLastPack;
        string _ValueFirstSheetFirstStackLastEle;
        string _ValueLastSheetFirstStackFirstElement;
        string _LeadOutToShow;
        string _ValueAlpha;
        string _ValueAlphaZeroOne;
        string _ValueAlphaOneZero;
        string _ValueBeta;
        string _ValueGamma;
        string _ValueDelta;
        string _ValueEpsilon;
        string _ZetaValue;
        string _EtaValue;
        string _ThetaValue;
        string _IotaValue;
        string _KappaValue;
        string _LambdaValue;
        string _MuValue;
        string _NuValue;
        string _XiValue;
        string _OmicronValue;
        string _PiValue;
        string _RoValue;
        string _SigmaValue;
        string _SigmaZeroOneValue;
        string _SigmaZeroLastValue;
        string _OmegaValue;

        public bool showResultsButtonEnabled
        {
            get
            {
                return _showResultsButtonEnabled;
            }
            set
            {
                _showResultsButtonEnabled = value;
                OnPropertyChanged("showResultsButtonEnabled");
            }
        }
        public string LeadOutToShow
        {
            get { return _LeadOutToShow; }
            set
            {
                if (_LeadOutToShow != value)
                {
                    _LeadOutToShow = value;
                    OnPropertyChanged("LeadOutToShow");
                }
            }
        }
        
                    public string LeadInToShow
        {
            get { return _LeadInToShow; }
            set
            {
                if (_LeadInToShow != value)
                {
                    _LeadInToShow = value;
                    OnPropertyChanged("LeadInToShow");
                }
            }
        }

        public string ValueAlpha
        {
            get
            {
                return _ValueAlpha;
            }
            set
            {
                if (_ValueAlpha != value)
                {
                    _ValueAlpha = value;
                    OnPropertyChanged("ValueAlpha");
                }
            }
        }
        public string ValueAlphaZeroOne
        {
            get
            {
                return _ValueAlphaZeroOne;
            }
            set
            {
                if (_ValueAlphaZeroOne != value)
                {
                    _ValueAlphaZeroOne = value;
                    OnPropertyChanged("ValueAlphaZeroOne");
                }
            }
        }
        public string ValueAlphaOneZero
        {
            get
            {
                return _ValueAlphaOneZero;
            }
            set
            {
                if (_ValueAlphaOneZero != value)
                {
                    _ValueAlphaOneZero = value;
                    OnPropertyChanged("ValueAlphaOneZero");
                }
            }
        }
        

        public string ValueBeta
        {
            get
            {
                return _ValueBeta;
            }
            set
            {
                if (_ValueBeta != value)
                {
                    _ValueBeta = value;
                    OnPropertyChanged("ValueBeta");
                }
            }
        }
        public string ValueGamma
        {
            get
            {
                return _ValueGamma;
            }
            set
            {
                if (_ValueGamma != value)
                {
                    _ValueGamma = value;
                    OnPropertyChanged("ValueGamma");
                }
            }
        }
        public string ValueDelta
        {
            get
            {
                return _ValueDelta;
            }
            set
            {
                if (_ValueDelta != value)
                {
                    _ValueDelta = value;
                    OnPropertyChanged("ValueDelta");
                }
            }
        }
        public string ValueEpsilon
        {
            get
            {
                return _ValueEpsilon;
            }
            set
            {
                if (_ValueEpsilon != value)
                {
                    _ValueEpsilon = value;
                    OnPropertyChanged("ValueEpsilon");
                }
            }
        }
        public string ZetaValue
        {
            get
            {
                return _ZetaValue;
            }
            set
            {
                if (_ZetaValue != value)
                {
                    _ZetaValue = value;
                    OnPropertyChanged("ZetaValue");
                }
            }
        }
        public string EtaValue
        {
            get
            {
                return _EtaValue;
            }
            set
            {
                if (_EtaValue != value)
                {
                    _EtaValue = value;
                    OnPropertyChanged("EtaValue");
                }
            }
        }
        public string ThetaValue
        {
            get
            {
                return _ThetaValue;
            }
            set
            {
                if (_ThetaValue != value)
                {
                    _ThetaValue = value;
                    OnPropertyChanged("ThetaValue");
                }
            }
        }
        public string IotaValue
        {
            get
            {
                return _IotaValue;
            }
            set
            {
                if (_IotaValue != value)
                {
                    _IotaValue = value;
                    OnPropertyChanged("IotaValue");
                }
            }
        }
        public string KappaValue
        {
            get
            {
                return _KappaValue;
            }
            set
            {
                if (_KappaValue != value)
                {
                    _KappaValue = value;
                    OnPropertyChanged("KappaValue");
                }
            }
        }
        public string LambdaValue
        {
            get
            {
                return _LambdaValue;
            }
            set
            {
                if (_LambdaValue != value)
                {
                    _LambdaValue = value;
                    OnPropertyChanged("LambdaValue");
                }
            }
        }
        public string MuValue
        {
            get
            {
                return _MuValue;
            }
            set
            {
                if (_MuValue != value)
                {
                    _MuValue = value;
                    OnPropertyChanged("MuValue");
                }
            }
        }
        public string NuValue
        {
            get
            {
                return _NuValue;
            }
            set
            {
                if (_NuValue != value)
                {
                    _NuValue = value;
                    OnPropertyChanged("NuValue");
                }
            }
        }
        public string XiValue
        {
            get
            {
                return _XiValue;
            }
            set
            {
                if (_XiValue != value)
                {
                    _XiValue = value;
                    OnPropertyChanged("XiValue");
                }
            }
        }
        public string OmicronValue
        {
            get
            {
                return _OmicronValue;
            }
            set
            {
                if (_OmicronValue != value)
                {
                    _OmicronValue = value;
                    OnPropertyChanged("OmicronValue");
                }
            }
        }
        public string PiValue
        {
            get
            {
                return _PiValue;
            }
            set
            {
                if (_PiValue != value)
                {
                    _PiValue = value;
                    OnPropertyChanged("PiValue");
                }
            }
        }
        public string RoValue
        {
            get
            {
                return _RoValue;
            }
            set
            {
                if (_RoValue != value)
                {
                    _RoValue = value;
                    OnPropertyChanged("RoValue");
                }
            }
        }
        public string SigmaValue
        {
            get
            {
                return _SigmaValue;
            }
            set
            {
                if (_SigmaValue != value)
                {
                    _SigmaValue = value;
                    OnPropertyChanged("SigmaValue");
                }
            }
        }
        public string SigmaZeroOneValue
        {
            get
            {
                return _SigmaZeroOneValue;
            }
            set
            {
                if (_SigmaZeroOneValue != value)
                {
                    _SigmaZeroOneValue = value;
                    OnPropertyChanged("SigmaZeroOneValue");
                }
            }
        }
        public string SigmaZeroLastValue
        {
            get
            {
                return _SigmaZeroLastValue;
            }
            set
            {
                if (_SigmaZeroLastValue != value)
                {
                    _SigmaZeroLastValue = value;
                    OnPropertyChanged("SigmaZeroLastValue");
                }
            }
        }
        
        public string OmegaValue
        {
            get
            {
                return _OmegaValue;
            }
            set
            {
                if (_OmegaValue != value)
                {
                    _OmegaValue = value;
                    OnPropertyChanged("OmegaValue");
                }
            }
        }
        public string ValueZeroLast
        {
            get
            {
                return _ValueZeroLast;
            }
            set
            {
                if (_ValueZeroLast != value)
                {
                    _ValueZeroLast = value;
                    OnPropertyChanged("ValueZeroLast");
                }
            }
        }
        public string ValueLastLast
        {
            get
            {
                return _ValueLastLast;
            }
            set
            {
                if (_ValueLastLast != value)
                {
                    _ValueLastLast = value;
                    OnPropertyChanged("ValueLastLast");
                }
            }
        }
        public string ValueZeroZeroLast
        {
            get
            {
                return _ValueZeroZeroLast;
            }
            set
            {
                if (_ValueZeroZeroLast != value)
                {
                    _ValueZeroZeroLast = value;
                    OnPropertyChanged("ValueZeroZeroLast");
                }
            }
        }


        
        public Alignment SelectedAlignment
        {
            get
            {
                return _SelectedAlignment;
            }
            set
            {
                if (_SelectedAlignment != value)
                {
                    _SelectedAlignment = value;
                    _matrixProcessor.SetAlignment(_SelectedAlignment.AlignmentName.ToString());
                    OnPropertyChanged("SelectedAlignment");
                }
            }
        }
        public Order SelectedOrder
        {
            get
            {
                return _SelectedOrder;
            }
            set
            {
                if (_SelectedOrder != value)
                {
                    _SelectedOrder = value;
                    _matrixProcessor.SetOrder(_SelectedOrder.OrderName.ToString());
                    OnPropertyChanged("SelectedOrder");
                }
            }
        }

        public CheckType SelectedBitChecking
        {
            get
            {
                return _SelectedBitChecking;
            }
            set
            {
                if (_SelectedBitChecking != value)
                {
                    _SelectedBitChecking = value;
                    _matrixProcessor.SetBitCheck(_SelectedBitChecking.CheckTypeName.ToString());
                    OnPropertyChanged("SelectedBitChecking");
                }
            }
        }



        string _TextProperty1;
        public string TextProperty1
        {
            get
            {
                return _TextProperty1;
            }
            set
            {
                if (_TextProperty1 != value)
                {
                    _TextProperty1 = value;
                    OnPropertyChanged("TextProperty1");
                }
            }
        }

        public int PackValue
        {
            get
            {
                return _PackValue;
            }
            set
            {
                if (_PackValue != value)
                {
                    _PackValue = value;
                    _matrixProcessor.SetPack(_PackValue);
                    if (_PackValue > 2)
                    {
                        CanMyPropertyBeChanged = true;
                    }
                    else { CanMyPropertyBeChanged = false; }
                    OnPropertyChanged("PackValue");
                }
            }
        }

        public int StackValue
        {
            get
            {
                return _StackValue;
            }
            set
            {
                if (_StackValue != value)
                {
                    _StackValue = value;
                    _matrixProcessor.SetStack(_StackValue);
                    OnPropertyChanged("StackValue");
                }
            }
        }

        public string ValueZeroZeroOneStack
        {
            get
            {
                return _ValueZeroZeroOneStack;
            }
            set
            {
                if (_ValueZeroZeroOneStack != value)
                {
                    _ValueZeroZeroOneStack = value;
                    OnPropertyChanged("ValueZeroZeroOneStack");
                }
            }
        }

        public string ValueFirstSheetFirstStackLastEle
        {
            get
            {
                return _ValueFirstSheetFirstStackLastEle;
            }
            set
            {
                if (_ValueFirstSheetFirstStackLastEle != value)
                {
                    _ValueFirstSheetFirstStackLastEle = value;
                    OnPropertyChanged("ValueFirstSheetFirstStackLastEle");
                }
            }
        }

        

        public string ValueLastLastPack
        {
            get
            {
                return _ValueLastLastPack;
            }
            set
            {
                if (_ValueLastLastPack != value)
                {
                    _ValueLastLastPack = value;
                    OnPropertyChanged("ValueLastLastPack");
                }
            }
        }
        public string ValueLastSheetFirstStackFirstElement
        {
            get
            {
                return _ValueLastSheetFirstStackFirstElement;
            }
            set
            {
                if (_ValueLastSheetFirstStackFirstElement != value)
                {
                    _ValueLastSheetFirstStackFirstElement = value;
                    OnPropertyChanged("ValueLastSheetFirstStackFirstElement");
                }
            }
        }

        
        public bool AddZeroesCheck
        {
            get
            {
                // return (_AddZeroesCheck != null) ? _AddZeroesCheck : false; 
                return _AddZeroesCheck;
            }
            set
            {
                if (_AddZeroesCheck != value)
                {
                    _AddZeroesCheck = value;
                    _matrixProcessor.SetAddZeroeValue(_AddZeroesCheck);
                    OnPropertyChanged("AddZeroesCheck");
                }
            }
        }

        public bool CheckDigitCheck
        {
            get
            {
                // return (_AddZeroesCheck != null) ? _AddZeroesCheck : false; 
                return _CheckDigitCheck;
            }
            set
            {
                if (_CheckDigitCheck != value)
                {
                    _CheckDigitCheck = value;
                    _matrixProcessor.SetCheckDigitValue(_CheckDigitCheck);
                    OnPropertyChanged("CheckDigitCheck");
                }
            }
        }

        public bool CanMyPropertyBeChanged
        {
            get
            {
                // return (_AddZeroesCheck != null) ? _AddZeroesCheck : false; 
                return _CanMyPropertyBeChanged;
            }
            set
            {
                if (_CanMyPropertyBeChanged != value)
                {
                    _CanMyPropertyBeChanged = value;

                    OnPropertyChanged("CanMyPropertyBeChanged");
                }
            }
        }

        //public bool CanColumPropertyBeChanged
        //{
        //    get
        //    {
        //        // return (_AddZeroesCheck != null) ? _AddZeroesCheck : false; 
        //        return _CanColumPropertyBeChanged;
        //    }
        //    set
        //    {
        //        if (_CanColumPropertyBeChanged != value)
        //        {
        //            _CanColumPropertyBeChanged = value;

        //            OnPropertyChanged("CanColumPropertyBeChanged");
        //        }
        //    }
        //}
        public string DateTimeValue
        {
            get
            {
                // return (_AddZeroesCheck != null) ? _AddZeroesCheck : false; 
                return _DateTimeValue;
            }
            set
            {
                if (_DateTimeValue != value)
                {
                    _DateTimeValue = value;

                    OnPropertyChanged("DateTimeValue");
                }
            }
        }

        public RelayAsyncCommand StartCommand { get; set; }
        public RelayCommand LeadingZeroCommand { get; set; }
        public RelayCommand CounterSettingCommand { get; set; }
        public RelayCommand GoRolledPaperPageCommand { get; set; }
        public RelayCommand GoShowResultPageCommand { get; set; }
        public RelayCommand GoOpenConfigurationPageCommand { get; set; }
        public RelayCommand GoToModulePageCommand { get; set; }
        private RelayCommand CloseCommand { get; set; }

        // public ViewModelMain(IMatrixProcessor matrixProcessor )
        public ViewModelMain()
        {
            _listRowsProcessor.ListOfRowsCreate();
            string DefaultTypeString = "NNN";
            _sequenceProcessor.SetSequence(DefaultTypeString);
            _sequenceTranslatorHelper.CalculateScalesInArray(_sequenceProcessor.GetSequence().ToCharArray());
            int MaxValuePossible = _alphabetConverterHelper.CalculateMaxValueToBeWritten();
            char[] LeadOutArr = { 'N' };
            char[] LeadInArr = { 'N' };
            string LeadInUpdate = "0";



            //   _matrixProcessor.setar
            _matrixProcessor.SetArrayInLength(4);
            _matrixProcessor.SetArrayOutLength(4);
            _matrixProcessor.CreateLeadInArray();
            _matrixProcessor.CreateLeadOutArray();

            _matrixProcessor.SetLeadInArrayLength(1);
            _matrixProcessor.SetLeadOutArrayLength(1);
            _matrixProcessor.SetLeadInArray(LeadInArr);
            _matrixProcessor.SetLeadOutArray(LeadInArr);

            string ValueAlphaZeroOne = "0";
            ValueAlphaOneZero = "0";
            ValueBeta = "0";
            ValueGamma = "0";
            ValueDelta = "0";
            ValueEpsilon = "0";
            ZetaValue = "0";
            EtaValue = "0";
            ThetaValue = "0";
            IotaValue = "0";
            KappaValue = "0";
            LambdaValue = "0";
            MuValue = "0";
            NuValue = "0";
            XiValue = "0";
            OmicronValue = "0";
            PiValue = "0";
            RoValue = "0";
            SigmaValue = "0";
            SigmaZeroOneValue = "0";
            SigmaZeroLastValue = "0";
            OmegaValue = "0";
            AddZeroesCheck = false;

            string sequenceType = DefaultTypeString;

            //_matrixProcessor= matrixProcessor;
            AvailableOrderings = new ObservableCollection<Order>()
            {
            new Order(_matrixProcessor) { OrderName = "Row,Columns,Sheets" },
            new Order(_matrixProcessor) { OrderName = "Columns,Row,Sheets" },
            new Order(_matrixProcessor) { OrderName = "Sheets,Columns,Row" },
            new Order(_matrixProcessor) { OrderName = "Columns,Sheets,Row" }
            };

            BitChecking = new ObservableCollection<CheckType>
            {
            new CheckType(_matrixProcessor) { CheckTypeName = "None" },
            new CheckType(_matrixProcessor) { CheckTypeName = "Modulus 2" },
            new CheckType(_matrixProcessor) { CheckTypeName = "Modulus 3" }
            };

            AlignmentToShow = new ObservableCollection<Alignment>
            {
              new Alignment(_matrixProcessor) { AlignmentName = "Horizontal" },
              new Alignment(_matrixProcessor) { AlignmentName = "Vertical" }
            };
            
            SelectedOrder = AvailableOrderings.First();
            SelectedBitChecking = BitChecking.First();
            SelectedAlignment = AlignmentToShow.First();

            _RowValueToShow = new RowValue(_matrixProcessor);
            _ColumnValueToShow = new ColumnValue(_matrixProcessor);
            _SheetValeToShow = new SheetValue(_matrixProcessor);
            _StackValueToShow = new StackValue(_matrixProcessor);
            _PackValueToShow = new PackValue(_matrixProcessor);
            _MultiplyByColumnToShow = new MultiplyByColumn(_matrixProcessor);
            _MultiplyBySheetToShow = new MultiplyBySheet(_matrixProcessor);
        //    _AddZeroesCheck = new AddZeroesCheck(_matrixProcessor);
            _ValueToShow = new ValuesToShow(_matrixProcessor);
            
            StackValue = _matrixProcessor.GetStack();
            PackValue = _matrixProcessor.GetPack();
            AddZeroesCheck = _matrixProcessor.GetAddZeroeValue();

            if (_matrixProcessor.GetLeadOutArrayLength() != 0)
            {
                    LeadOutToShow = _matrixProcessor.GetLeadOutToKeepValue();
            }


            if (_matrixProcessor.GetLeadInArrayLength() != 0)
            {
                LeadInToShow = _matrixProcessor.GetLeadInToKeepValue();
            }
            //if (_sequenceProcessor.GetSequence()!="NNN")
            //{
            //    _sequenceProcessor.SetSequence("NNN");
            //}
                if (_RowValueToShow.RowValueToKeep != 1 && _ColumnValueToShow.ColumnValueToKeep != 1 && _SheetValeToShow.SheetValueToKeep != 1 && _StackValueToShow.StackValueToKeep != 1 && _PackValueToShow.PackValueToKeep != 1)
            {
                //if (Int32.Parse(_RowValueToShow.RowValuetoShow) < 1)
                //{ _RowValueToShow.CanRowPropertyBeChanged = false; }
                //else { _RowValueToShow.CanRowPropertyBeChanged = true; }
                _RowValueToShow.RowValuetoShow = _alphabetConverterHelper.FormattingIterating(_RowValueToShow.RowValueToKeep, 0, "");
                _RowValueToShow.LastRowColumn = _alphabetConverterHelper.FormattingIterating(_ColumnValueToShow.ColumnValueToKeep, 0, "");

                _ValueToShow.ValueZeroLast = _matrixProcessor.GetValueZeroLast();
                //  if (_ColumnValueToShow.ColumnValueToKeep != 1)
                _ColumnValueToShow.ColumnValuetoShow = _alphabetConverterHelper.FormattingIterating(_ColumnValueToShow.ColumnValueToKeep, 0, "");
                //if (Int32.Parse(_ColumnValueToShow.ColumnValuetoShow) <= 2)
                //{ _ColumnValueToShow.CanColumPropertyBeChanged  = false; }
                //else { _ColumnValueToShow.CanColumPropertyBeChanged = true; }
                if (_SheetValeToShow.SheetValueToKeep != 1)
                    _SheetValeToShow.SheetValuetoShow = _SheetValeToShow.SheetValueToKeep.ToString();
                _ValueToShow.ValueLastLast = _SheetValeToShow.SheetValueToKeep.ToString();

                _StackValueToShow.StackValuetoShow = _StackValueToShow.StackValueToKeep.ToString();
                _PackValueToShow.PackValuetoShow = _PackValueToShow.PackValueToKeep.ToString();


                ValueZeroLast = _matrixProcessor.GetValueZeroLast();
                ValueAlpha = _matrixProcessor.GetValueZeroZero();
                ValueAlphaZeroOne = _matrixProcessor.GetValueAlphaZeroOneValue();
                ValueAlphaOneZero = _matrixProcessor.GetValueAlphaOneZeroValue();
                ValueBeta = _matrixProcessor.GetValueBetaValue();
                ValueGamma = _matrixProcessor.GetValueGammaValue();
                ValueDelta = _matrixProcessor.GetValueDeltaValue();
                ValueEpsilon = _matrixProcessor.GetValueEpsilonValue();
                EtaValue  = _matrixProcessor.GetValueEtaValue();
                ThetaValue= _matrixProcessor.GetValueThetaValue();
                IotaValue = _matrixProcessor.GetValueIotaValue();
                KappaValue = _matrixProcessor.GetValueKappaValue();
                LambdaValue = _matrixProcessor.GetValueKappaValue();
                NuValue = _matrixProcessor.GetValueNuValue();
                MuValue = _matrixProcessor.GetValueMuValue();
                XiValue = _matrixProcessor.GetValueXiValue();
                OmicronValue = _matrixProcessor.GetValueOmicronValue();
                PiValue = _matrixProcessor.GetValuePiValue();
                RoValue = _matrixProcessor.GetValueRoValue();
                SigmaValue = _matrixProcessor.GetValueSigmaValue();
                SigmaZeroOneValue = _matrixProcessor.GetValueSigmaZeroOneValue();
                SigmaZeroLastValue = _matrixProcessor.GetValueSigmaZeroLastValue();
                ZetaValue = _matrixProcessor.GetValueZetaValue();
                OmegaValue = _matrixProcessor.GetValueOmegaValue();


                ValueLastLast = _matrixProcessor.GetValueLastLast();
                ValueZeroZeroLast = _matrixProcessor.GetValueZeroZeroLast();
                ValueZeroZeroOneStack = _matrixProcessor.GetValueZeroZeroOneStack();
                ValueLastSheetFirstStackFirstElement = _matrixProcessor.GetValueFirstSheetFirstStackFirstElement(); 
                ValueFirstSheetFirstStackLastEle = _matrixProcessor.GetValueFirstSheetFirstStackLastEle();
                ValueLastLastPack = _matrixProcessor.GetValueLastLastPack();

            }
            else {
                _RowValueToShow.RowValuetoShow = _matrixProcessor.GetRow().ToString();
                _RowValueToShow.LastRowColumn = _matrixProcessor.GetSheet().ToString();
                _ColumnValueToShow.ColumnValuetoShow = _matrixProcessor.GetColumn().ToString();
                ValueZeroLast = _matrixProcessor.GetValueZeroLast();
                ValueAlpha = _matrixProcessor.GetValueZeroZero();
                ValueAlphaZeroOne = _matrixProcessor.GetValueAlphaZeroOneValue();
                ValueAlphaOneZero = _matrixProcessor.GetValueAlphaOneZeroValue();
                ValueBeta = _matrixProcessor.GetValueBetaValue();
                ValueGamma = _matrixProcessor.GetValueGammaValue();
                ValueDelta = _matrixProcessor.GetValueDeltaValue();
                ValueEpsilon = _matrixProcessor.GetValueEpsilonValue();
                EtaValue = _matrixProcessor.GetValueEtaValue();
                ThetaValue = _matrixProcessor.GetValueThetaValue();
                IotaValue = _matrixProcessor.GetValueIotaValue();
                KappaValue = _matrixProcessor.GetValueKappaValue();
                LambdaValue = _matrixProcessor.GetValueKappaValue();
                MuValue = _matrixProcessor.GetValueMuValue();
                NuValue = _matrixProcessor.GetValueNuValue();
                XiValue = _matrixProcessor.GetValueXiValue();
                OmicronValue = _matrixProcessor.GetValueOmicronValue();
                PiValue = _matrixProcessor.GetValuePiValue();
                RoValue = _matrixProcessor.GetValueRoValue();
                SigmaValue = _matrixProcessor.GetValueSigmaValue();
                SigmaZeroOneValue = _matrixProcessor.GetValueSigmaZeroOneValue();
                SigmaZeroLastValue = _matrixProcessor.GetValueSigmaZeroLastValue();
                ZetaValue = _matrixProcessor.GetValueZetaValue();
                OmegaValue = _matrixProcessor.GetValueOmegaValue();
                ValueLastLast = _matrixProcessor.GetValueLastLast();
                ValueZeroZeroLast = _matrixProcessor.GetValueZeroZeroLast();
                ValueZeroZeroOneStack= _matrixProcessor.GetValueZeroZeroOneStack();
                ValueLastLastPack = _matrixProcessor.GetValueLastLastPack();
                ValueFirstSheetFirstStackLastEle = _matrixProcessor.GetValueFirstSheetFirstStackLastEle();
                ValueLastSheetFirstStackFirstElement = _matrixProcessor.GetValueFirstSheetFirstStackFirstElement();
                _SheetValeToShow.SheetValuetoShow = _matrixProcessor.GetSheet().ToString();
                _ValueToShow.ValueLastLast = _matrixProcessor.GetSheet().ToString();
                _StackValueToShow.StackValuetoShow = _matrixProcessor.GetStack().ToString();
                _PackValueToShow.PackValuetoShow = _matrixProcessor.GetPack().ToString();             

            }
            //      _LeadInToShow = "test";
            //LeadInToShow = LeadInToShow;
            if (_MultiplyByColumnToShow.MultiplyByColumnValueToKeep != 1)
            {
                _MultiplyByColumnToShow.MultiplyByColumnValueToShow = _MultiplyByColumnToShow.MultiplyByColumnValueToKeep;
            }
            else {
                _MultiplyByColumnToShow.MultiplyByColumnValueToShow = _matrixProcessor.GetMultiplyByColumnValue();
            }

            if (_MultiplyBySheetToShow.MultiplyBySheetValueToKeep != 1)
            {
                _MultiplyBySheetToShow.MultiplyBySheetValueToShow = _MultiplyBySheetToShow.MultiplyBySheetValueToKeep;
            }
            else
            {
                _MultiplyBySheetToShow.MultiplyBySheetValueToShow = _matrixProcessor.GetMultiplyBySheetValue();
            }

            
              LeadInToShow = _matrixProcessor.GetLeadInToKeepValue(); ;

            StartCommand = new Numbering3.Helper.RelayAsyncCommand(executeAsync, canexecute);
          
            
            LeadingZeroCommand = new Numbering3.Helper.RelayCommand(LeadingZero, canexecute);
            CounterSettingCommand = new Numbering3.Helper.RelayCommand(CounterSetting, canexecute);
            GoRolledPaperPageCommand = new Numbering3.Helper.RelayCommand(GoToRolledPaperPage, canexecute);
            GoShowResultPageCommand = new Numbering3.Helper.RelayCommand(GoToShowResultPagePage, canexecute);
            GoOpenConfigurationPageCommand = new Numbering3.Helper.RelayCommand(GoToConfigurationPage, canexecute);
            GoToModulePageCommand = new Numbering3.Helper.RelayCommand(GoToModulePage, canexecute);
   //         CloseCommand = new  Numbering3.Helper.RelayCommand(CloseApplication, canexecute);



        }

        void AddUser(object parameter)
        {
            if (parameter == null) return;
            AvailableOrderings.Add(new Order(_matrixProcessor) { OrderName = parameter.ToString() });

        }

        private void TextBox_Loaded_1(object sender, RoutedEventArgs e)
        { ((TextBox)sender).Text = "Default text"; }

        private bool canexecute(object parameter)
        {
            //if (Number1 != null || Number2 != null || Number3 != null)
            //{
            return true;
            //}
            //else { return false; }
        }


         private async Task executeAsync(object parameter)        
        {
            if (parameter == null) return;
            var values = (object[])parameter;
            int num4 = 0;
            int Maxvalue = 0;
            int Max = 0;

            //int num1 = Convert.ToInt32((string)values[0]);
            //int num2 = Convert.ToInt32((string)values[1]);
            //int num3 = Convert.ToInt32((string)values[2]);
            //int num5= Convert.ToInt32((string)values[3]);

            int num1 = _RowValueToShow.RowValueToKeep;
            int num2 = _ColumnValueToShow.ColumnValueToKeep;
            int num3 = _SheetValeToShow.SheetValueToKeep;
            int num5 = _StackValueToShow.StackValueToKeep;
            bool check1 = (bool)values[5];
            int num6 = _MultiplyBySheetToShow.MultiplyBySheetValueToKeep;
            int num7 = _MultiplyBySheetToShow.MultiplyBySheetValueToKeep;
            if (_sequenceProcessor.GetSequenceLength() > 0)
            {
                num4 = _sequenceProcessor.GetSequenceLength();
            }
        else{ num4 = 0; }
            //if (values[3] != null)
            //{
            //    num4 = Convert.ToInt32((string)values[3]);
            //}

            // NumberSum = num1 + num2 + num3;
            _matrixProcessor.SetRow(num1);

            _matrixProcessor.SetColumn(num2);
            _matrixProcessor.SetSheet(num3);
            Max = (num1 * num2 * num3 * num5 * PackValue) - 1 + Int32.Parse(_matrixProcessor.GetStartPoint());
           // Max = (num1 * num2 * num3) - 1;
            _matrixProcessor.SetMaxNumber(Max);
            _matrixProcessor.SetPack(PackValue);
            _matrixProcessor.SetStack(StackValue);
            

            Maxvalue = ((_matrixProcessor.GetPack()* _matrixProcessor.GetStack() * Max + _matrixProcessor.GetPack()) +_matrixProcessor.GetStack() * Max + _matrixProcessor.GetStack() + Max).ToString().Length + 1;
           // Maxvalue = (num1 * num2 * num3).ToString().Length+1; 
            _matrixProcessor.SetArrayDigitLength(Maxvalue);
            //_matrixProcessor.SetArrayInLength(num4 + _matrixProcessor.GetArrayInLength());
            _matrixProcessor.SetRepetitionNumber(num5);
           

          //  bool selected = (bool)values[5];
            //_IsSelected.IsSelected = selected;
           // _matrixProcessor.SetIsChecked(_addz.IsSelected);
            _matrixProcessor.SetMultiplyByColumnValue(num6);
            _matrixProcessor.SetMultiplyBySheetValue(num7);

            await Task.Run(() => StartAlgo());
            showResultsButtonEnabled = true;
            //StartAlgo();


        }

        void LeadingZero(object parameter)
        {
            var win = new LeadingZero { DataContext = new ViewModelLeadingZero() };
            win.ShowDialog();
         //   win.Show();
            //      CloseWindow();
        }

        public void CloseApplication()
        {
            Application.Current.MainWindow.Close();
          }
          public void Close()
        {
            this.Close();
        }
        void CounterSetting(object parameter)
        {
            var win = new CounterSettings{ DataContext = new ViewModelCounterSettings() };
            win.ShowDialog();
            //    win.Show();
        }

        
        void GoToModulePage(object parameter)
        {
            var win = new Module { DataContext = new ViewModelModulePage() };
            win.ShowDialog();
         //   win.Show();

        }
        void GoToConfigurationPage(object parameter)
        {
            var win = new ConfigurationPage { DataContext = new ViewModelConfigurationPage() };
            win.Show();
            CloseWindowEvent(this, null);
        }
        void GoToRolledPaperPage(object parameter)
        {
            int num1 = _RowValueToShow.RowValueToKeep;
            int num2 = _ColumnValueToShow.ColumnValueToKeep;
            int num3 = _SheetValeToShow.SheetValueToKeep;
            int Maxvalue = 0;
            int Max = 0;

            if (_matrixProcessor.GetOrder() != "Columns,Row")
            { _matrixProcessor.SetOrder("Row,Columns"); }
            _matrixProcessor.SetPack(PackValue);
            _matrixProcessor.SetStack(StackValue);

            Max = (num1 * num2 * num3) - 1;
            Maxvalue = ((_matrixProcessor.GetPack() * _matrixProcessor.GetStack() * Max + _matrixProcessor.GetPack()) + _matrixProcessor.GetStack() * Max + _matrixProcessor.GetStack() + Max).ToString().Length + 1;
            // Maxvalue = (num1 * num2 * num3).ToString().Length+1; 
            _matrixProcessor.SetArrayDigitLength(Maxvalue);
            StartRolledAlgo();

            var win = new RolledPaper { DataContext = new ViewModelRolledPaper() };

        //    CloseWindow();
            win.Show();
            CloseWindowEvent(this, null);
        }

        void GoToShowResultPagePage(object parameter)
        {
            var win = new ShowResult { DataContext = new ViewModelShowResult() };
            CloseWindow();
            win.Show();
            CloseWindowEvent(this, null);
          }
        public void StartRolledAlgo()
        {
            int[,,] RolledMatrixtoShow;
            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;


            _matrixProcessor.CreateRolledMatrix(_matrixProcessor.GetRow(), _matrixProcessor.GetColumn(), _matrixProcessor.GetSheet());
            string chosenorder = _matrixProcessor.GetOrder();
            _switcherContext.DoAlgorithm(chosenorder);
            string ValueStringtoAdd = "";
            char[] baseCharArray;
            int indexbase;



            RolledMatrixtoShow = _matrixProcessor.GetRolledMatrix();
            baseCharArray = _alphabetConverterHelper.makeBaseArray();
            char[] tempcharArray = new char[baseCharArray.Length];
            //    using (StreamWriter twBis = File.CreateText(pathBis))
            //    ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(RolledMatrixtoShow[0, 0, 0], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }
                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }



            ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(RolledMatrixtoShow[0, RolledMatrixtoShow.GetUpperBound(1), 0], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }
                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }
            _ColumnValueToShow.ColumnValuetoShow = ValueStringtoAdd;

            ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(RolledMatrixtoShow[0, 0, RolledMatrixtoShow.GetUpperBound(2)], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }

                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }

        }


        async Task StartAlgo()
        {
            int[,,] MatrixtoShow;
            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            // string pathBis = Path.Combine(location, "NumberingTestBis.txt");
            using (StreamWriter twBis = File.CreateText(@".\NumberingTestBis.txt"))
                //     using (StreamWriter twBis = File.CreateText(pathBis))

                _matrixProcessor.CreateMatrix(_matrixProcessor.GetRow(), _matrixProcessor.GetColumn(), _matrixProcessor.GetSheet());
            string chosenorder = _matrixProcessor.GetOrder();
            _switcherContext.DoAlgorithm(chosenorder);
            string ValueStringtoAdd = "";
            char[] baseCharArray;
            int indexbase;



            MatrixtoShow = _matrixProcessor.GetMatrix();
            baseCharArray = _alphabetConverterHelper.makeBaseArray();
            char[] tempcharArray = new char[baseCharArray.Length];
            //    using (StreamWriter twBis = File.CreateText(pathBis))
            //    ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(MatrixtoShow[0, 0, 0], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }
                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }

            _RowValueToShow.LastRowColumn = _matrixProcessor.GetValueLastLast();
            _RowValueToShow.RowValuetoShow = ValueStringtoAdd;


            ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(MatrixtoShow[0, MatrixtoShow.GetUpperBound(1), 0], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }
                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }
            _ColumnValueToShow.ColumnValuetoShow = ValueStringtoAdd;

            ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(MatrixtoShow[0, 0, MatrixtoShow.GetUpperBound(2)], 0, "");
            if (_matrixProcessor.GetIsChecked() == true)
            {
                indexbase = baseCharArray.Length - 1;
                for (int q = ValueStringtoAdd.Length - 1; q >= 0; q--)
                {
                    baseCharArray[indexbase--] = ValueStringtoAdd[q];
                }

                ValueStringtoAdd = fromCharArrayToString(baseCharArray);
            }
            _SheetValeToShow.SheetValuetoShow = ValueStringtoAdd;

            _RowValueToShow.LastRowColumn = _matrixProcessor.GetValueLastLast().ToString();
            //ValueZeroLast = _matrixProcessor.GetValueZeroLast();
            ValueZeroLast = _matrixProcessor.GetValueZeroLast();
            ValueAlpha = _matrixProcessor.GetValueAlphaValue();
            ValueAlphaZeroOne = _matrixProcessor.GetValueAlphaZeroOneValue();
            ValueAlphaOneZero = _matrixProcessor.GetValueAlphaOneZeroValue();
            ValueBeta = _matrixProcessor.GetValueBetaValue();
            ValueGamma = _matrixProcessor.GetValueGammaValue();
            ValueDelta = _matrixProcessor.GetValueDeltaValue();
            ValueEpsilon = _matrixProcessor.GetValueEpsilonValue();
            EtaValue = _matrixProcessor.GetValueEtaValue();
            ThetaValue = _matrixProcessor.GetValueThetaValue();
            IotaValue = _matrixProcessor.GetValueIotaValue();
            KappaValue = _matrixProcessor.GetValueKappaValue();
            LambdaValue = _matrixProcessor.GetValueLambdaValue();
            MuValue = _matrixProcessor.GetValueMuValue();
            NuValue = _matrixProcessor.GetValueNuValue();
            XiValue = _matrixProcessor.GetValueXiValue();
            OmicronValue = _matrixProcessor.GetValueOmicronValue();
            PiValue = _matrixProcessor.GetValuePiValue();
            RoValue = _matrixProcessor.GetValueRoValue();
            SigmaValue = _matrixProcessor.GetValueSigmaValue();
            SigmaZeroOneValue = _matrixProcessor.GetValueSigmaZeroOneValue();
            SigmaZeroLastValue = _matrixProcessor.GetValueSigmaZeroLastValue();
            ZetaValue = _matrixProcessor.GetValueZetaValue();
            OmegaValue = _matrixProcessor.GetValueOmegaValue();

            ValueLastLast = _matrixProcessor.GetValueLastLast();
            ValueZeroZeroLast = _matrixProcessor.GetValueZeroZeroLast();
            ValueZeroZeroOneStack = _matrixProcessor.GetValueZeroZeroOneStack();
            ValueFirstSheetFirstStackLastEle = _matrixProcessor.GetValueFirstSheetFirstStackLastEle();
            ValueLastSheetFirstStackFirstElement = _matrixProcessor.GetValueFirstSheetFirstStackFirstElement();
            ValueLastLastPack = _matrixProcessor.GetValueLastLastPack();
            LeadOutToShow = _matrixProcessor.GetLeadOutToKeepValue();
            LeadInToShow = _matrixProcessor.GetLeadInToKeepValue();
        }

        ////////////////////////////////////////

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public string fromCharArrayToString(char[] input)
        {
            string result = string.Join("", input);
            return result;
        }

    }
}
