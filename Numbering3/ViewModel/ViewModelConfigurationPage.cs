﻿using Microsoft.Win32;
using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.ViewModel
{
    class ViewModelConfigurationPage : ViewModelBase, IClosableViewModel
    {
        IConfigurationListHelper _configurationListHelper = new ConfigurationListHelper();
        ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        public RelayCommand SaveFileCommand { get; set; }
        public RelayCommand LoadFileCommand { get; set; }
        public RelayCommand ClosePageCommand { get; set; }
       // public SequenceValue _SequenceValueToShow { get; set; }
        public event EventHandler CloseWindowEvent;
        private string _fileToSave;

        public String FileToSave
        {
            get { return _fileToSave; }

            set
            {
                this._fileToSave = value;
                _matrixProcessor.SetConfigurationFileValue(FileToSave);
                RaisePropertyChanged("FileNamefromGui");
            }
        }


        public ViewModelConfigurationPage()
        {
        //    _SequenceValueToShow = new SequenceValue(_sequenceProcessor);

         //   SequenceValue = _sequenceProcessor.GetSequence();

            SaveFileCommand = new RelayCommand(SavePageStart, canexecute);
            LoadFileCommand = new RelayCommand(LoadPageStart, canexecute);
            ClosePageCommand = new RelayCommand(ClosePageStart, canexecute);
         //   ClosePageCommand = new RelayCommand(CloseWin);
        }

        private bool canexecute(object parameter)
        {
            return true;
        }
        private void SavePageStart(object parameter)
        {
            SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog.FileName = _matrixProcessor.GetConfigurationFileValue();  //  "Document"; // Default file name
            saveFileDialog.DefaultExt = ".text"; // Default file extension
            saveFileDialog.Filter = "Text documents (.txt)|*.txt"; // Filter files by extensionSaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();


            Nullable<bool> result = saveFileDialog.ShowDialog();
            if (result == true)
            {
                File.WriteAllText(saveFileDialog.FileName, "");
                string filename = saveFileDialog.FileName;
                _configurationListHelper.WriteActualConfiguration();
                _configurationListHelper.WriteListTofile(FileToSave);
            } 
         //   CloseWindowEvent(this, null);
        }

        

    private void LoadPageStart(object parameter)
    {
        // Create OpenFileDialog 
        Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

        dlg.DefaultExt = ".txt";

        // Display OpenFileDialog by calling ShowDialog method 
        Nullable<bool> result = dlg.ShowDialog();


        // Get the selected file name and display in a TextBox 
        if (result == true)
        {
                // Open document 
                FileToSave = dlg.FileName ;
          //  FileName.Text = FileName;
            _configurationListHelper.ReadConfigurationFile(FileToSave);

        }

    }
        public void CloseWin(object obj)
        {
            Window win = obj as Window;
            win.Close();
        }

        private void ClosePageStart(object parameter)
        {

        
            var win = new MainWindow { DataContext = new ViewModelMain() };
            win.Show();
            CloseWindowEvent(this, null);
            CloseWindow();

        }
        
    }
    
}
