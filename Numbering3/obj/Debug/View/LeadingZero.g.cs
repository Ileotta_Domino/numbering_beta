﻿#pragma checksum "..\..\..\View\LeadingZero.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "F1E49C8017F3AA01BA197B65764CC332BC00CFA4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Numbering3.Helper;
using Numbering3.View;
using Numbering3.ViewModel;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;


namespace Numbering3.View {
    
    
    /// <summary>
    /// LeadingZero
    /// </summary>
    public partial class LeadingZero : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 27 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LeadInText;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LeadOutNumber;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LeadOutText;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox StepBox;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBetweenPacks;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox RecordsOfPacksForTextBetweenPacks;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBetweenSheets;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox RecordsForTextBetweenSheets;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EveryXSheetsNumber;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\View\LeadingZero.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LeadInNumber;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Numbering3;component/view/leadingzero.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\LeadingZero.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 14 "..\..\..\View\LeadingZero.xaml"
            ((Numbering3.View.LeadingZero)(target)).DataContextChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.LeadingZero_DataContextChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.LeadInText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.LeadOutNumber = ((System.Windows.Controls.TextBox)(target));
            
            #line 30 "..\..\..\View\LeadingZero.xaml"
            this.LeadOutNumber.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.LeadOutNumberValidationTextBox);
            
            #line default
            #line hidden
            
            #line 30 "..\..\..\View\LeadingZero.xaml"
            this.LeadOutNumber.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.LeadOutNumber_TextChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.LeadOutText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.StepBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 33 "..\..\..\View\LeadingZero.xaml"
            this.StepBox.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.StepBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.TextBetweenPacks = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.RecordsOfPacksForTextBetweenPacks = ((System.Windows.Controls.TextBox)(target));
            
            #line 39 "..\..\..\View\LeadingZero.xaml"
            this.RecordsOfPacksForTextBetweenPacks.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.RecordsOfPacks_TextChanged);
            
            #line default
            #line hidden
            return;
            case 8:
            this.TextBetweenSheets = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.RecordsForTextBetweenSheets = ((System.Windows.Controls.TextBox)(target));
            
            #line 47 "..\..\..\View\LeadingZero.xaml"
            this.RecordsForTextBetweenSheets.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.RecordsForTextBetweenSheets_TextChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.EveryXSheetsNumber = ((System.Windows.Controls.TextBox)(target));
            
            #line 51 "..\..\..\View\LeadingZero.xaml"
            this.EveryXSheetsNumber.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.EveryXSheetsNumberToShow_TextChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 55 "..\..\..\View\LeadingZero.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.LeadInNumber = ((System.Windows.Controls.TextBox)(target));
            
            #line 72 "..\..\..\View\LeadingZero.xaml"
            this.LeadInNumber.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.LeadInNumberValidationTextBox);
            
            #line default
            #line hidden
            
            #line 72 "..\..\..\View\LeadingZero.xaml"
            this.LeadInNumber.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.LeadInNumber_TextChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

