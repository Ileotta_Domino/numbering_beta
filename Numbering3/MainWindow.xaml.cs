﻿using Microsoft.Win32;
using Numbering3.Helper.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Numbering3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        public MainWindow()
        {
            InitializeComponent();
            this.MouseLeftButtonDown += delegate { this.DragMove(); };
            DataContextChanged += new DependencyPropertyChangedEventHandler(MainWindow_DataContextChanged);
        }

        void MainWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }
        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {

            SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog.FileName = "Document"; // Default file name
            saveFileDialog.DefaultExt = ".text"; // Default file extension
            saveFileDialog.Filter = "Text documents (.txt)|*.txt"; // Filter files by extensionSaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();


            Nullable<bool> result = saveFileDialog.ShowDialog();
            if (result == true)
            {
                string filename = saveFileDialog.FileName;
            }
        }

        private void Repetition_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void RepetitionValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void RowInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            RowInput1.CaretIndex = RowInput1.Text.Length;// (RowInput1.Text.Length, 0);
            RowInput1.ScrollToEnd();
            RowInput1.Focus();
        }
        private void RowInputValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ColumnInputValidationTextBox(object sender, TextCompositionEventArgs e)
        {

            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ColumnInput_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PackInput_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void PacksInputValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
            Close();
        }



        private void ModuleCheckbox_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
