﻿using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class PrintingOptions
    {
        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        public void PrintRowColumnSheet()
        {

            int[,,] Matrix;
            int ArrayInLength = _matrixProcessor.GetArrayInLength();
            int[] digits = new int[ArrayInLength];

            using (TextWriter tw = new StreamWriter("C:\\Users\\I_Leotta\\Documents\\NumberingTest.txt"))
            {
                Matrix = _matrixProcessor.GetMatrix();
                for (int n = 0; n <= Matrix.GetUpperBound(2); n++)
                {
                    for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                    {
                        for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                        {

                            Array.Clear(digits, 0, digits.Length);
                            int count = 0;
                            int val = Matrix[i, j, n];

                            for (; val != 0; val /= 10)
                            {
                                digits[count] = val % 10;
                                count++;
                            }
                            Array.Reverse(digits);

                            if (_matrixProcessor.GetBitCheck() != "None")
                            {
                                if (_matrixProcessor.GetBitCheck() == "Modulus 2")
                                {
                                    digits[0] = Matrix[i, j, n] % 2;
                                }
                                else { digits[0] = Matrix[i, j, n] % 3; }

                            }

                            tw.Write("{0}", digits[0]);

                            for (int k = 1; k < digits.Length; k++)
                            {
                                //translate to letters
                                string hexavigesimal = IntToString(digits[k], Enumerable.Range('A', 26).Select(x => (char)x).ToArray());
                                //    tw.Write("{0}", digits[k]);
                                tw.Write("{0}", hexavigesimal);
                            }
                            tw.Write("{0}", " ");
                        }
                        tw.WriteLine();
                    }
                    tw.WriteLine();
                }
            }
        }

        public void PrintColumnRowSheet()
        { }

        public void PrintSheetColumnRow()
        { }

        public static string IntToString(int value, char[] baseChars)
        {
            string result = string.Empty;
            int targetBase = baseChars.Length;

            do
            {
                result = baseChars[value % targetBase] + result;
                value = value / targetBase;
            }
            while (value > 0);

            return result;
        }

    }
}
