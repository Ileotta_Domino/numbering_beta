﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class ConfigurationFile : INotifyPropertyChanged
    {
        string _ConfigurationFileValue;
        string _ConfigurationFileValueToKeep;
        private readonly IMatrixProcessor _matrixprocessor;

        public ConfigurationFile(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._ConfigurationFileValue = "42";
            }
        }
        public string ConfigurationFileValueToShow
        {
            get { return _ConfigurationFileValue; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_ConfigurationFileValue != value)
                {
                    _ConfigurationFileValue = value;
                    RaisePropertyChanged("ConfigurationFileValue");
                }
            }
        }

        public string ConfigurationFileValueToKeep
        {
            get
            {

                return _ConfigurationFileValueToKeep = _matrixprocessor.GetConfigurationFileValue();
            }

            set
            {
                if (_ConfigurationFileValueToKeep != value)
                {
                    _matrixprocessor.SetConfigurationFileValue(value);
                    RaisePropertyChanged("ConfigurationFileValueToKeep");
                }
            }
        }


        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
