﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
   public class SequenceValue : INotifyPropertyChanged
    {
        string _sequenceValue;
        string _sequenceValueToKeep;
     //   private readonly IMatrixProcessor _matrixprocessor;
        private readonly ISequenceProcessor _sequenceProcessor;

        public SequenceValue( ISequenceProcessor sequenceProcessor)
        {
           // _matrixprocessor = matrixProcessor;
            _sequenceProcessor = sequenceProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._sequenceValue = "42";
            }
        }

        public string SequenceValuetoShow
        {
            get { return _sequenceValue ; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_sequenceValue != value)
                {
                    _sequenceValue = value;
                    RaisePropertyChanged("SequenceValueToShow");
                }
            }
        }


        public string SequenceValueToKeep
        {
            get
            {
                return _sequenceValueToKeep=_sequenceProcessor.GetSequence();
            }

            set
            {
                if (_sequenceValueToKeep != value)
                {
                    _sequenceProcessor.SetSequence(value);
             //       SequenceValuetoShow = value;
                    RaisePropertyChanged("SequenceValueToKeep");
                }
            }
        }


        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
