﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class Modulus: INotifyPropertyChanged
    {
        private readonly IMatrixProcessor _matrixprocessor;

        string _modulusValue;
        public Modulus(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._modulusValue = "42";
            }
        }

        public string ModulusValuetoShow
        {
            get { return _modulusValue; }
            set
            {
                if (_modulusValue != value)
                {
                    _modulusValue = value;
                    RaisePropertyChanged("ModulusValuetoShow");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
