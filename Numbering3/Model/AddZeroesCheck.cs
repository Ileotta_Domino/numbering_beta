﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public  class AddZeroesCheck : INotifyPropertyChanged
    {
        private readonly IMatrixProcessor _matrixprocessor;
        private bool _isSelected;

        public AddZeroesCheck(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._isSelected = true;
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value) return;

                _isSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
