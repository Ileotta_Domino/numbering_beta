﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class StartPointValue : INotifyPropertyChanged
    {
        private readonly IMatrixProcessor _matrixprocessor;
        private string _StartPoint;
        private string _StartPointValueToKeep;

        public StartPointValue(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._StartPoint = "1";
            }
        }

        public string StartPoint
        {
            get { return _StartPoint; }
            set
            {
                if (_StartPoint == value) return;

                _StartPoint = value;
                RaisePropertyChanged("StartPointAdded");
            }
        }


        public string StartPointValueToKeep
        {
            get
            {
                return _StartPointValueToKeep = _matrixprocessor.GetStartPoint();
            }

            set
            {
                if (_StartPointValueToKeep != value)
                {
                    _matrixprocessor.SetStartPoint(value);
                    RaisePropertyChanged("StartPointValueToKeep");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
