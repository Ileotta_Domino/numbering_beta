﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class DoRolledColumnRow : IStrategySwitcher
    {
        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        private readonly IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        private readonly ILineIndex _lineIndex = new LineIndex();
        private readonly ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        private readonly IListRowsProcessor _listRowsProcessor = new ListRowsProcessor();
        private readonly IAlgorithmCalculatorHelper _algorithmCalculatorHelper = new AlgorithmCalculatorHelper();
        int Y;
        int X;
        int Z;
        private string ValueStringtoAdd = "";

        public void DoAlgo()
        {
            /// throw new NotImplementedException();
            ///  
            //int[,,] Matrix;
            int ArrayInLength = _matrixProcessor.GetLeadInArrayLength();
            int ArrayDigitLength = _matrixProcessor.GetArrayDigitLength();
            int[] digits = new int[ArrayDigitLength];
            int[] lineIndex = new int[ArrayDigitLength];
            int[] zeroarr = new int[ArrayDigitLength];
            char[] LeadIn = new char[_matrixProcessor.GetLeadInArray().Length];
            char[] LeadOut = new char[_matrixProcessor.GetLeadOutArray().Length];
            char[] tempchararray = new char[ArrayInLength];
            int LeadInNumber = _matrixProcessor.GetNumberOfLeadIn();
            int LeadOutNumber = _matrixProcessor.GetNumberOfLeadOut();
            List<string> ListofRows = new List<string>();
            //string tempRow;
            //int indexval;
            //int indexCount = 0;
            //int counter = 0;
            //int Gridcounter = 0;
            //string IndexRow;
            X = _matrixProcessor.GetRow();
            Y = _matrixProcessor.GetColumn();
            Z = _matrixProcessor.GetSheet();
            int s = _matrixProcessor.GetStepValue();
            //int stringtoint = 0;
            int outputlength = _sequenceProcessor.GetSequenceLength();
            //string pathBis = "C:\\Users\\I_Leotta\\Documents\\NumberingTestBis.txt";
            //string path = "C:\\Users\\I_Leotta\\Documents\\NumberingTest.txt";
            //char[] tempcharArray;
            //char[] baseCharArray;

            //Array.Copy(_matrixProcessor.GetLeadInArray(), LeadIn, _matrixProcessor.GetLeadInArray().Length);
            //Array.Copy(_matrixProcessor.GetLeadOutArray(), LeadOut, _matrixProcessor.GetLeadOutArray().Length);

            //    using (StreamWriter twBis = File.CreateText(pathBis))
            //using (StreamWriter twBis = File.CreateText(@".\NumberingTestBis.txt"))
                _listRowsProcessor.ListOfRowsCreate();

            if (_matrixProcessor.GetBitCheck() != "None")
            {
                ArrayInLength += 1;
            }
            _matrixProcessor.CreateMatrix(X, Y, Z);

            for (int k = 0; k < Z; k++)
            {
                //  for (int i = 0; i < X; i++)
                for (int j = 0; j < Y; j++)
                {
                    // for (int j = 0; j < Y; j++)
                    for (int i = 0; i < X; i++)
                    {
                        int elem = s * ((k * (X * Y)) + j + (i * Y));
                        _matrixProcessor.SetMatrix(elem, i, j, k);
                    }
                }
            }
        }
    
    public static string IntToString(int value, char[] baseChars)
    {
        string result = string.Empty;
        int targetBase = baseChars.Length;

        do
        {
            result = baseChars[value % targetBase] + result;
            value = value / targetBase;
        }
        while (value > 0);

        return result;

    }

    public string SpacesToadd(int generalArraylength, string tosubtract)
    {
        string spacesline = "";
        for (int h = 0; h < generalArraylength - tosubtract.Length; h++)
        {
            spacesline = spacesline + "  ";
        }
        return spacesline;
    }

    public string fromCharArrayToString(char[] input)
    {
        string result = string.Join("", input);
        return result;
    }

    public string fromStringArrayToString(string[] input)
    {
        string result = string.Join("", input);
        return result;
    }
    public string fromArrayToString(int[] input)
    {
        string result = string.Join("", input);
        return result;
    }

}
}
