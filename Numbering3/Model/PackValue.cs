﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class PackValue : INotifyPropertyChanged
    {
        string _packValue;
        int _packValueToKeep;
        private readonly IMatrixProcessor _matrixprocessor;

        public PackValue(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._packValue = "42";
            }
        }

        public string PackValuetoShow
        {
            get { return _packValue; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_packValue != value)
                {
                    _packValue = value;
                    RaisePropertyChanged("PackValuetoShow");
                }
            }
        }

        public int PackValueToKeep
        {
            get
            {
                if (_matrixprocessor.GetPack() == 0)
                {
                    _matrixprocessor.SetPack(1);
                }
                return _packValueToKeep = _matrixprocessor.GetPack();
            }

            set
            {
                if (_packValueToKeep != value)
                {
                    _matrixprocessor.SetPack(value);
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
