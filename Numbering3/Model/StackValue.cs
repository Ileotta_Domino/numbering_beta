﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class StackValue : INotifyPropertyChanged
    {
        string _stackValue;
        int _stackValueToKeep;
        private readonly IMatrixProcessor _matrixprocessor;

        public StackValue(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._stackValue = "42";
            }
        }

        public string StackValuetoShow
        {
            get { return _stackValue; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_stackValue != value)
                {
                    _stackValue = value;
                    RaisePropertyChanged("RowValuetoShow");
                }
            }
        }

        public int StackValueToKeep
        {
            get
            {
                if (_matrixprocessor.GetStack() == 0)
                {
                    _matrixprocessor.SetStack(1);
                }
                return _stackValueToKeep = _matrixprocessor.GetStack();
            }

            set
            {
                if (_stackValueToKeep != value)
                {
                    _matrixprocessor.SetStack(value);
                }
            }
        }
        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
