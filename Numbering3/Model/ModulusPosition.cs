﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class ModulusPosition : INotifyPropertyChanged
    {
        string _ModulusPositionName;
        private readonly IMatrixProcessor _matrixprocessor;

        public ModulusPosition(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
        }

        public string ModulusPositionName
        {
            get { return _ModulusPositionName; }
            set
            {
                if (_ModulusPositionName != value)
                {
                    _ModulusPositionName = value;
                    _matrixprocessor.SetModulusPosition(_ModulusPositionName.ToString());
                    OnPropertyChanged("ModulusTypeName");
                }
            }
        }

        void OnPropertyChanged(string propertyName)

        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
