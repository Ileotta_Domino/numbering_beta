﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class ValuesToShow : INotifyPropertyChanged
    {
        private readonly IMatrixProcessor _matrixprocessor;

        string _value00;
        string _value0Last;
        string _valueLastLast;
        string _valueLast0;
        string _LeadOutToShow;


        public ValuesToShow(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._value00 = "42";
            }
        }


        public string ValueZeroZero
        {
            get { return _value00; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_value00 != value)
                {
                    _value00 = value;
                    RaisePropertyChanged("ValuetoShow");
                }
            }
        }

        public string ValueZeroLast
        {
            get { return _value0Last; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_value0Last != value)
                {
                    _value0Last = value;
                    RaisePropertyChanged("ValuetoShow");
                }
            }
        }

        public string ValueLastZero
        {
            get { return _valueLast0; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_valueLast0 != value)
                {
                    _valueLast0 = value;
                    RaisePropertyChanged("ValuetoShow");
                }
            }
        }
        public string ValueLastLast
        {
            get { return _valueLastLast; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_valueLastLast != value)
                {
                    _valueLastLast = value;
                    RaisePropertyChanged("ValuetoShow");
                }
            }
        }

        public string LeadOutToShow
        {
           get { return _LeadOutToShow; }
            set
            {
                if (_LeadOutToShow != value)
                {
                    _LeadOutToShow = value;
                    RaisePropertyChanged("LeadOutToShow");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
