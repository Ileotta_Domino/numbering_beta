﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    class OrderProcessingObject
    {
        public event EventHandler OrderChanged;

        List<OrderHelper> Ordering { get; set; }

        public OrderProcessingObject()
        { }
        public List<OrderHelper> GetOrder()
        {
            return Ordering;
        }

        void OnOrderChanged()
        {
            if (OrderChanged != null)
                OrderChanged(this, null);
        }
    }
}
