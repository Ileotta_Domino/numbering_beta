﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class ColumnValue : INotifyPropertyChanged
    {
        string _coulmnValue;
        int _columnValueToKeep;
        private readonly IMatrixProcessor _matrixprocessor;
        private readonly IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();

        public bool _CanColumPropertyBeChanged;

        public ColumnValue(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._coulmnValue = "42";
            }
        }

        public bool CanColumPropertyBeChanged
        {
            get
            {
                // return (_AddZeroesCheck != null) ? _AddZeroesCheck : false; 
                return _CanColumPropertyBeChanged;
            }
            set
            {
                if (_CanColumPropertyBeChanged != value)
                {
                    _CanColumPropertyBeChanged = value;

                    RaisePropertyChanged("CanColumPropertyBeChanged");
                }
            }
        }
        public string ColumnValuetoShow
        {
            get { return _coulmnValue; }
            set
            {
                if (_coulmnValue != value)
                {
                    _coulmnValue =value;
                  
                    RaisePropertyChanged("ColumnValuetoShow");
                }
            }
        }

        public int ColumnValueToKeep
        {
            get
            {
                if (_matrixprocessor.GetColumn() == 0 )
                {
                    _matrixprocessor.SetColumn(1);
                }
                return _columnValueToKeep = _matrixprocessor.GetColumn();
            }

            set
            {
                if (_columnValueToKeep != value)
                {
                    _matrixprocessor.SetColumn(value);
                    RaisePropertyChanged("ColumnValuetoShow");
                    if (value <= 2)
                    { CanColumPropertyBeChanged = false; }
                    else { CanColumPropertyBeChanged = true; }
                    RaisePropertyChanged("CanColumPropertyBeChanged");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
