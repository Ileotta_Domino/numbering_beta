﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class Order : INotifyPropertyChanged
    {
        string _Ordername;
        private readonly IMatrixProcessor _matrixprocessor;
        public Order(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
        }
        public string OrderName
        {
            get { return _Ordername; }
            set
            {
                if (_Ordername != value)
                {
                    _Ordername = value;
                    _matrixprocessor.SetOrder(value);
                    OnPropertyChanged("OrderName");
                }
            }
        }
        

        void OnPropertyChanged(string propertyName)

        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}
