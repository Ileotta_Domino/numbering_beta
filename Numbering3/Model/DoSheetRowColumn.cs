﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Model.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class DoSheetRowColumn : IStrategySwitcher
    {
        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        private readonly IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        private readonly ILineIndex _lineIndex = new LineIndex();
        private readonly ISequenceProcessor _sequenceProcessor = new SequenceProcessor();
        private readonly IListRowsProcessor _listRowsProcessor = new ListRowsProcessor();
        private readonly IAlgorithmCalculatorHelper _algorithmCalculatorHelper = new AlgorithmCalculatorHelper();
        int intToFormat = 0;
        int Y;
        int X;
        int Z;
        private List<TypeElement> ListOfType = new List<TypeElement>();
        private string ValueStringtoAdd = "";


        public void DoAlgo()
        {
            int[,,] Matrix;
            int ArrayInLength = _matrixProcessor.GetArrayInLength();
            int[] digits = new int[ArrayInLength];
            int[] lineIndex = new int[ArrayInLength];
            int[] zeroarr = new int[ArrayInLength];
            char[] LeadIn = new char[_matrixProcessor.GetLeadInArray().Length];
            char[] LeadOut = new char[_matrixProcessor.GetLeadOutArray().Length];
            char[] tempchararray = new char[ArrayInLength];
            int LeadInNumber = _matrixProcessor.GetNumberOfLeadIn();
            int LeadOutNumber = _matrixProcessor.GetNumberOfLeadOut();
            List<string> ListofRows = new List<string>();
            string tempRow;
            int indexval;
            int indexCount = 0;
            int counter = 0;
            int LeadOutcounter = 0;
            int Gridcounter = 0;
            X = _matrixProcessor.GetRow();
            Y = _matrixProcessor.GetColumn();
            Z = _matrixProcessor.GetSheet();
            int stringtoint = 0;
            string IndexRow;
            int outputlength = _sequenceProcessor.GetSequenceLength();
            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string pathBis = Path.Combine(location, "NumberingTestBis.txt");
         //   string pathBis = "C:\\Users\\I_Leotta\\Documents\\NumberingTestBis.txt";
            string path = "C:\\Users\\I_Leotta\\Documents\\NumberingTest.txt";
            char[] tempcharArray;
            char[] baseCharArray;

            Array.Copy(_matrixProcessor.GetLeadInArray(), LeadIn, _matrixProcessor.GetLeadInArray().Length);
            Array.Copy(_matrixProcessor.GetLeadOutArray(), LeadOut, _matrixProcessor.GetLeadOutArray().Length);
            //    using (StreamWriter twBis = File.CreateText(pathBis))
            var watch = System.Diagnostics.Stopwatch.StartNew();
            _listRowsProcessor.ListOfRowsCreate();
            if (_matrixProcessor.GetBitCheck() != "None")
            {
                ArrayInLength += 1;
            }
            _matrixProcessor.CreateMatrix(X, Y, Z);


            for (int n = 0; n < Z; n++)
            {
                for (int j = 0; j < Y; j++)
                {
                    for (int i = 0; i < X; i++)
                    {
                        //  int elem = i + 1 + (j * X);
                        int elem = (i * (Y * Z)) + n + (j * Z);
                        _matrixProcessor.SetMatrix(elem, i,j,n);
                    }
                }
            }
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            using (StreamWriter twLog = File.CreateText(@".\NumberingTimeLog.txt"))
            {
                twLog.WriteLine("CreateMatrix DoSheetRowColumn" + elapsedMs);
            }
            for (int p = 0; p <= _matrixProcessor.GetRepetitionNumber(); p++)

                {
                    //   using (StreamWriter twBis = File.CreateText(pathBis))
                    using (StreamWriter twBis = File.CreateText(@".\NumberingTestBis.txt"))
                    {
                        Matrix = _matrixProcessor.GetMatrix();

                        Array.Clear(lineIndex, 0, lineIndex.Length);
                        indexval = counter;
                        indexCount = 0;
                    }
                }
            _algorithmCalculatorHelper.Calculator();
            //    //fill datagrid
            //    using (StreamWriter twBis = File.AppendText(pathBis))
            //        for (int k = 0; k < LeadInNumber; k++)
            //        {
            //            for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
            //            {
            //                for (int i = 0; i <= Matrix.GetUpperBound(1); i++)
            //                {
            //                    tempRow = string.Empty;

            //                    //Index adder start
            //                    Array.Clear(lineIndex, 0, lineIndex.Length);
            //                    indexval = Gridcounter;

            //                    indexCount = 0;
            //                    for (; indexval != 0; indexval /= 10)
            //                    {
            //                        lineIndex[indexCount] = indexval % 10;
            //                        indexCount++;
            //                    }
            //                    Array.Reverse(lineIndex);
            //                    IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

            //                    //index adder end
            //                    for (int j = 0; j <= Matrix.GetUpperBound(0); j++)
            //                    {
            //                        string spacestoadd = SpacesToadd(ArrayInLength, fromCharArrayToString(LeadIn));
            //                        tempRow = tempRow + fromCharArrayToString(LeadIn) + spacestoadd + "   ";
            //                    }
            //                    Gridcounter++;
            //                    _listRowsProcessor.ListOfRowsAdd(IndexRow + tempRow);
            //                    twBis.WriteLine("{0}", IndexRow + "  " + tempRow);
            //                }
            //            }
            //        }

            //    using (StreamWriter twBis = File.AppendText(pathBis))
            //        for (int n = 0; n <= Matrix.GetUpperBound(2); n++)
            //        {

            //            for (int i = 0; i <= Matrix.GetUpperBound(1); i++)
            //            {
            //                tempRow = string.Empty;
            //                //Index adder start
            //                Array.Clear(lineIndex, 0, lineIndex.Length);
            //                indexval = Gridcounter;
            //                indexCount = 0;

            //                for (; indexval != 0; indexval /= 10)
            //                {
            //                    lineIndex[indexCount] = indexval % 10;
            //                    indexCount++;
            //                }
            //                Array.Reverse(lineIndex);
            //                IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

            //                //index adder end

            //                for (int j = 0; j <= Matrix.GetUpperBound(0); j++)
            //                {

            //                    Array.Clear(digits, 0, digits.Length);
            //                    int count = 0;
            //                    int val = Matrix[i, j, n];

            //                    for (; val != 0; val /= 10)
            //                    {
            //                        digits[count] = val % 10;
            //                        count++;
            //                    }
            //                    Array.Reverse(digits);

            //                    if (_matrixProcessor.GetBitCheck() != "None")
            //                    {
            //                        if (_matrixProcessor.GetBitCheck() == "Modulus 2")
            //                        {
            //                            digits[0] = Matrix[i, j, n] % 2;
            //                        }
            //                        else { digits[0] = Matrix[i, j, n] % 3; }

            //                    }
            //                    Int32.TryParse(fromArrayToString(digits), out stringtoint);

            //                    //fill Array and turn into string
            //                    baseCharArray = _alphabetConverterHelper.makeBaseArray();
            //                    int indexbase = baseCharArray.Length - 1;
            //                    ValueStringtoAdd = _alphabetConverterHelper.FormattingIterating(stringtoint, 0, "");
            //                    tempcharArray = ValueStringtoAdd.ToCharArray();
            //                    for (int q = tempcharArray.Length - 1; q >= 0; q--)
            //                    {
            //                        baseCharArray[indexbase--] = tempcharArray[q];
            //                    }
            //                    ValueStringtoAdd = fromCharArrayToString(baseCharArray);

            //                    tempRow = tempRow + ValueStringtoAdd + "   ";
            //                }
            //                Gridcounter++;
            //                twBis.WriteLine("{0}", IndexRow + "  " + tempRow);
            //                _listRowsProcessor.ListOfRowsAdd(IndexRow + tempRow);
            //            }

            //        }
            //    //////////////////////////Lead out //////////////////////////////////////

            //    using (StreamWriter twBis = File.AppendText(pathBis))
            //        for (int k = 0; k < LeadOutNumber; k++)
            //        {
            //            for (int h = 0; h <= Matrix.GetUpperBound(2); h++)
            //            {
            //                for (int i = 0; i <= Matrix.GetUpperBound(1); i++)
            //                {
            //                    tempRow = string.Empty;

            //                    //Index adder start
            //                    Array.Clear(lineIndex, 0, lineIndex.Length);
            //                    indexval = Gridcounter;

            //                    indexCount = 0;
            //                    for (; indexval != 0; indexval /= 10)
            //                    {
            //                        lineIndex[indexCount] = indexval % 10;
            //                        indexCount++;
            //                    }
            //                    Array.Reverse(lineIndex);
            //                    IndexRow = fromArrayToString(lineIndex) + "   " + fromArrayToString(zeroarr) + "   ";

            //                    //index adder end
            //                    for (int j = 0; j <= Matrix.GetUpperBound(0); j++)
            //                    {
            //                        string spacestoadd = SpacesToadd(ArrayInLength, fromCharArrayToString(LeadOut));
            //                        tempRow = tempRow + fromCharArrayToString(LeadOut) + spacestoadd + "   ";
            //                    }
            //                    Gridcounter++;
            //                    _listRowsProcessor.ListOfRowsAdd(IndexRow + tempRow);
            //                    twBis.WriteLine("{0}", IndexRow + "  " + tempRow);
            //                }
            //            }
            //        }
            //}
            /////////////////////////////////////////////////End LeadOut/////////////////////////////////////////////////////////////////////////

        }

        public static string IntToString(int value, char[] baseChars)
        {
            string result = string.Empty;
            int targetBase = baseChars.Length;

            do
            {
                result = baseChars[value % targetBase] + result;
                value = value / targetBase;
            }
            while (value > 0);

            return result;
        }

        public string SpacesToadd(int generalArraylength, string tosubtract)
        {
            string spacesline = "";
            for (int h = 0; h < generalArraylength - tosubtract.Length; h++)
            {
                spacesline = spacesline + "  ";
            }
            return spacesline;
        }

        public string fromCharArrayToString(char[] input)
        {
            string result = string.Join("", input);
            return result;
        }

        public string fromStringArrayToString(string[] input)
        {
            string result = string.Join("", input);
            return result;
        }
        public string fromArrayToString(int[] input)
        {
            string result = string.Join("", input);
            return result;
        }

    }
}
