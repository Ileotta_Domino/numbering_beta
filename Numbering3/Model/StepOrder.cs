﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class StepOrder : INotifyPropertyChanged
    {
        string _StepOrderName;
        private readonly IMatrixProcessor _matrixprocessor;

        public StepOrder(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
        }


        public string StepOrderName
        {
            get { return _StepOrderName; }
            set
            {
                if (_StepOrderName != value)
                {
                    _StepOrderName = value;
                    _matrixprocessor.SetStepOrder(_StepOrderName.ToString());
                    OnPropertyChanged("StepOrderName");
                }
            }
        }

        void OnPropertyChanged(string propertyName)

        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
    }
