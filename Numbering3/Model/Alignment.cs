﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
   public class Alignment : INotifyPropertyChanged
    {
        string _AlignmentName;
        private readonly IMatrixProcessor _matrixprocessor;

        public Alignment(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
        }

        public string AlignmentName
        {
            get { return _AlignmentName; }
            set
            {
                if (_AlignmentName != value)
                {
                    _AlignmentName = value;
                    _matrixprocessor.SetBitCheck(value);
                    RaisePropertyChanged("AlignmentName");
                }
            }
        }

        void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
