﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class SheetValue : INotifyPropertyChanged
    {
        string _sheetValue;
        int _sheetValueToKeep;
        private readonly IMatrixProcessor _matrixprocessor;
        private readonly IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();

        public SheetValue(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._sheetValue = "42";
            }
        }
        public string SheetValuetoShow
        {
            get { return _sheetValue; }
            set
            {
                if (_sheetValue != value)
                {
                    _sheetValue =value;
                    RaisePropertyChanged("SheetValuetoShow");
                }
            }
        }
        public int SheetValueToKeep
        {
            get
            {
                if (_matrixprocessor.GetSheet() == 0)
                {
                    _matrixprocessor.SetSheet(1);
                }
                return _sheetValueToKeep = _matrixprocessor.GetSheet();
            }

            set
            {
                if (_sheetValueToKeep != value)
                {
                    _matrixprocessor.SetSheet(value);
                }
            }
        }


        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
