﻿using Numbering3.Model.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class SequenceHelper: ISequenceHelper
    {

        int incrementValue(int i, int outputlength, int sequencelegth)
        {
            // a verbose way of writing "return (i + 1) % 26^3"
            i++;
            if (i == Math.Pow(sequencelegth, outputlength)) i = 0;
            return i;
        }

       public string formatValue(int i, int outputlength, int sequencelegth)
        {
            var result = new StringBuilder();
            int j = 0;
           while (j != outputlength)
            {
                result.Insert(0, (char)('A' + (i % sequencelegth)));
            i /= 26;
              }
            return result.ToString();
        }


        public char[] CreateArray(int length)
        {
            char[] SequenceArray = new char[length];
            return SequenceArray;
        }

        public void DoSequence(char[] input)
        {
            int lengtharr = input.Length;
            var temparr = CreateArray(lengtharr);
            
        }

        public char SequenceCounter(int length, char[] input, int number)
        {
            int mod;

            mod = number % length;

            return input[mod];
        }


    }
}
