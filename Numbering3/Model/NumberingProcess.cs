﻿using Numbering3.Model.Interface;
using Numbering3.Parameters;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class NumberingProcess : INumberingProcess
    {
        private readonly IMatrixProcessor _matrixProcessor = new MatrixProcessor();
        int Y;
        int X;
        int Z;
        int[,,] Matrix;
        public NumberingProcess()
        {

        }

        public void execute(object parameter)
        {
            throw new NotImplementedException();
        }

        public void fillArray()
        {
            throw new NotImplementedException();
        }

      //  private int _rowNumber;


        public void fillArrayRowColumn()
        {
            X = _matrixProcessor.GetRow();
            Y = _matrixProcessor.GetColumn();
            Z = _matrixProcessor.GetSheet();
            int ArrayInLength = _matrixProcessor.GetArrayInLength();

            if (_matrixProcessor.GetBitCheck() != "None")
            {
                ArrayInLength += 1;
           //     if (_matrixProcessor.GetBitCheck()== "Modulus 2")
            }
            int[] digits = new int[ArrayInLength];
            _matrixProcessor.CreateMatrix(X, Y, Z);

             for (int n=0; n<Z; n++)
                {
                for (int j = 0; j < Y; j++)
                {
                    for (int i = 0; i < X; i++)
                    {
                      //  int elem = i + 1 + (j * X);
                        int elem=(n*(X*Y))+i + 1 + (j * X);
                        _matrixProcessor.SetMatrix(elem, i, j, n);
                    }
                }
            }



            using (TextWriter tw = new StreamWriter("C:\\Users\\I_Leotta\\Documents\\NumberingTest.txt"))
            {
                Matrix = _matrixProcessor.GetMatrix();
                for (int n = 0; n <= Matrix.GetUpperBound(2); n++)
                {
                    for (int i = 0; i <= Matrix.GetUpperBound(0); i++)
                {
                    for (int j = 0; j <= Matrix.GetUpperBound(1); j++)
                    {

                            Array.Clear(digits, 0, digits.Length);
                            int count = 0;
                            int val = Matrix[i, j, n];

                            for (; val != 0; val /= 10)
                            {
                                digits[count] = val % 10;
                                count++;
                            }
                            Array.Reverse(digits);

                            if (_matrixProcessor.GetBitCheck() != "None")
                            {
                                if (_matrixProcessor.GetBitCheck() == "Modulus 2")
                                {
                                    digits[0] = Matrix[i, j, n] % 2;
                                }
                                else { digits[0] = Matrix[i, j, n]  % 3; }

                            }

                            tw.Write("{0}", digits[0]);
                            
                            for (int k = 1; k < digits.Length; k++)
                            {
                                //translate to letters
                                string hexavigesimal = IntToString(digits[k], Enumerable.Range('A', 26).Select(x => (char)x).ToArray());
                             //    tw.Write("{0}", digits[k]);
                                tw.Write("{0}", hexavigesimal);
                            }
                            tw.Write("{0}", " ");
                        }
                        tw.WriteLine();
                    }
                    tw.WriteLine();
                }
            }
        }


        public void converter()
        {
            string hexavigesimal = IntToString(42, Enumerable.Range('A', 26).Select(x => (char)x).ToArray());
        }

        public static string IntToString(int value, char[] baseChars)
        {
            string result = string.Empty;
            int targetBase = baseChars.Length;

            do
            {
                result = baseChars[value % targetBase] + result;
                value = value / targetBase;
            }
            while (value > 0);

            return result;
        }

        }
    }

