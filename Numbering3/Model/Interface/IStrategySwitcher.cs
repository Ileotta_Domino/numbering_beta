﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model.Interface
{
    public interface IStrategySwitcher
    {
        void DoAlgo();
    }
}
