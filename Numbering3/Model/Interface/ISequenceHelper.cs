﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model.Interface
{
    public interface ISequenceHelper
    {
        string formatValue(int i, int outputlength, int sequencelegth);
        char[] CreateArray(int length);
        void DoSequence(char[] input);
        char SequenceCounter(int length, char[] input, int number);
    }
}
