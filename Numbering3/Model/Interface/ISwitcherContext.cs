﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Numbering3.Model.SwitcherContext;

namespace Numbering3.Model.Interface
{
    public interface ISwitcherContext
    {
        //  void DoAlgorithm(eOrderingSelection ordering);
        void DoAlgorithm(string ordering);
    }
}
