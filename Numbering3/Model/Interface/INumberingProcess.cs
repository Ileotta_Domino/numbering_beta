﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model.Interface
{
    public interface INumberingProcess
    {
        void fillArray();
        void execute(object parameter);
        void fillArrayRowColumn();
    }
}
