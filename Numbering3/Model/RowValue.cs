﻿using Numbering3.Helper;
using Numbering3.Helper.Interface;
using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class RowValue : INotifyPropertyChanged
    {
        string _rowValue;
        string _lastRowColumn;
        int _rowValueToKeep;
        private readonly IMatrixProcessor _matrixprocessor;
        private readonly IAlphabetConverterHelper _alphabetConverterHelper = new AlphabetConverterHelper();
        public bool _CanRowPropertyBeChanged;
        public RowValue(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._rowValue  = "42";
            }
        }

        public bool CanRowPropertyBeChanged
        {
            get
            {
                // return (_AddZeroesCheck != null) ? _AddZeroesCheck : false; 
                return _CanRowPropertyBeChanged;
            }
            set
            {
                if (_CanRowPropertyBeChanged != value)
                {
                    _CanRowPropertyBeChanged = value;

                    RaisePropertyChanged("CanRowPropertyBeChanged");
                }
            }
        }
        public string RowValuetoShow
        {
            get { return _rowValue; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_rowValue != value)
                {
                    _rowValue = value;
                    RaisePropertyChanged("RowValuetoShow");
                }
            }
        }

        public string LastRowColumn
        {
            get { return _lastRowColumn; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_lastRowColumn != value)
                {
                    _lastRowColumn = value;
                    RaisePropertyChanged("RowValuetoShow");
                }
            }
        }

        public int RowValueToKeep
        {
            get
            {
                if (_matrixprocessor.GetRow() == 0)
                {
                    _matrixprocessor.SetRow(1);
                }
                if (_matrixprocessor.GetRow() > 40)
                {
                    _matrixprocessor.SetRow(40);
                }
                if (_matrixprocessor.GetRow() <= 0)
                { CanRowPropertyBeChanged = false; }
                else { CanRowPropertyBeChanged = true; }
                return _rowValueToKeep = _matrixprocessor.GetRow();
            }

            set
            {
                if (_rowValueToKeep != value)
                {
                    _matrixprocessor.SetRow(value);
                    if (value < 1)
                    { _matrixprocessor.SetRow(1); }

               //     { CanRowPropertyBeChanged = false; }
                   // else {
                        CanRowPropertyBeChanged = true; 
                    RaisePropertyChanged("CanRowPropertyBeChanged");
                    RaisePropertyChanged("RowValuetoShow");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
