﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class ModulusType : INotifyPropertyChanged
    {
        string _ModulusTypeName;
        private readonly IMatrixProcessor _matrixprocessor;


        public ModulusType(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
        }

        public string ModulusTypeName
        {
            get { return _ModulusTypeName; }
            set
            {
                if (_ModulusTypeName != value)
                {
                    _ModulusTypeName = value;
                    _matrixprocessor.SetModulusType(_ModulusTypeName.ToString());
                    OnPropertyChanged("ModulusTypeName");
                }
            }
        }
        void OnPropertyChanged(string propertyName)

        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
