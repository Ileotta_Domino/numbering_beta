﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class MultiplyBySheet : INotifyPropertyChanged
    {
        private readonly IMatrixProcessor _matrixprocessor;
        int _MultiplyBySheetValue;
        int _MultiplyBySheetValueToKeep;

        public MultiplyBySheet(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._MultiplyBySheetValue = 42;
            }
        }

        public int MultiplyBySheetValueToShow
        {
            get { return _MultiplyBySheetValue; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_MultiplyBySheetValue != value)
                {
                    _MultiplyBySheetValue = value;
                    RaisePropertyChanged("MultiplyBySheetValue");
                }
            }
        }

        public int MultiplyBySheetValueToKeep
        {
            get
            {
                if (_matrixprocessor.GetMultiplyBySheetValue() == 1)
                {
                    _matrixprocessor.SetMultiplyBySheetValue(1);
                }
                return _MultiplyBySheetValueToKeep = _matrixprocessor.GetMultiplyBySheetValue();
            }

            set
            {
                if (_MultiplyBySheetValueToKeep != value)
                {
                    _matrixprocessor.SetMultiplyBySheetValue(value);
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
