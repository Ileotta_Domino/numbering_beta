﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Numbering3.Model
{
    public class MultiplyByColumn : INotifyPropertyChanged
    {
        private readonly IMatrixProcessor _matrixprocessor;
        int _MultiplyByColumnValue;
        int _MultiplyByColumnValueToKeep;

        public MultiplyByColumn(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._MultiplyByColumnValue = 42;
            }
        }

        public int MultiplyByColumnValueToShow
        {
            get { return _MultiplyByColumnValue; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_MultiplyByColumnValue != value)
                {
                    _MultiplyByColumnValue = value;
                    RaisePropertyChanged("MultiplyByColumnValue");
                }
            }
        }

        public int MultiplyByColumnValueToKeep
        {
            get
            {
                if (_matrixprocessor.GetMultiplyByColumnValue() == 1)
                {
                    _matrixprocessor.SetMultiplyByColumnValue(1);
                }
                return _MultiplyByColumnValueToKeep = _matrixprocessor.GetMultiplyByColumnValue();
            }

            set
            {
                if (_MultiplyByColumnValueToKeep != value)
                {
                    _matrixprocessor.SetMultiplyByColumnValue(value);
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
