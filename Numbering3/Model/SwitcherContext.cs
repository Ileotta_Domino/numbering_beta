﻿using Numbering3.Model.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class SwitcherContext : ISwitcherContext
    {
        public enum eOrderingSelection
        {
            RowColumnSheet,
            ColumnRowSheet,
            ColumnSheetRow,
            SheetColumnRow,
            SheetRowColumn,
            RowCol,
            ColRow
        }

        //   private static Dictionary<eOrderingSelection, IStrategySwitcher> _strategies = new Dictionary<eOrderingSelection, IStrategySwitcher>();
        private static Dictionary<string, IStrategySwitcher> _strategies = new Dictionary<string, IStrategySwitcher>();

        #region Ctor

        public SwitcherContext()
        {
            if (!_strategies.ContainsKey("Row,Columns,Sheets"))
            { _strategies.Add("Row,Columns,Sheets", new DoRowColumnSheet()); }
            if (!_strategies.ContainsKey("Columns,Row,Sheets"))
            { _strategies.Add("Columns,Row,Sheets", new DoColumnRowSheet()); }
            if (!_strategies.ContainsKey("Sheets,Row,Columns"))
            { _strategies.Add("Sheets,Row,Columns", new DoColumnRowSheet()); }
            if (!_strategies.ContainsKey("Sheets,Columns,Row"))
            { _strategies.Add("Sheets,Columns,Row", new DoSheetColumnRow()); }
            if (!_strategies.ContainsKey("Columns,Sheets,Row"))
            { _strategies.Add("Columns,Sheets,Row", new DoColumnSheetRow()); }
            if (!_strategies.ContainsKey("Col,Row"))
            { _strategies.Add("Col,Row", new DoRolledColumnRow()); }
            if (!_strategies.ContainsKey("Row,Columns"))
            { _strategies.Add("Row,Columns", new DoRolledRowColumn()); }

        }
        #endregion


//        public void DoAlgorithm(eOrderingSelection ordering)
      public void DoAlgorithm(string ordering)
        {   
            _strategies[ordering].DoAlgo();
        }
    }
}
