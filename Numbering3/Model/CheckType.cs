﻿using Numbering3.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbering3.Model
{
    public class CheckType : INotifyPropertyChanged
    {
        string _CheckTypeName;
        private readonly IMatrixProcessor _matrixprocessor;

        public CheckType(IMatrixProcessor matrixProcessor)
        {
            _matrixprocessor = matrixProcessor;
        }

        public string CheckTypeName
        {
              get { return _CheckTypeName; }
            set
            {
                if (_CheckTypeName != value)
                {
                    _CheckTypeName = value;
                    _matrixprocessor.SetBitCheck(value);
                    RaisePropertyChanged("CheckTypename");
                }
            }
        }

        void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
